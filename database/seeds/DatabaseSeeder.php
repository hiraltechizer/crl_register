<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       /*DB::table('admins')->insert([
            'name' => Str::random(10),
            'email' => 'admin@demo.com',
            'password' => Hash::make('admin123'),
        ]);*/

        $value = [[
            'product_id' => 1,
            'support' => 1,
            'total_value' => 100,
        ],[
            'product_id' => 2,
            'support' => 2,
            'total_value' => 200,
        ],[
            'product_id' => 3,
            'support' => 1,
            'total_value' => 100,
        ],[
            'product_id' => 4,
            'support' => 3,
            'total_value' => 300,
        ],[
            'product_id' => 5,
            'support' => 1,
            'total_value' => 100,
        ],[
            'product_id' => 6,
            'support' => 2,
            'total_value' => 200,
        ],[
            'product_id' => 7,
            'support' => 4,
            'total_value' => 400,
        ],[
            'product_id' => 8,
            'support' => 1,
            'total_value' => 100,
        ],[
            'product_id' => 9,
            'support' => 2,
            'total_value' => 200,
        ],[
            'product_id' => 10,
            'support' => 1,
            'total_value' => 100,
        ],[
            'product_id' => 11,
            'support' => 2,
            'total_value' => 200,
        ],[
            'product_id' => 12,
            'support' => 3,
            'total_value' => 300,
        ],[
            'product_id' => 13,
            'support' => 3,
            'total_value' => 300,
        ],[
            'product_id' => 14,
            'support' => 1,
            'total_value' => 100,
        ],[
            'product_id' => 15,
            'support' => 1,
            'total_value' => 100,
        ],[
            'product_id' => 16,
            'support' => 2,
            'total_value' => 200,
        ],[
            'product_id' => 17,
            'support' => 3,
            'total_value' => 300,
        ],[
            'product_id' => 18,
            'support' => 4,
            'total_value' => 400,
        ]];

        
        for($i=1;$i<=36500;$i++){
           DB::table('doctor_support_dummy')->insert([
               'doctor_id' => $i,
               'first_year' => 2020,
               'second_year' => 2021,
               'apr' => json_encode($value),
               'may' => json_encode($value),
               'jun' => json_encode($value),
               'jul' => json_encode($value),
               'aug' => json_encode($value),
               'sep' => json_encode($value),
               'oct' => json_encode($value),
               'nov' => json_encode($value),
               'dec' => json_encode($value),
               'jan' => json_encode($value),
               'feb' => json_encode($value),
               'mar' => json_encode($value),
               'created_at' => date('Y-m-d h:i:s'),
               'updated_at' => date('Y-m-d h:i:s'),
           ]);
       }
    }
}
