<?php

return [

    'api_key' => 'pu8sBXTZ[svu52N7#8Rkrm<Zp?8JY',

    'fcm_push_key' => 'AIzaSyBv-x5LClJqA7Cj4vdwWa2ZV3G7o--74JU',

    'row_no' => 20,
	
	'android_app_version' => '1.0',
	
	'ios_app_version' => '1.0',
	
	'android_app_link' => 'https://play.google.com/store/apps/details?id=com.suprivadigitaldpm&hl=en',
	
    'ios_app_link' => '',
    
    'js_css_version' => '1.0.2',

    'month' => [
        1 => 'jan',
        2 => 'feb',
        3 => 'mar',
        4 => 'apr',
        5 => 'may',
        6 => 'jun',
        7=> 'jul',
        8 => 'aug',
        9 => 'sep',
        10 => 'oct',
        11 => 'nov',
        12 => 'dec'
    ],

    'month_no' => [
        'jan' => 1,
        'feb' => 2,
        'mar' => 3,
        'apr' => 4,
        'may' => 5,
        'jun' => 6,
        'jul' => 7,
        'aug' => 8,
        'sep' => 9,
        'oct' => 10,
        'nov' => 11,
        'dec' => 12
    ],

    'p_n_value_api_dashboard' => 3000,

    // DEFAULT VALUES FOR "Rxbers Category" MODULE FILTER
    'rxbers_category_colors' => [
        'green' => 10000,
        'red' => 2000,
    ],

    // PERCENTAGE FOR USERS LISTING PROGRESS BAR
    'user_list_pr_basic_detail' => [
        'green' => 75,
        'blue' => 50,
        'orange' => 25,
        'red' => 0,
    ],
    'user_list_pr_dpm_curr_mon' => [
        'green' => 75,
        'blue' => 50,
        'orange' => 25,
        'red' => 0,
    ],

    // CRON MESSAGE LIST
    'sm_doc_remaining_msg' => '[N] doctors DPM remaining in your list. Ask your BE to fill all data before end of this month',
    'rbm_doc_remaining_msg' => '[N] doctors DPM remaining in your list. Ask your BE to fill all data before end of this month',
    'abm_doc_remaining_msg' => '[N] doctors DPM remaining in your list. Ask your BE to fill all data before end of this month',
    'be_doc_remaining_msg' => 'You have [N] doctors remaining. You are requested to fill all doctors DPM before end of the month',

];

?>