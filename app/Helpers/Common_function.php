<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Crypt;

class Common_function{
    public static function encrypt($string){
        return Crypt::encryptString($string);
    }

    public static function decrypt($string){
        return Crypt::decryptString($string);
    }

	public static function current_financial_year(){
		// FINANCIAL
        $current_year = date('Y');
        $past_year = $current_year-1;

        if(date('m') > 3){
            $start_date = date('Y-04-01');
            $end_date = ($current_year+1).'-03-31';
        }
        else{
            $start_date = $past_year.'-04-01';
            $end_date = date('Y-03-t');
        }

        $start_date = (new \DateTime($start_date));
        $end_date = (new \DateTime($end_date));
        $interval = \DateInterval::createFromDateString('1 month');

        $period   = new \DatePeriod($start_date, $interval, $end_date);

        $return = [];
        foreach ($period as $k=>$dt) {
            $return[$k] = [
                'month' => $dt->format("M"),
                'year' => $dt->format("Y"),
            ];
        }
        return $return;
	}
	
    public static function push_notification($token,$message,$title,$data){
        $google_api_key = config('custom_config.fcm_push_key');
        $url = 'https://fcm.googleapis.com/fcm/send';
        $no_data = [
            'title'=>$title,
            "message" =>$message,
			"sound" => "default",
        ];
        $no_data = array_merge($no_data,$data);
        $fields = array(
            'registration_ids' => array($token),
            'data' => $no_data,
        );
        $headers = array(
            'Authorization: key=' . $google_api_key,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if($result === FALSE) {
            //die('Curl failed: ' . curl_error($ch));
            return;
        }
        curl_close($ch);
        return;
    }

    public static function ios_push_notification($token,$message,$title,$data){
		exit;
        $passphrase = '';

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert','ck.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp){
            //exit("Failed to connect: $err $errstr" . PHP_EOL);
            return 'all';
        }
        $no_data = [
            'title'=>$title,
            "message" =>$message,
        ];
        $no_data = array_merge($no_data,$data);

        // Create the payload body
        $body['aps'] = array('alert' => $no_data,'sound'=>'default');

        $payload = json_encode($body);

        $return = [];
        if(is_array($token)){
            foreach ($token as $row){
                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $row) . pack('n', strlen($payload)) . $payload;
                $result = fwrite($fp, $msg, strlen($msg));
                if (!$result)
                    array_push($return, $row);
            }
        }
        else{
            // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            if (!$result)
                array_push($return,$token);
        }
        /*if (!$result)
            echo 'Message not delivered' . PHP_EOL;
        else
            echo 'Message successfully delivered' . PHP_EOL;
        */
        fclose($fp);
        return $return;
    }
}