<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
	public $table = 'doctor';
    protected $fillable = [
        'doctorname', 'speciality', 'hospitalname', 'city', 'message', 'downloadimage', 'status'
    ];

   
}
