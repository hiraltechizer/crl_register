<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class EducationModel extends Model
{
    protected $table = 'education';
    protected $fillable = [
        'educationTitle',
        'status'
    ];

    public function get_education_name($id){
        $name_obj = EducationModel::where('id',$id)->select('educationTitle')->first();
        return $name_obj['educationTitle'];
    }
}
