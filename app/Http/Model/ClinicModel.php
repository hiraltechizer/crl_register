<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class ClinicModel extends Model
{
    protected $table = 'clinic';
    protected $fillable = [
        'doctorid',
        'loginuserid',
        'address_line_1',
        'address_line_2',
        'cityid',
        'pincode',
        'geolocation',
        'latitude',
        'longitude',
        'status'
    ];

    public function get_clinic_name($id){
        $name_obj = ClinicModel::where('id',$id)->select('doctorid')->first();
        return $name_obj['doctorid'];
    }
}
