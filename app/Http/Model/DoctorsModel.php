<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorsModel extends Model
{
    protected $table = 'doctors';
    protected $fillable = [
        'name',
        'pos_code',
        'email',
        'contact_no',
        'city',
        'state',
        'pincode',
        'address',
        'language',
        'status',
        'productflag'
    ];

    public function get_doctors_name($id){
        $name_obj = DoctorsModel::where('id',$id)->select('name')->first();
        return $name_obj['name'];
    }
}
