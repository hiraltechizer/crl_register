<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class ClinicscheduleModel extends Model
{
    protected $table = 'clinicschedule';
    protected $fillable = [
        'clinicid',
        'day',
        'timeing',
        'starttime',
        'endtime',
        'status'
    ];

    public function get_clinicschedule_name($id){
        $name_obj = ClinicscheduleModel::where('id',$id)->select('clinicid')->first();
        return $name_obj['clinicid'];
    }
}
