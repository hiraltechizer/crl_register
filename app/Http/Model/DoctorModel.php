<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorModel extends Model
{
    protected $table = 'doctor';
    protected $fillable = [
        'doctorFname',
        'doctorLname',
        'doctorEmail',
        'password',
        'doctorMobile',
        'doctorWhatsapp',
        'education',
        'doctorSpecialization',
        'doctorProfilepicture',
        'doctorExperience',
        'type',
        'businesscardFlag',
        'status'
    ];

    public function get_doctor_name($id){
        $name_obj = DoctorModel::where('id',$id)->select('doctorFname')->first();
        return $name_obj['doctorFname'];
    }
}
