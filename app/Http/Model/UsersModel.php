<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class UsersModel extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'created_at',
        'updated_at'
    ];

    public function get_users_name($id){
        $name_obj = UsersModel::where('id',$id)->select('name')->first();
        return $name_obj['name'];
    }
}
