<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotManage
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'manage')
	{
        if (!Auth::guard($guard)->check()) {
            return redirect('manage/login');
        }
        else{
            $user = Auth::guard($guard)->user();
            if($user->status=='Disable'){
                auth($guard)->logout();
                return redirect('/manage/login')->with('error', 'Your account is disable by administrator.');
            }
            else if($user->status=='Delete'){
                auth($guard)->logout();
                return redirect('/manage/login')->with('error', "Account don't exists.");
            }
            else{
                return $next($request);
            }
        }
	}
}