<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class GetCurrUser
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = '')
	{
		if (Auth::guard('admin')->check()) {
	        $guard = 'admin';
		}
		if (Auth::guard('web')->check()) {
	        $guard = 'web';
		}

	    if ($guard=='') {
	        return redirect('login');
		}
		else{
            $user = Auth::guard($guard)->user();
            if($user->status=='Disable'){
                auth($guard)->logout();
                return redirect('/login')->with('error', 'Your account is disable by administrator.');
            }
            else if($user->status=='Delete'){
                auth($guard)->logout();
                return redirect('/login')->with('error', "Account don't exists.");
            }
            else{
                return $next($request);
            }
        }
	}
}