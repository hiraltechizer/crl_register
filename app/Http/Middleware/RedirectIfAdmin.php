<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Config;
use Illuminate\Support\Facades\Request;

class RedirectIfAdmin
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = '')
	{
	    if (Auth::guard('admin')->check()) {
			$guard = 'admin';
			if(Request::segment(1)!='admin'){
				return redirect(url('admin'));	
			}
		}
		if (Auth::guard('web')->check()) {
			$guard = 'web';
			if(Request::segment(1)=='admin'){
				return redirect(url('/'));	
			}
			Config::set('custom_config.login_user_type', Auth::guard('web')->user()->user_type);
		}
		Config::set('custom_config.login_type', $guard);

	    if ($guard=='') {
	        return redirect('login');
		}
		else{
            $user = Auth::guard($guard)->user();
            if($user->status=='Disable'){
                auth($guard)->logout();
                return redirect('/login')->with('error', 'Your account is disable by administrator.');
            }
            else if($user->status=='Delete'){
                auth($guard)->logout();
                return redirect('/login')->with('error', "Account don't exists.");
            }
            else{
                return $next($request);
            }
        }
	}
}