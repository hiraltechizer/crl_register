<?php

namespace App\Http\Middleware;

use Closure;

class ApiUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private function _logout($request){
        $value = $request->bearerToken();
        $id= (new Parser())->parse($value)->getHeader('jti');
        $token= $request->user()->tokens->find($id);
        $token->revoke();
        return $request->user()->token()->delete();
    }
    public function handle($request, Closure $next)
    {
        if($request->userId == ''){
            return response()->json(array('status'=>'0','message'=>__('custom.error_user_id')), 200);
        }
        else if($request->user()->id != $request->userId){
            return response()->json(array('status'=>'5','message'=>__('custom.error_auth_fail')), 200);
        }
        else if($request->user()->status=='Disable' || $request->user()->isFreeze==1){
            $this->_logout($request);
            return response()->json(array('status'=>'5','message'=>__('custom.error_account_disable')), 200);
        }
        else if($request->user()->status=='Delete'){
            $this->_logout($request);
            return response()->json(array('status'=>'5','message'=>__(__('custom.error_account_delete'))), 200);
        }
        return $next($request);
    }
}
