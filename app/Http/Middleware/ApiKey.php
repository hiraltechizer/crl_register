<?php

namespace App\Http\Middleware;

use Closure;

class ApiKey
{
    public function handle($request, Closure $next)
    {
        if($request->header('ApiKey') != config('custom_config.api_key')){
            return response()->json(array('status'=>'0','message'=>__('custom.error_auth_fail')), 200);
        }
        return $next($request);
    }
}
