<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Exports\UserExport;
use App\User;
use Auth;
use Excel;
use DB;

@session_start();
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		
		$input = $request->all();
		
		if(empty($input['daterange'])&& isset($_SESSION['range'])){
			$arr = explode("-",$_SESSION['range']);
			$fst = date("Y-m-d", strtotime($arr[0]));
			$first = $fst." 00:00:00";
			$snd = date("Y-m-d", strtotime($arr[1]));
			$second = $snd." 23:59:59";
			$data = array('title'=>'User','tbl'=>encrypt("users"));
			$data['offset'] = 0;
			if($request->page!=''){
				$data['offset'] = ((($request->page)-1)*config('custom_config.row_no'));
			}
			$data['result'] = User::whereBetween( 'created_at', [$first, $second])->orderBy('id', 'DESC');
			$data['result'] = $data['result']->paginate(config('custom_config.row_no'));
			$data['result'] = $data['result']->appends(['firstname'=>$request->firstname]);
			$filter_data =  User::where('status','!=','Inactive')->orderBy('id', 'DESC')->get();
			return view('admin.users',$data);
		}else{
			if(!empty($input['daterange'])){
				$_SESSION['range'] = $input['daterange'];
				$arr = explode("-",$input['daterange']);
				$fst = date("Y-m-d", strtotime($arr[0]));
				$first = $fst." 00:00:00";
				$snd = date("Y-m-d", strtotime($arr[1]));
				$second = $snd." 23:59:59";
				$data = array('title'=>'User','tbl'=>encrypt("users"));
				$data['offset'] = 0;
				if($request->page!=''){
					$data['offset'] = ((($request->page)-1)*config('custom_config.row_no'));
				}
				$data['result'] = User::whereBetween( 'created_at', [$first, $second])->orderBy('id', 'DESC');
				$data['result'] = $data['result']->paginate(config('custom_config.row_no'));
				$data['result'] = $data['result']->appends(['firstname'=>$request->firstname]);
				$filter_data =  User::whereBetween( 'created_at', [$first, $second])->get();
				
				return view('admin.users',$data);
			}else{
				
				$currentdate = date("m/d/Y");
				$beforeseven = date( "m/d/Y", strtotime( "$currentdate -7 day" ) );
				$fst = date("Y-m-d", strtotime($beforeseven));
				$first = $fst." 00:00:00";
				$snd = date("Y-m-d", strtotime($currentdate));
				$second = $snd." 23:59:59";
				$_SESSION['range'] = $beforeseven.' - '.$currentdate;
				$data['offset'] = 0;
				if($request->page!=''){
					$data['offset'] = ((($request->page)-1)*config('custom_config.row_no'));
				}
				$data['result'] = User::whereBetween( 'created_at', [$first, $second])->orderBy('id', 'DESC');
				$data['result'] = $data['result']->paginate(config('custom_config.row_no'));
				$data['result'] = $data['result']->appends(['firstname'=>$request->firstname]);
				$filter_data =  User::whereBetween( 'created_at', [$first, $second])->get();
				
				return view('admin.users',$data);

				
			}
			
			
		}
    }

    public function create()
    {
		return view('admin.createmr');
    }
	public function store(Request $request){
		$input = $request->all();
		
		$destinationPath = public_path().'/uploads/profilepic/';
        if ($cover_detail_image = $request->file('profilepicture')) {
            $cover_detail = time().'_'.$cover_detail_image->getClientOriginalName().'.'.$cover_detail_image->getClientOriginalExtension();
            $cover_detail_image->move($destinationPath, $cover_detail);
        }
		$values = array(
            'firstname' => $input['firstname'],
            'lastname' => $input['lastname'],
            'fullname' => $input['firstname'].' '.$input['lastname'],
            'empcode' => $input['empcode'],
            'email' => $input['email'],
            'zone' => $input['zone'],
            'region' => $input['region'],
            'headquarter' => $input['headquarter'],
            'profilepicture' => $cover_detail,
            'password' => bcrypt($input['password']),
            'type' => "MR",
            'status' => "Active",
            'created_at' => date("Y-m-d H:i:s")
        );
		$insertdata = DB::table('users')->insert($values);
		 if($insertdata){
			return redirect('admin/users');
		 }else{
			return redirect('admin/users'); 
		 }
		
		
	}
	/*public function insertlink(Request $request){
		$input = $request->all();
		
		$arr = explode("/",$input['expiry']);
		$dates = $arr[2]."-".$arr[1]."-".$arr[0];
		$values = array(
            'name' => $input['fullname'],
            'pos_code' => $input['prifix'],
            'link' => $input['link'],
            'expiryDate' => $dates,
            'productuseflag' => "Yes",
            'created_at' => date("Y-m-d H:i:s")
        );
		
		$insertdata = DB::table('users')->insert($values);
		$status = "updated";
		return response($status, 200);
	}*/
	public function changestatus(Request $request){
		$input = $request->all();
        $id = $input['id'];
		$create = DB::table('users')->where('id', $id)->update([
					"productuseflag" => "Yes"
					]);
		$status = "updated";
		return response($status, 200);
	}

    public function importUsers(Request $request)
    {
        
    }

    public function edit($id)
    {
		
		$row =  DB::table('users')->where('id',$id)->get();
		 return view('admin.editmr',compact('row')); 
       
    }

    public function update(Request $request, $id)
    {
		$input = $request->all();
		if(!empty($request->file('profilepicture'))){
		$destinationPath = public_path().'/uploads/profilepic/';
			if ($cover_detail_image = $request->file('profilepicture')) {
				$cover_detail = time().'_'.$cover_detail_image->getClientOriginalName().'.'.$cover_detail_image->getClientOriginalExtension();
				$cover_detail_image->move($destinationPath, $cover_detail);
			}
		}else{
			$cover_detail = $input['hiddenprofile'];
		}
		$values = array(
            'firstname' => $input['firstname'],
            'lastname' => $input['lastname'],
            'fullname' => $input['firstname'].' '.$input['lastname'],
            'empcode' => $input['empcode'],
            'email' => $input['email'],
            'zone' => $input['zone'],
            'region' => $input['region'],
            'headquarter' => $input['headquarter'],
            'profilepicture' => $cover_detail,
            'password' => $input['password'],
            'type' => "MR",
            'status' => "Active",
            'created_at' => date("Y-m-d H:i:s")
        );
		$updatedata = DB::table('users')->where('id',$id)->update($values);
		if($updatedata){
			return redirect('admin/users');
        }else{
			return redirect('admin/users');
        }
		
    }
	public function viewdata(Request $request)
    {
		$input = $request->all();
		
		$row = DB::table('users')->where('id',$input['id'])->get();
		if(!empty($row[0]->prescription)){
			$prescription = $row[0]->prescription;
			$prearr = explode(".", $prescription);
			if($prearr[1] == "pdf" || $prearr[1] == "PDF"){
				$pre = "../images/pdf-icon.png";
			}else{
				$pre = '../uploads/prescription/'.$row[0]->prescription;
			}
		}else{
				$pre = "";
		}
		if(!empty($row[0]->invoice)){
			$invoice = $row[0]->invoice;
			$invarr = explode(".", $invoice);
			if($invarr[1] == "pdf" || $invarr[1] == "PDF"){
				$inv = "../images/pdf-icon.png";
			}else{
				$inv = '../uploads/invoicebill/'.$row[0]->invoice;
			}
		}else{
				$inv = "";
		}
		if(!empty($row[0]->bottleimage)){
			$bottleimage = $row[0]->bottleimage;
			$botarr = explode(".", $bottleimage);
			if($botarr[1] == "pdf" || $botarr[1] == "PDF"){
				$bot = "../images/pdf-icon.png";
			}else{
				$bot = '../uploads/bottleimage/'.$row[0]->bottleimage;
			}
		}else{
				$bot = "";
		}
		/*$prescription = $row[0]->prescription;
		$invoice = $row[0]->invoice;
		$bottleimage = $row[0]->bottleimage;
		$extension = explode(".", $prescription);
		$extensioninvoice = explode(".", $invoice);
		$extensionbottle = explode(".", $bottleimage);
		if($extension[0] == "pdf" || $extension[0] == "PDF"){
			$pre = "../images/pdf-icon.png";
		}else{
			$pre = '../uploads/prescription/'.$row[0]->prescription;
		}
		if($extensioninvoice[0] == "pdf" || $extension[0] == "PDF"){
			$invoicebill = "../images/pdf-icon.png";
		}else{
			$invoicebill = '../uploads/invoicebill/'.$row[0]->invoice;
		}
		if($extensionbottle[0] == "pdf" || $extension[0] == "PDF"){
			$bottle = "../images/pdf-icon.png";
		}else{
			$bottle = '../uploads/bottleimage/'.$row[0]->bottleimage;
		}
		
		
		if($row[0]->invoice  != "../uploads/invoicebill/dummy.png" || !empty($row[0]->invoice)){
			$invoice = $row[0]->invoice;
		}else{
			$invoice = "";
		}
		if($row[0]->bottleimage != "dummy.png" || !empty($row[0]->bottleimage)){
			$bottleimage = $row[0]->bottleimage;
		}else{
			$bottleimage = "";
		}*/
		if(!empty($row[0]->city)){
			$city = $row[0]->city;
		}else{
			$city = "";
		}
		$html = '';
		$html = '<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="formrow-firstname-input">Name:  </label> <span>'.$row[0]->name.'</span>
							</div>
						</div>
						<div class="col-md-6">
								<div class="form-group">
									<label for="formrow-firstname-input">Age:  </label> <span>'.$row[0]->age.'</span>
								</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="formrow-firstname-input">Gender:  </label> <span>'.$row[0]->gender.'</span>
								</div>
							</div>
							<div class="col-md-6">
									<div class="form-group">
										<label for="formrow-firstname-input">Address:  </label> <span>'.$row[0]->address.'</span>
									</div>
							</div>
                    </div>
					<div class="row">	
						<div class="col-md-6">
							<div class="form-group">
								<label for="formrow-firstname-input">City:  </label> <span>'.$city.'</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="formrow-firstname-input">State:  </label> <span>'.$row[0]->state.'</span>
							</div>
						</div>
					</div>		
					<div class="row">	
						<div class="col-md-6">
								<div class="form-group">
									<label for="formrow-firstname-input">Mobile:  </label> <span>'.$row[0]->mobile.'</span>
								</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="formrow-firstname-input">Pincode:  </label> <span>'.$row[0]->pincode.'</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
								<div class="form-group">
									<label for="formrow-firstname-input">Doctor Name:  </label> <span>'.$row[0]->doctorname.'</span>
								</div>
						</div>
						<div class="col-md-6">
								<div class="form-group">
									 <label for="formrow-firstname-input">Doctor City:  </label> <span>'.$row[0]->doctorcity.'</span>
								</div>
						</div>
					</div>
				<div class="row">
								<div class="col-md-6">
										<div class="form-group">
											 <label for="formrow-firstname-input">Preferred Language:  </label> <sapn>'.$row[0]->userlanguage.'</span>
										</div>
								</div>
								<div class="col-md-6">
										<div class="form-group">
											 <label for="formrow-firstname-input">Created Date:  </label> <sapn>'.date("d-m-Y g:i A", strtotime($row[0]->created_at)).'</span>
										</div>
								</div>
							</div>
					<div class="row">
						<div class="col-md-6">
								<div class="form-group">';
								if(!empty($row[0]->prescription)){
									$html .= '<label for="formrow-firstname-input">Prescription:  </label></br><a href="'.$pre.'" title="Prescription" target="_blank"><img src="'.$pre.'" alt="prescription" style="height: 50px; width: 50px;"></a>';
								}else{
									$html .= '<label for="formrow-firstname-input">Prescription:  </label> -';
								}
							$html .= '</div>
						</div>
						<div class="col-md-6">
								<div class="form-group">';
								if(!empty($row[0]->invoice)){
									$html .= '<label for="formrow-firstname-input">Invoice/Bill:  </label></br><a href="'.$inv.'" title="Invoice bill" target="_blank"><img src="'.$inv.'" alt="prescription" style="height: 50px; width: 50px;"></a>';
								}else{
									$html .= '<label for="formrow-firstname-input">Invoice/Bill:  </label> -';
								}
							$html .= '</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-6">
								<div class="form-group">';
								if(!empty($row[0]->bottleimage)){
									$html .= '<label for="formrow-firstname-input">Bottle Purchased:  </label></br><a href="'.$bot.'" title="Image of purchased bottle" target="_blank"><img src="'.$bot.'" alt="prescription" style="height: 50px; width: 50px;"></a>';
								}else{
									$html .= '<label for="formrow-firstname-input">Bottle Purchased: </label> - ';
								}
							$html .= '</div>
						</div>
					</div>';
					
					
		return  $html;	
		
    }

    public function updatelink(Request $request)
    {
        $input = $request->all();
		
		$id = $input['id'];
		$name = $input['uname'];
		$link = $input['ulink'];
		$date1 = strtr($input['datearea'], '/', '-');;
		$dates= date("Y-m-d", strtotime($date1));
		$create = DB::table('users')->where('id', $id)->update([
					"name" => $name,
					"link" => $link,
					"expiryDate" => $dates
					]);
					
		$status = "updated";
		return response($status, 200);
    }
		public function export()
	{
			return Excel::download(new UserExport, 'carenshare-user-report'.date('dmyyhm').'.xlsx');
	}
	public function getcount(Request $request){
		$input = $request->all();
		$arr = explode("-",$input['range']);
		$fst = date("Y-m-d", strtotime($arr[0]));
		$first = $fst." 00:00:00";
		$snd = date("Y-m-d", strtotime($arr[1]));
		$second = $snd." 23:59:59";
		$rows = DB::table('users')->whereBetween( 'created_at', [$first, $second])->get();
		return count($rows);
	}


}
