<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Exports\DoctorExport;
use App\Doctor;
use Auth;
use Excel;
use DB;

class DoctorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		 $data = array('title'=>'Doctors','tbl'=>encrypt("doctors"));

        $data['offset'] = 0;
        if($request->page!=''){
            $data['offset'] = ((($request->page)-1)*config('custom_config.row_no'));
        }

        $data['result'] = Doctor::where('status','!=','Inactive');
        if($request->doctorname!=''){
            $data['result'] = $data['result']->where('doctorname', 'like', '%'.$request->doctorname.'%');
            $data['doctorname'] = $request->doctorname;
        }
        $data['result'] = $data['result']->paginate(config('custom_config.row_no'));
        $data['result'] = $data['result']->appends(['doctorname'=>$request->doctorname]);

        return view('admin.doctor',$data);
    }
	 public function download(Request $request)
    {
		 $data = array('title'=>'Doctors','tbl'=>encrypt("doctors"));

        $data['offset'] = 0;
        if($request->page!=''){
            $data['offset'] = ((($request->page)-1)*config('custom_config.row_no'));
        }

        $data['result'] = Doctor::where('status','!=','Inactive');
        if($request->doctorname!=''){
            $data['result'] = $data['result']->where('doctorname', 'like', '%'.$request->doctorname.'%');
            $data['doctorname'] = $request->doctorname;
        }
        $data['result'] = $data['result']->paginate(config('custom_config.row_no'));
        $data['result'] = $data['result']->appends(['doctorname'=>$request->doctorname]);

        return view('admin.download',$data);
    }
	public function completedoc(Request $request)
    {
		 $data = array('title'=>'Doctors','tbl'=>encrypt("doctors"));

        $data['offset'] = 0;
        if($request->page!=''){
            $data['offset'] = ((($request->page)-1)*config('custom_config.row_no'));
        }

        $data['result'] = Doctor::where('businesscardFlag','=','Complete');
        if($request->doctorEmail!=''){
            $data['result'] = $data['result']->where('doctorEmail', 'like', '%'.$request->doctorEmail.'%');
            $data['doctorEmail'] = $request->doctorEmail;
        }
        $data['result'] = $data['result']->paginate(config('custom_config.row_no'));
        $data['result'] = $data['result']->appends(['doctorEmail'=>$request->doctorEmail]);

        return view('admin.completedoc',$data);
    }
	public function pendingdoc(Request $request)
    {
		 $data = array('title'=>'Doctors','tbl'=>encrypt("doctors"));

        $data['offset'] = 0;
        if($request->page!=''){
            $data['offset'] = ((($request->page)-1)*config('custom_config.row_no'));
        }

        $data['result'] = Doctor::where('businesscardFlag','=','Pending');
        if($request->doctorEmail!=''){
            $data['result'] = $data['result']->where('doctorEmail', 'like', '%'.$request->doctorEmail.'%');
            $data['doctorEmail'] = $request->doctorEmail;
        }
        $data['result'] = $data['result']->paginate(config('custom_config.row_no'));
        $data['result'] = $data['result']->appends(['doctorEmail'=>$request->doctorEmail]);

        return view('admin.pendingdoc',$data);
    }
	public function viewdata(Request $request)
    {
		
		$input = $request->all();
		
		$id = $input['id'];
		
		$row = DB::table('doctor')
			->select('doctor.*')
			->where('id',$id) 
            ->get();
		$doctorid = $row[0]->id;
		$result = DB::table('clinic')
			->select('clinic.*')
			->where('doctorid',$doctorid) 
            ->get();	
		
		if($row[0]->status == "Active"){
			$status = "Active";
		}else{
			$status = "Inactive";
		}
		$html = '';
		$html = '<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="formrow-firstname-input">Name:  </label> <span>'.$row[0]->doctorFname.' '.$row[0]->doctorLname.'</span>
							</div>
						</div>
						<div class="col-md-6">
								<div class="form-group">
									<label for="formrow-firstname-input">Email:  </label> <span>'.$row[0]->doctorEmail.'</span>
								</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="formrow-firstname-input">Mobile:  </label> <span>'.$row[0]->doctorMobile.'</span>
								</div>
							</div>
							<div class="col-md-6">
									<div class="form-group">
										<label for="formrow-firstname-input">Whatsapp:  </label> <span>'.$row[0]->doctorWhatsapp.'</span>
									</div>
							</div>
                    </div>
					<div class="row">	
						<div class="col-md-6">
							<div class="form-group">
								<label for="formrow-firstname-input">Education:  </label> <span>'.$row[0]->education.'</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="formrow-firstname-input">Specialization:  </label> <span>'.$row[0]->doctorSpecialization.'</span>
							</div>
						</div>
					</div>		
					<div class="row">	
						<div class="col-md-6">
								<div class="form-group">
									<label for="formrow-firstname-input">Experience:  </label> <span>'.$row[0]->doctorExperience.'</span>
								</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="formrow-firstname-input">Doc:  </label> <span>'.$row[0]->businesscardFlag.'</span>
							</div>
						</div>
					</div>
					<div class="row">	
						<div class="col-md-6">
								<div class="form-group">
									<label for="formrow-firstname-input">Website:  </label> <span>'.$row[0]->weburl.'</span>
								</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="formrow-firstname-input">SMS No:  </label> <span>'.$row[0]->smsnumber.'</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
								<div class="form-group">
									<label for="formrow-firstname-input">Created Date:  </label> <span>'.date("d-m-Y g:i A", strtotime($row[0]->created_at)).'</span>
								</div>
						</div>
						<div class="col-md-6">
								<div class="form-group">
									 <label for="formrow-firstname-input">Profile Picture  </label></br><img src="../uploads/doctorpic/'.$row[0]->doctorProfilepicture.'" alt="profile picture" style="height: 50px; width: 50px;">
								</div>
						</div>
					</div>';
			if(count($result)>0) {		
					$html .= '<div class="row">
						  <div class="col-md-12 ">
							<div class="card card-secondary ">
							  <div class="card-header">
								<h3 class="card-title">Clinic Detail</h3>
							  </div>
							  <!-- /.card-header -->
							  <div class="card-body table-responsive p-0">
								<table class="table table-hover">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Clinic Name</th>
									  <th>Address</th>
									  <th>City</th>
									  <th>Pincode</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>';
								for($i=0;$i<count($result);$i++){
									$j = $i+1;
								   $html .= '<tr>
									  <td>'.$j.'</td>
									  <td>'.$result[$i]->clinicname.'</td>
									  <td>'.$result[$i]->address_line_1.'</td>
									  <td>'.$result[$i]->cityid.'</td>
									  <td>'.$result[$i]->pincode.'</td>
									  <td>
									  <a href="javascript:void(0);" id="'.$result[$i]->id.'" class="schBtn" title="clinic schedule"><i class="fa fa-hospital-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
									</td></tr>';
								}
								
							  $html .= '</tbody>
								</table>
							  </div>
							</div>
						  </div>
						  </div>';
			}
		return  $html;	
		
    }
	public function viewschedule(Request $request)
    {
		
		$input = $request->all();
		
		$id = $input['id'];
		$result = DB::table('clinictiming')
			->select('clinictiming.*')
			->where('clinicid',$id) 
            ->get();
		/* Sunday*/	
		if(!empty($result[0]->sunmorning)){
			$Sunday = 'Sunday';
			$time = 'Morning';
		}else{
			$Sunday = "-";
			$time = "-";
		}
		if(!empty($result[0]->sun_morning_starttime) && ($result[0]->sun_morning_starttime != "00:00")){
			$sun_morning_starttime = date("g:i A", strtotime($result[0]->sun_morning_starttime));
		}else{
			$sun_morning_starttime  = "-";
		}
		if(!empty($result[0]->sun_morning_endtime) && ($result[0]->sun_morning_endtime != "00:00")){
			$sun_morning_endtime = date("g:i A", strtotime($result[0]->sun_morning_endtime));
		}else{
			$sun_morning_endtime  = "-";
		}	
		if(!empty($result[0]->sunevening)){
			$Sunday = 'Sunday';
			$time = 'Evening';
		}else{
			$Sunday = "-";
			$time = "-";
		}
		if(!empty($result[0]->sun_evening_starttime)  && ($result[0]->sun_evening_starttime != "00:00")){
			$sun_evening_starttime = date("g:i A", strtotime($result[0]->sun_evening_starttime));
		}else{
			$sun_evening_starttime  = "-";
		}
		if(!empty($result[0]->sun_evening_endtime)  && ($result[0]->sun_evening_endtime != "00:00")){
			$sun_evening_endtime = date("g:i A", strtotime($result[0]->sun_evening_endtime));
		}else{
			$sun_evening_endtime  = "-";
		}
		/* Monday */
		if(!empty($result[0]->monmorning)){
			$Monday = 'Monday';
			$time = 'Morning';
		}else{
			$Monday = "-";
			$time = "-";
		}
		if(!empty($result[0]->mon_morning_starttime) && ($result[0]->mon_morning_starttime != "00:00")){
			$mon_morning_starttime =  date("g:i A", strtotime($result[0]->mon_morning_starttime));
		}else{
			$mon_morning_starttime  = "-";
		}
		if(!empty($result[0]->mon_morning_endtime)  && ($result[0]->mon_morning_endtime != "00:00")){
			$mon_morning_endtime = date("g:i A", strtotime($result[0]->mon_morning_endtime));
		}else{
			$mon_morning_endtime  = "-";
		}	
		if(!empty($result[0]->monevening)){
			$Monday = 'Monday';
			$time = 'Evening';
		}else{
			$Sunday = "-";
			$time = "-";
		}
		if(!empty($result[0]->mon_evening_starttime) && ($result[0]->mon_evening_starttime != "00:00")){
			$mon_evening_starttime = date("g:i A", strtotime($result[0]->mon_evening_starttime));
		}else{
			$mon_evening_starttime  = "-";
		}
		if(!empty($result[0]->mon_evening_endtime)  && ($result[0]->mon_evening_endtime != "00:00")){
			$mon_evening_endtime = date("g:i A", strtotime($result[0]->mon_evening_endtime));
		}else{
			$mon_evening_endtime  = "-";
		}
		
		/* Tuesday */
		if(!empty($result[0]->tuemorning)){
			$Tuesday = 'Tuesday';
			$time = 'Morning';
		}else{
			$Tuesday = "-";
			$time = "-";
		}
		if(!empty($result[0]->tue_morning_starttime) && ($result[0]->tue_morning_starttime != "00:00")){
			$tue_morning_starttime = date("g:i A", strtotime($result[0]->tue_morning_starttime)); 
		}else{
			$tue_morning_starttime  = "-";
		}
		if(!empty($result[0]->tue_morning_endtime) && ($result[0]->tue_morning_endtime != "00:00")){
			$tue_morning_endtime = date("g:i A", strtotime($result[0]->tue_morning_endtime));
		}else{
			$tue_morning_endtime  = "-";
		}	
		if(!empty($result[0]->tueevening)){
			$Tuesday = 'Tuesday';
			$time = 'Evening';
		}else{
			$Tuesday = "-";
			$time = "-";
		}
		if(!empty($result[0]->tue_evening_starttime) && ($result[0]->tue_evening_starttime != "00:00")){
			$tue_evening_starttime = date("g:i A", strtotime($result[0]->tue_evening_starttime));
		}else{
			$tue_evening_starttime  = "-";
		}
		if(!empty($result[0]->tue_evening_endtime) && ($result[0]->tue_evening_endtime != "00:00")){
			$tue_evening_endtime = date("g:i A", strtotime($result[0]->tue_evening_endtime));
		}else{
			$tue_evening_endtime  = "-";
		}
		
		/* Wensday */
		if(!empty($result[0]->wenmorning)){
			$Wensday = 'Wensday';
			$time = 'Morning';
		}else{
			$Wensday = "-";
			$time = "-";
		}
		if(!empty($result[0]->wen_morning_starttime) && ($result[0]->wen_morning_starttime != "00:00")){
			$wen_morning_starttime = date("g:i A", strtotime($result[0]->wen_morning_starttime)); 
		}else{
			$wen_morning_starttime  = "-";
		}
		if(!empty($result[0]->wen_morning_endtime) && ($result[0]->wen_morning_endtime != "00:00")){
			$wen_morning_endtime = date("g:i A", strtotime($result[0]->wen_morning_endtime));
		}else{
			$wen_morning_endtime  = "-";
		}	
		if(!empty($result[0]->wenevening)){
			$Wensday = 'Wensday';
			$time = 'Evening';
		}else{
			$Wensday = "-";
			$time = "-";
		}
		if(!empty($result[0]->wen_evening_starttime) && ($result[0]->wen_evening_starttime != "00:00")){
			$wen_evening_starttime = date("g:i A", strtotime($result[0]->wen_evening_starttime));
		}else{
			$wen_evening_starttime  = "-";
		}
		if(!empty($result[0]->wen_evening_endtime) && ($result[0]->wen_evening_endtime != "00:00")){
			$wen_evening_endtime = date("g:i A", strtotime($result[0]->wen_evening_endtime));
		}else{
			$wen_evening_endtime  = "-";
		}
		
		/* Thursday */
		if(!empty($result[0]->thumorning)){
			$Thursday = 'Thursday';
			$time = 'Morning';
		}else{
			$Thursday = "-";
			$time = "-";
		}
		if(!empty($result[0]->thu_morning_starttime) && ($result[0]->thu_morning_starttime != "00:00")){
			$thu_morning_starttime = date("g:i A", strtotime($result[0]->thu_morning_starttime)); 
		}else{
			$thu_morning_starttime  = "-";
		}
		if(!empty($result[0]->thu_morning_endtime) && ($result[0]->thu_morning_endtime != "00:00")){
			$thu_morning_endtime = date("g:i A", strtotime($result[0]->thu_morning_endtime));
		}else{
			$thu_morning_endtime  = "-";
		}	
		if(!empty($result[0]->thuevening)){
			$Thursday = 'Thursday';
			$time = 'Evening';
		}else{
			$Thursday = "-";
			$time = "-";
		}
		if(!empty($result[0]->thu_evening_starttime) && ($result[0]->thu_evening_starttime != "00:00")){
			$thu_evening_starttime = date("g:i A", strtotime($result[0]->thu_evening_starttime));
		}else{
			$thu_evening_starttime  = "-";
		}
		if(!empty($result[0]->thu_evening_endtime) && ($result[0]->thu_evening_endtime != "00:00")){
			$thu_evening_endtime = date("g:i A", strtotime($result[0]->thu_evening_endtime));
		}else{
			$thu_evening_endtime  = "-";
		}

		/* Friday */
		if(!empty($result[0]->frimorning)){
			$Friday = 'Friday';
			$time = 'Morning';
		}else{
			$Friday = "-";
			$time = "-";
		}
		if(!empty($result[0]->fri_morning_starttime) && ($result[0]->fri_morning_starttime != "00:00")){
			$fri_morning_starttime = date("g:i A", strtotime($result[0]->fri_morning_starttime)); 
		}else{
			$fri_morning_starttime  = "-";
		}
		if(!empty($result[0]->fri_morning_endtime) && ($result[0]->fri_morning_endtime != "00:00")){
			$fri_morning_endtime = date("g:i A", strtotime($result[0]->fri_morning_endtime));
		}else{
			$fri_morning_endtime  = "-";
		}	
		if(!empty($result[0]->frievening)){
			$Friday = 'Friday';
			$time = 'Evening';
		}else{
			$Friday = "-";
			$time = "-";
		}
		if(!empty($result[0]->fri_evening_starttime) && ($result[0]->fri_evening_starttime != "00:00")){
			$fri_evening_starttime = date("g:i A", strtotime($result[0]->fri_evening_starttime));
		}else{
			$fri_evening_starttime  = "-";
		}
		if(!empty($result[0]->fri_evening_endtime) && ($result[0]->fri_evening_endtime != "00:00")){
			$fri_evening_endtime = date("g:i A", strtotime($result[0]->fri_evening_endtime));
		}else{
			$fri_evening_endtime  = "-";
		}
		
		/* Saterday */
		if(!empty($result[0]->satmorning)){
			$Saterday = 'Saterday';
			$time = 'Morning';
		}else{
			$Friday = "-";
			$time = "-";
		}
		if(!empty($result[0]->sat_morning_starttime) && ($result[0]->sat_morning_starttime != "00:00")){
			$sat_morning_starttime = date("g:i A", strtotime($result[0]->sat_morning_starttime)); 
		}else{
			$sat_morning_starttime  = "-";
		}
		if(!empty($result[0]->sat_morning_endtime) && ($result[0]->sat_morning_endtime != "00:00")){
			$sat_morning_endtime = date("g:i A", strtotime($result[0]->sat_morning_endtime));
		}else{
			$sat_morning_endtime  = "-";
		}	
		if(!empty($result[0]->satevening)){
			$Saterday = 'Saterday';
			$time = 'Evening';
		}else{
			$Friday = "-";
			$time = "-";
		}
		if(!empty($result[0]->sat_evening_starttime) && ($result[0]->sat_evening_starttime != "00:00")){
			$sat_evening_starttime = date("g:i A", strtotime($result[0]->sat_evening_starttime));
		}else{
			$sat_evening_starttime  = "-";
		}
		if(!empty($result[0]->sat_evening_endtime) && ($result[0]->sat_evening_endtime != "00:00")){
			$sat_evening_endtime = date("g:i A", strtotime($result[0]->sat_evening_endtime));
		}else{
			$sat_evening_endtime  = "-";
		}
		
		$html = '';
			if(count($result)>0) {		
					$html .= '<div class="row">
						  <div class="col-md-12 ">
							<div class="card card-secondary ">
							  <div class="card-header">
								<h3 class="card-title">Clinic Schedule Detail</h3>
							  </div>
							  <!-- /.card-header -->
							  <div class="card-body table-responsive p-0">
								<table class="table table-hover">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Day</th>
									  <th>Morning Start Time</th>
									  <th>Morning End Time</th>
									  <th>Evening Start Time</th>
									  <th>Evening End Time</th>
									</tr>
								  </thead>
								  <tbody>
									<tr>
									  <td>1</td>
									  <td>'.$Sunday.'</td>
									  <td>'.$sun_morning_starttime.'</td>
									  <td>'.$sun_morning_endtime.'</td>
									  <td>'.$sun_evening_starttime.'</td>
									  <td>'.$sun_evening_endtime.'</td>
									</tr>
									<tr>
									  <td>2</td>
									  <td>'.$Monday.'</td>
									  <td>'.$mon_morning_starttime.'</td>
									  <td>'.$mon_morning_endtime.'</td>
									  <td>'.$mon_evening_starttime.'</td>
									  <td>'.$mon_evening_endtime.'</td>
									</tr>
									<tr>
									  <td>3</td>
									  <td>'.$Tuesday.'</td>
									  <td>'.$tue_morning_starttime.'</td>
									  <td>'.$tue_morning_endtime.'</td>
									  <td>'.$tue_evening_starttime.'</td>
									  <td>'.$tue_evening_endtime.'</td>
									</tr>
									<tr>
									  <td>4</td>
									  <td>'.$Wensday.'</td>
									  <td>'.$wen_morning_starttime.'</td>
									  <td>'.$wen_morning_endtime.'</td>
									  <td>'.$wen_evening_starttime.'</td>
									  <td>'.$wen_evening_endtime.'</td>
									</tr>
									<tr>
									  <td>5</td>
									  <td>'.$Thursday.'</td>
									  <td>'.$thu_morning_starttime.'</td>
									  <td>'.$thu_morning_endtime.'</td>
									  <td>'.$thu_evening_starttime.'</td>
									  <td>'.$thu_evening_endtime.'</td>
									</tr>
									<tr>
									  <td>6</td>
									  <td>'.$Friday.'</td>
									  <td>'.$fri_morning_starttime.'</td>
									  <td>'.$fri_morning_endtime.'</td>
									  <td>'.$fri_evening_starttime.'</td>
									  <td>'.$fri_evening_endtime.'</td>
									</tr>
									<tr>
									  <td>7</td>
									  <td>'.$Saterday.'</td>
									  <td>'.$sat_morning_starttime.'</td>
									  <td>'.$sat_morning_endtime.'</td>
									  <td>'.$sat_evening_starttime.'</td>
									  <td>'.$sat_evening_endtime.'</td>
									</tr>
									</tbody>
								</table>
							  </div>
							</div>
						  </div>
						  </div>';
			}
		return  $html;	
		
    }
	public function linkdata(Request $request)
    {
		$input = $request->all();
		$id = $input['id'];
		$result = DB::table('link')
			->where('doctorid',$id) 
            ->get();
		$html = '';
		$html = '<div class="row">
						<div class="form-group col-md-12">
							<input type="text" name="copyTarget" readonly class="form-control" id="copyTarget" value="'.$result[0]->linkurl.'">
						  </div>
					</div>';
		return  $html;
	}
	public function export()
	{
			return Excel::download(new DoctorExport, 'doctor_report.xlsx');
	}


   
}
