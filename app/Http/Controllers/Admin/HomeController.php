<?php

namespace App\Http\Controllers\Admin;

use App\Exports\DashboardExport;
use App\HqMaster;
use Illuminate\Http\Request;
use App\Doctor;
use App\User;
use App\Product;
use App\Speciality;
use App\DoctorPotential;
use App\DoctorSupport;
use App\DoctorSpeciality;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use DB;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    

    public function index()
    {
		
		$currentdate = date("m/d/Y");
		$fst = date( "Y-m-d", strtotime( "$currentdate -7 day" ) );
		$first = $fst." 00:00:00";
		$beforeseven = date("Y-m-d", strtotime($currentdate));
		$snd = date("Y-m-d");
		$second = $snd." 23:59:59";
		 
		$users = DB::table("users")
				->whereBetween('created_at', array($fst.' 00:00:00', $snd.' 23:59:59'))
				->get();
		return view('admin.home', ['usercount' => count($users)]);
    }

    
}
