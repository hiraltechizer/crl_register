<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Doctor;
use App\Customlink;
use App\User;
use App\Imports\ImportDoctors;
use Auth;
use Excel;

class CustomlinkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		
        $data = array('title'=>'Customlink','tbl'=>encrypt("customlink"));

        $data['offset'] = 0;
        if($request->page!=''){
            $data['offset'] = ((($request->page)-1)*config('custom_config.row_no'));
        }

        $data['result'] = Customlink::where('status','==','Yes');
        if($request->fullname!=''){
            $data['result'] = $data['result']->where('fullname', 'like', '%'.$request->fullname.'%');
            $data['fullname'] = $request->fullname;
        }

        

        $data['result'] = $data['result']->paginate(config('custom_config.row_no'));
        $data['result'] = $data['result']->appends(['fullname'=>$request->fullname]);

        // GET DATA FOR FILTER 
        $filter_data =  Customlink::where('status','==','Yes')->get();
        
        return view('admin.customlink',$data);
    }

    public function add()
    {
        
    }

    public function importDoctors(Request $request)
    {
        
    }

    public function information($id)
    {
        
    }

}
