<?php
namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Admin;
use App\User;
use Auth;
use Config;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin')->except('logoutadmin');
    }

    public function showLoginForm()
    {
		$arr =  explode("/",url()->current());
		if($arr[4] == "login"){
				$check_login_flag = '';
				if (Auth::guard('admin')->check()) {
				
					$check_login_flag = 'admin';
				}
				if (Auth::guard('web')->check()) {
					
					$check_login_flag = 'web';
				}
				if ($check_login_flag!='') {
				
					return redirect('home');
				}
				$data = ['title'=>'Login'];
				return view('adminlte::login',$data);
		}
		if($arr[4] == "users"){
			return redirect('/admin/login');
		}
}
	
	

    public function login(Request $request)
    {
		$input = $request->all();
		
        $check_login_flag = '';
		
        if (Auth::guard('admin')->check()) {
			$check_login_flag = 'admin';
		}
		if (Auth::guard('web')->check()) {
			$check_login_flag = 'web';
        }
       
        if ($check_login_flag!='') {
			return redirect('home');
        }

        $rules = [
            'email' => 'required|max:255',
            'password' => 'required',
        ];
        $this->validate($request,$rules);
		
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
			
            $get_user = Admin::where('email','=',$request->email)->first();

            if(!empty($get_user)){
				$guard = 'admin';
                $credentials = [
                    'email' => $request->email,
                    'password' => $request->password,
                ];
				
                if (Auth::guard($guard)->attempt($credentials)) {
					$user = Auth::guard($guard)->user();
                    if($user->status=='disable'){
						auth($guard)->logout();
                        return redirect('/login')->with('error', 'Your account is disable by administrator.');
                    }
                    else if($user->status=='delete'){
						auth($guard)->logout();
                        return redirect('/login')->with('error', "Account don't exists.");
                    }
                    else{
						// Authentication passed...
                        Config::set('custom_config.login_type', $guard);
                        return redirect('admin/home');
                    }
                }else{
					auth($guard)->logout();
                    return redirect('/login')->with('error', "Password is incorrect.");
                }
            }else{
				return redirect('/login')->with('error', "Account don't exists.");
            }
        } 
        else { 
				
		    
            $get_user = Admin::where('email','=',$request->email)->first();

            if(!empty($get_user)){
				$guard = 'admin';
                $credentials = [
                    'email' => $request->email,
                    'password' => $request->password,
                ];
				
                if (Auth::guard($guard)->attempt($credentials)) {
					$user = Auth::guard($guard)->user();
                    if($user->status=='disable'){
						auth($guard)->logout();
                        return redirect('/admin/login')->with('error', 'Your account is disable by administrator.');
                    }
                    else if($user->status=='delete'){
						auth($guard)->logout();
                        return redirect('/admin/login')->with('error', "Account don't exists.");
                    }
                    else{
						// Authentication passed...
                        Config::set('custom_config.login_type', $guard);
                        return redirect('admin/home');
                    }
                }else{
					auth($guard)->logout();
                    return redirect('/admin/login')->with('error', "Password is incorrect.");
                }
            }else{
				return redirect('/admin/login')->with('error', "Account don't exists.");
            }
        } 
    }

    public function logoutAdmin(Request $request)
    {
		
		Auth::guard(config('custom_config.login_type'))->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('admin/login');
		
		/*$this->guard()->logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect()->back();*/
    }
}
