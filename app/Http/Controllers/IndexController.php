<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\User;
//use App\Model\CommonModel;
use Auth;
use Excel;
use Illuminate\Support\Carbon;
use DB;
use Config;
@session_start();
class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function indexform()
    {
		$states = DB::table('states')->orderBy('state_name', 'ASC')->get();
		$language = DB::table('langugaefile')->where('type','=','video')->orderBy('id', 'ASC')->get();
		return view('carenshare.index', ['states' => $states, 'language'=>$language]);
    }
	public function store(Request $request)
    {
		// dd($request);	
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {  
			 $http = "https://";   
		}else{  
         $http = "http://";
		}	 
		$url = $http.$_SERVER['HTTP_HOST']; 
        
	 
	   $input = $request->all();
	  
		$arr = explode(" ",$input['name']);
		if(!empty($arr[0]) && !empty($arr[1])){
			$fname = $arr[0].'-'.$arr[1];
		}else if(!empty($arr[0]) && empty($arr[1])){
			$fname = $arr[0];
		}
		if(!empty($input['inputGroupFile01'])){
				$destinationPrescription = public_path().'/uploads/prescription/';
			if ($prescription = $request->file('inputGroupFile01')) {
			 $prescription_detail = 'pre-'.$fname.'-'.$input['mobile'].'.'.$prescription->getClientOriginalExtension();
             $prescription->move($destinationPrescription, $prescription_detail);
			 $priurl = $url."/uploads/prescription/".$prescription_detail;
			 }
        }else{
			$priurl = "";
			$prescription_detail = "";
		}
		if(!empty($input['inputGroupFile02'])){
			$destinationPath1 = public_path().'/uploads/invoicebill/';
			if ($cover_detail_image1 = $request->file('inputGroupFile02')) {
				//$cover_detail1 = md5(time().'_'.$cover_detail_image1->getClientOriginalName()).'.'.$cover_detail_image1->getClientOriginalExtension();
				$cover_detail1 = 'bill-'.$fname.'-'.$input['mobile'].'.'.$cover_detail_image1->getClientOriginalExtension();
				$cover_detail_image1->move($destinationPath1, $cover_detail1);
				if(!empty($cover_detail1)){ 
					$invurl = $url."/uploads/invoicebill/".$cover_detail1;
				}else{
					$invurl = "";
				}
				
			}
		}else{
			$invurl = "";
			$cover_detail1 = "";
		}	
			
		if(!empty($input['inputGroupFile03'])){
			
			$destinationPath = public_path().'/uploads/bottleimage/';
			if ($cover_detail_image = $request->file('inputGroupFile03')) {
				 $cover_detail = 'pack-'.$fname.'-'.$input['mobile'].'.'.$cover_detail_image->getClientOriginalExtension();;
				 $cover_detail_image->move($destinationPath, $cover_detail);
				
				if(!empty($cover_detail)){ 
					$btlurl = $url."/uploads/bottleimage/".$cover_detail;
				}else{
					$btlurl = "";
					$cover_detail = "";
				}
			}
			
		}else{
			$btlurl = "";
			$cover_detail = "";
		}
		
		/*$destinationPath2 = public_path().'/uploads/bottle/';
		if ($cover_detail_image2 = $request->file('inputGroupFile03')) {
            $cover_detail2 = md5(time().'_'.$cover_detail_image2->getClientOriginalName()).'.'.$cover_detail_image2->getClientOriginalExtension();
            $cover_detail_image1->move($destinationPath2, $cover_detail2);
        }*/
	   $name = $input['name'];
	   $age = $input['age'];
	   $gender = $input['gender'];
	   $address = $input['address'];
	   $city = $input['city'];
	   $state = $input['state'];
	   $mobile = $input['mobile'];
	   $pincode = $input['pincode'];
	   $doctorname = $input['doctorname'];
	   $doctorcity = $input['doctorcity'];
	   $langugae = $input['langugae'];
	  
		$values = array(
            'name' =>$name,
            'age' => $age,
            'gender' => $gender,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'mobile' => $mobile,
            'pincode' => $pincode,
            'doctorname' => $doctorname,
            'doctorcity' => $doctorcity,
            'userlanguage' => $langugae,
            'prescription' => $prescription_detail,
            'invoice' => $cover_detail1,
            'bottleimage' => $cover_detail,
            'preurl' => $priurl,
            'invurl' => $invurl,
            'btlurl' => $btlurl,
            'status' => "Active",
            'created_at' => date("Y-m-d H:i:s")
        );
		
		$insertdata = DB::table('users')->insert($values);
		// $mobile_exist = DB::connection('mysql2')->table('patient')->where('mobile_1',$mobile)->value('patient_id');
		// $language_id= $this->get_language($langugae);

		//================New patient=======================//

		// if(empty($mobile_exist))
		// {
		// 	$patient_code = $this->get_patient_unique_code();
		// 	$values1 = array(
        //     'name_1' =>$name,
        //     'patient_age' => $age,
        //     'patient_gender' => $gender,
        //     'mobile_1' => $mobile,
        //     'patient_code'=>$patient_code,
        //     'lang_code' =>$language_id,
        //     'insert_dt' => date("Y-m-d H:i:s"),
        //     'update_dt' => date("Y-m-d H:i:s")
        // 	);
		// 	$patient_id = DB::connection('mysql2')->table('patient')->insertGetId($values1);
		// 	if($patient_id)
		// 	{
		// 		$doc_value = array(
		// 			'doc_name'=>$doctorname,
		// 			'insert_dt'=>date("Y-m-d H:i:s"),
		// 			'update_dt'=>date("Y-m-d H:i:s")
		// 		);
		// 		$doctor_id = DB::connection('mysql2')->table('doctor')->insertGetId($doc_value);

		// 		$city_exist = DB::connection('mysql2')->table('city')->where('city_name',$doctorcity)->value('city_id');
		// 		if(!empty($city_exist))
		// 		{
		// 			$city_id = $city_exist;
		// 			$get_mr_id = DB::connection('mysql2')->table('manpower')->where('users_city_id',$city_id)->where('users_division_id',2)->value('users_id');
		// 			if(!empty($get_mr_id)){
		// 			$mr_id = $get_mr_id;
		// 			}else{
		// 				$mr_id = '';
		// 			}

		// 			$doc_mr=array(
		// 				'doc_id'=>$doctor_id,
		// 				'city_id'=>$city_id,
		// 				'mr_id'=>$mr_id,
		// 				'division_id'=>2,
		// 				'insert_dt'=>date("Y-m-d H:i:s"),
		// 				'update_dt'=>date("Y-m-d H:i:s")
		// 			);
		// 			$doc_mr_id = DB::connection('mysql2')->table('doctor_mr')->insertGetId($doc_mr);
		// 		}

		// 		//================insert address==================//

		// 		$state_id= $this->get_state($state);
		// 		$city_exist_id = $this->get_cities($city);

		// 		if(empty($city_exist_id))
		// 		{
		// 			$cities = array(
		// 				'city_name'=>$city,
		// 				'added_by'=>'patient',
		// 				'insert_dt'=>date("Y-m-d H:i:s"),
		// 				'update_dt'=>date("Y-m-d H:i:s")
		// 			);
		// 			$cities_id=DB::connection('mysql2')->table('cities')->insertGetId($cities);
		// 		}
		// 		else{
		// 			$cities_id = $city_exist_id;
		// 		}
		// 		$address_data = array(
		// 			'patient_id'=>$patient_id,
		// 			'state'=>$state_id,
		// 			'city'=>$cities_id,
		// 			'pincode'=>$pincode,
		// 			'address_line'=>$address,
		// 			'insert_dt'=>date("Y-m-d H:i:s"),
		// 			'update_dt'=>date("Y-m-d H:i:s")
		// 			);
		// 		$address_id=DB::connection('mysql2')->table('address')->insertGetId($address_data);
		// 		//==================address end===================//

		// 		$patient_tag_data=array(
		// 			'patient_id'=>$patient_id,
		// 			'division_id'=>2,
		// 			'doc_id'=>$doctor_id,
		// 			'register_by'=>'patient',
		// 			'prescription'=>$priurl,
		// 			'invoice'=>$invurl,
		// 			'bottle_purchase'=>$btlurl,
		// 			'register_status'=>'Active',
		// 			'insert_dt'=>date("Y-m-d H:i:s"),
		// 			'update_dt'=>date("Y-m-d H:i:s")
		// 		);

		// 		$patient_brand_tag=DB::connection('mysql2')->table('patient_brand_tag')->insertGetId($patient_tag_data);

		// 		$enroll_data=array(
		// 			'patient_id'=>$patient_id,
		// 			'division_id'=>2,
		// 			'enroll_type'=>'qrcode',
		// 			'insert_dt'=>date("Y-m-d H:i:s"),
		// 			'update_dt'=>date("Y-m-d H:i:s")
		// 		);

		// 		$patient_enroll = DB::connection('mysql2')->table('patient_enroll_way')->insertGetId($enroll_data);

		// 		$call_history_data= ['mobile'=>$mobile,'patient_brand_tag_id'=>$patient_brand_tag,'insert_dt'=>date("Y-m-d H:i:s"),'update_dt'=>date("Y-m-d H:i:s")];

		// 		$call_history = DB::connection('mysql2')->table('call_history')->insertGetId($call_history_data);
		// 	}
		// }
		// //====================old patient=======================//
		// else{

		// 	$patient_id = $mobile_exist;
		// 	$patient_exist = $this->patient_tag_exist($patient_id);
		// 	//==============Tagging not exist==================//
		// 	if(empty($patient_exist))
		// 	{
		// 		$doc_value = array(
		// 			'doc_name'=>$doctorname,
		// 			'insert_dt'=>date("Y-m-d H:i:s"),
		// 			'update_dt'=>date("Y-m-d H:i:s")
		// 		);
		// 		$doctor_id = DB::connection('mysql2')->table('doctor')->insertGetId($doc_value);

		// 		$city_exist = DB::connection('mysql2')->table('city')->where('city_name',$doctorcity)->value('city_id');
		// 		if(!empty($city_exist))
		// 		{
		// 			$city_id = $city_exist;
		// 			$get_mr_id = DB::connection('mysql2')->table('manpower')->where('users_city_id',$city_id)->where('users_division_id',2)->value('users_id');
		// 			if(!empty($get_mr_id)){
		// 			$mr_id = $get_mr_id;
		// 			}else{
		// 				$mr_id = '';
		// 			}

		// 			$doc_mr=array(
		// 				'doc_id'=>$doctor_id,
		// 				'city_id'=>$city_id,
		// 				'mr_id'=>$mr_id,
		// 				'division_id'=>2,
		// 				'insert_dt'=>date("Y-m-d H:i:s"),
		// 				'update_dt'=>date("Y-m-d H:i:s")
		// 			);
		// 			$doc_mr_id = DB::connection('mysql2')->table('doctor_mr')->insertGetId($doc_mr);
		// 		}

		// 		$patient_tag_data=array(
		// 			'patient_id'=>$patient_id,
		// 			'division_id'=>2,
		// 			'doc_id'=>$doctor_id,
		// 			'register_by'=>'patient',
		// 			'prescription'=>$priurl,
		// 			'invoice'=>$invurl,
		// 			'bottle_purchase'=>$btlurl,
		// 			'register_status'=>'Active',
		// 			'insert_dt'=>date("Y-m-d H:i:s"),
		// 			'update_dt'=>date("Y-m-d H:i:s")
		// 		);

		// 		$patient_brand_tag=DB::connection('mysql2')->table('patient_brand_tag')->insertGetId($patient_tag_data);

		// 		$enroll_data=array(
		// 			'patient_id'=>$patient_id,
		// 			'division_id'=>2,
		// 			'enroll_type'=>'qrcode',
		// 			'insert_dt'=>date("Y-m-d H:i:s"),
		// 			'update_dt'=>date("Y-m-d H:i:s")
		// 		);

		// 		$patient_enroll = DB::connection('mysql2')->table('patient_enroll_way')->insertGetId($enroll_data);

		// 		$call_history_data= ['mobile'=>$mobile,'patient_brand_tag_id'=>$patient_brand_tag,'insert_dt'=>date("Y-m-d H:i:s"),'update_dt'=>date("Y-m-d H:i:s")];

		// 		$call_history = DB::connection('mysql2')->table('call_history')->insertGetId($call_history_data);
		// 	}

		// 	//===================tagging exist end ================//

		// 	else{
		// 		$is_registered = $this->patient_register_exist($patient_id);
		// 		if(!empty($is_registered))
		// 		{
		// 			$patient_brand_tag_id = $patient_exist;
		// 			$query1=DB::connection('mysql2')->table('patient')->where('mobile_1', $mobile)->update(['name_1'=>$name,'patient_gender'=>$gender, 'patient_age'=>$age,'lang_code' =>$language_id,'update_dt'=>date("Y-m-d H:i:s")]);

		// 			$doc_value = array(
		// 				'doc_name'=>$doctorname,
		// 				'insert_dt'=>date("Y-m-d H:i:s"),
		// 				'update_dt'=>date("Y-m-d H:i:s")
		// 				);
		// 			$doctor_id = DB::connection('mysql2')->table('doctor')->insertGetId($doc_value);

		// 			$city_exist = DB::connection('mysql2')->table('city')->where('city_name',$doctorcity)->value('city_id');
		// 			if(!empty($city_exist))
		// 			{
		// 				$city_id = $city_exist;
		// 				$get_mr_id = DB::connection('mysql2')->table('manpower')->where('users_city_id',$city_id)->where('users_division_id',2)->value('users_id');
		// 				if(!empty($get_mr_id)){
		// 				$mr_id = $get_mr_id;
		// 				}else{
		// 					$mr_id = '';
		// 				}

		// 				$doc_mr=array(
		// 					'doc_id'=>$doctor_id,
		// 					'city_id'=>$city_id,
		// 					'mr_id'=>$mr_id,
		// 					'division_id'=>2,
		// 					'insert_dt'=>date("Y-m-d H:i:s"),
		// 					'update_dt'=>date("Y-m-d H:i:s")
		// 				);
		// 				$doc_mr_id = DB::connection('mysql2')->table('doctor_mr')->insertGetId($doc_mr);
		// 			}

		// 			$patient_address_exist= $this->patient_address_exist($patient_id);
		// 			//===================Address================//
		// 			if(empty($patient_address_exist))
		// 			{
		// 				$state_id= $this->get_state($state);
		// 				$city_exist_id = $this->get_cities($city);

		// 				if(empty($city_exist_id))
		// 				{
		// 					$cities = array(
		// 						'city_name'=>$city,
		// 						'added_by'=>'patient',
		// 						'insert_dt'=>date("Y-m-d H:i:s"),
		// 						'update_dt'=>date("Y-m-d H:i:s")
		// 					);
		// 					$cities_id=DB::connection('mysql2')->table('cities')->insertGetId($cities);
		// 				}
		// 				else{
		// 					$cities_id = $city_exist_id;
		// 				}
		// 				$address_data = array(
		// 				'patient_id'=>$patient_id,
		// 				'state'=>$state_id,
		// 				'city'=>$cities_id,
		// 				'pincode'=>$pincode,
		// 				'address_line'=>$address,
		// 				'insert_dt'=>date("Y-m-d H:i:s"),
		// 				'update_dt'=>date("Y-m-d H:i:s")
		// 				);
		// 				$address_id=DB::connection('mysql2')->table('address')->insertGetId($address_data);
		// 			}
		// 			//===================Address end===============//

		// 			$query_update=DB::connection('mysql2')->table('patient_brand_tag')->where('patient_brand_tag_id', $patient_brand_tag_id)->update(['doc_id'=>$doctor_id,'register_by'=>'patient','prescription'=>$priurl,'invoice'=>$invurl,'bottle_purchase'=>$btlurl,'register_status'=>'Active','update_dt'=>date("Y-m-d H:i:s")]);
		// 		}
		// 	}
		// }

		//======================old patient end=====================

		if($insertdata){ 
		$_SESSION['lan'] = $langugae;
		    return redirect()->action('IndexController@backactivites');
        }else{
           return redirect()->action('IndexController@backactivites');
        }
		
    }
	public function termscondition()
    {
		return view('carenshare.termscondition');
    }
	public function benefitscnsprogram()
    {
        return view('carenshare.benefitscnsprogram');
    }
	public function aboutglaucoma()
    {
		$video = DB::table('langugaefile')->where('type', '=', 'video' )->get();
		$file = DB::table('langugaefile')->where('type', '=', 'file' )->get();
		$video1 = DB::table('langugaefile')->where('type', '=', 'video1' )->get();
        return view('carenshare.aboutglaucoma', ['video' => $video, 'video1' => $video1, 'file' => $file]);
    }
	public function glaucomaeyedrops()
    {
		$videos = DB::table('langugaefile')->where('type', '=', 'video' )->get();
        return view('carenshare.glaucomaeyedrops' , ['videos' => $videos]);
    }
	public function backactivites()
    {
	    return view('carenshare.activities');
    }
	public function video(Request $request){
		$input = $request->all();
		$lan = $input['lan'];
		$row = DB::table('langugaefile')->where(['type'=>'video', 'language'=>$lan])->get();
		$_SESSION['video'] = $row;
		return "update";
	}
	public function video1(Request $request){
		$input = $request->all();
		$lan = $input['lan'];
		$row = DB::table('langugaefile')->where(['type'=>'video1', 'language'=>$lan])->get();
		$_SESSION['video1'] = $row;
		return "update";
	}
	public function modalvideo(Request $request){
		$input = $request->all();
		$lan = $input['lan'];
		$row = DB::table('langugaefile')->where(['type'=>'video', 'language'=>$lan])->get();
		$_SESSION['video'] = $row;
		$html = '';
		$html .='<iframe width="670" height="390" src="'.$row[0]->url.'" data-toggle="modal" data-target="#JoinNow"></iframe>';
		echo $html;
	}
	public function modalvideos(Request $request){
		$input = $request->all();
		$lan = $input['lan'];
		$row = DB::table('langugaefile')->where(['type'=>'video1', 'language'=>$lan])->get();
		$_SESSION['video1'] = $row;
		$html = '';
		$html .='<iframe width="670" height="390" src="'.$row[0]->url.'" data-toggle="modal" data-target="#JoinNow"></iframe>';
		echo $html;
	}
	public function eyevideosss(Request $request){
		$input = $request->all();
		$lan = $input['lan'];
		$row = DB::table('langugaefile')->where(['type'=>'eyecare', 'language'=>$lan])->get();
		$_SESSION['Eyevideos'] = $row;
		$html = '';
		$html .='<iframe width="670" height="390" src="'.$row[0]->url.'" data-toggle="modal" data-target="#JoinNow"></iframe>';
		echo $html;
	}
	public function eyevideos(Request $request){
		$input = $request->all();
		$lan = $input['lan'];
		$result = DB::table('langugaefile')->where(['type'=>'eyecare', 'language'=>$lan])->get();
		$_SESSION['Eyevideos'] = $result;
		return "update";
	}
	public function file(Request $request){
		$input = $request->all();
		$lan = $input['lan'];
		$rows = DB::table('langugaefile')->where(['type'=>'file', 'language'=>$lan])->get();
		$_SESSION['file'] = $rows;
		return "update";
	}
	public function addcard(Request $request){
		 $input = $request->all();
		 $data = array(
            'doctorname' =>$input['name'],
            'downloadcard' => $input['card'],
			'status' => "Active",
            'createddate' => date("Y-m-d H:i:s")
        );
		$create = DB::table('downloadcard')->insert($data);
		$status = "update";
		return $status;
	}
	
	public function get_patient_unique_code(){
        $unique = TRUE;
        while($unique){
            $patient_code = 'PT'. str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT);
            //DB::connection("mysql2")->enableQueryLog();
            $value = DB::connection('mysql2')->table('patient')->where('patient_code',$patient_code)->value('patient_id');
            //print_r(DB::connection('mysql2')->getQueryLog()); exit;

            $unique = !empty($value) ? TRUE : FALSE;
            
        }
        return $patient_code;
    }

    public function get_state($state)
	{
	    $query = DB::connection('mysql2')->table('state')->where('state',$state)->value('id');
	    return $query;
	}

	public function get_cities($city)
	{
	    $query = DB::connection('mysql2')->table('cities')->where('city_name',$city)->value('city_id');
	    return $query;
	}

	public function get_language($language)
	{
	    $query = DB::connection('mysql2')->table('language')->where('language_name',$language)->value('language_id');
	    return $query;
	}

	public function patient_tag_exist($patient_id)
	{
	    $query = DB::connection('mysql2')->table('patient_brand_tag')->where('patient_id',$patient_id)->where('division_id',2)->value('patient_brand_tag_id');
	    return $query;
	}

	public function patient_register_exist($patient_id)
	{
		//DB::enableQueryLog();
	    $query = DB::connection('mysql2')->table('patient_brand_tag')->where('patient_id',$patient_id)->where('division_id',2)->whereNull('register_by')->value('patient_brand_tag_id');
	    //$query = DB::connection('mysql2')->table('patient_brand_tag')->where('patient_id',$patient_id)->where('division_id',2)->where('register_by !','')->toSql();
	    //$querylog =  DB::getQueryLog();
		//print_r($query); exit;
	    return $query;
	}

	public function patient_address_exist($patient_id)
	{
	    
		$query = DB::connection('mysql2')->table('address')->where('patient_id',$patient_id)->value('patient_id');
	    return $query;
	}

}

