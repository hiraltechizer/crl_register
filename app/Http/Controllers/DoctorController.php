<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use App\User;
use App\Imports\ImportDoctors;
use Auth;
use Excel;

class DoctorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		
        $data = array('title'=>'Doctors','tbl'=>encrypt("doctors"));

        $data['offset'] = 0;
        if($request->page!=''){
            $data['offset'] = ((($request->page)-1)*config('custom_config.row_no'));
        }

        // GET ALL IDS TO SHOW
        // -----------------------------------------------------------
        $data['user_id'] = Auth::guard('web')->user()->id;
        $user_type = Auth::guard('web')->user()->user_type;
        $user_pos_code = Auth::guard('web')->user()->pos_code;
        $all_ids_array = array();
            
        if($user_type=='SM'){
            // USERS 
            $users = User::select('id','name','user_type','pos_code')->where('parent_id','=',$user_pos_code)->where('status','!=','delete')->get();
            // GET RBM POS_CODES
            $rbm_pos_codes = array();
            foreach($users as $row){
                array_push($rbm_pos_codes,$row->pos_code);
                array_push($all_ids_array,$row->pos_code);
            }
            // GET ABM POS_CODES
            $abm_users = User::select('id','name','user_type','pos_code')->whereIn('parent_id',$rbm_pos_codes)->where('status','!=','delete')->get();
            $abm_pos_codes = array();
            foreach($abm_users as $row){
                array_push($abm_pos_codes,$row->pos_code);
                array_push($all_ids_array,$row->pos_code);
            }
            // GET BE POS_CODES
            $be_users = User::select('id','name','user_type','pos_code')->whereIn('parent_id',$abm_pos_codes)->where('status','!=','delete')->get();
            foreach($be_users as $row){
                array_push($all_ids_array,$row->pos_code);
            }
        }else if($user_type=='RBM'){
            // USERS 
            $users = User::select('id','name','user_type','pos_code')->where('parent_id','=',$user_pos_code)->where('status','!=','delete')->get();
            // GET RBM POS_CODES
            $abm_pos_codes = array();
            foreach($users as $row){
                array_push($abm_pos_codes,$row->pos_code);
                array_push($all_ids_array,$row->pos_code);
            }
            // GET BE POS_CODES
            $be_users = User::select('id','name','user_type','pos_code')->whereIn('parent_id',$abm_pos_codes)->where('status','!=','delete')->get();
            foreach($be_users as $row){
                array_push($all_ids_array,$row->pos_code);
            }
        }else if($user_type=='ABM'){
            // USERS 
            $users = User::select('id','name','user_type','pos_code')->where('parent_id','=',$user_pos_code)->where('status','!=','delete')->get();

            // GET RBM POS_CODES
            foreach($users as $row){
                array_push($all_ids_array,$row->pos_code);
            }
        }else if($user_type=='BE'){
            array_push($all_ids_array,$user_pos_code);
        }
        // -----------------------------------------------------------
        

        $data['result'] = Doctor::whereIn('pos_code',$all_ids_array)->where('status','!=','delete');
        if($request->name!=''){
            $data['result'] = $data['result']->where('name', 'like', '%'.$request->name.'%');
            $data['name'] = $request->name;
        }

        $data['sm'] = $request->sm;
        if($request->sm!='' && $request->rbm=='' && $request->abm=='' && $request->rm==''){
            $data['result'] = $data['result']->where('pos_code', '=', $request->sm);
            $data['rbm'] = '';
            $data['abm'] = '';
            $data['rm'] = '';
        }
        // LIST OF RBM
        $data['rbm'] = $request->rbm;
        if($request->rbm!='' && $request->abm=='' && $request->rm==''){
            $data['result'] = $data['result']->where('pos_code', '=', $request->rbm);
            $data['abm'] = '';
            $data['rm'] = '';
        }

        // LIST OF RBM
        $data['abm'] = $request->abm;
        if($request->abm!=''){
            $data['result'] = $data['result']->where('pos_code', '=', $request->abm);
            $data['rm'] = '';
        }

        // LIST OF RM
        $data['rm'] = $request->rm;
        if($request->rm!=''){
            $data['result'] = $data['result']->where('pos_code', '=', $request->rm);
        }

        $data['result'] = $data['result']->paginate(config('custom_config.row_no'));
        $data['result'] = $data['result']->appends(['name'=>$request->name,'sm'=>$request->sm,'rbm'=>$request->rbm,'abm'=>$request->abm,'rm'=>$request->rm]);

        // GET DATA FOR FILTER 
        $filter_data =  User::where('status','!=','delete')->get();
        // LIST OF SM
        $list_sm = array();
        foreach($filter_data as $row){
            if($row->user_type=='SM'){
                array_push($list_sm,$row);
            }
        }
        $data['list_sm'] = $list_sm;
        // LIST OF RBM
        $list_rbm = array();
        foreach($filter_data as $row){
            if($row->user_type=='RBM' && $row->parent_id == $data['sm'] ){
                array_push($list_rbm,$row);
            }
        }
        $data['list_rbm'] = $list_rbm;
        // LIST OF ABM
        $list_abm = array();
        foreach($filter_data as $row){
            if($row->user_type=='ABM' && $row->parent_id == $data['rbm']){
                array_push($list_abm,$row);
            }
        }
        $data['list_abm'] = $list_abm;
        // LIST OF RM
        $list_rm = array();
        foreach($filter_data as $row){
            if($row->user_type=='RM' && $row->parent_id == $data['abm']){
                array_push($list_rm,$row);
            }
        }
        $data['list_rm'] = $list_rm;

        return view('doctors',$data);
    }

    public function information($id)
    {
        $data = array('title'=>'Doctor Information');
        $data['result'] = Doctor::where('status','!=','delete')
                                ->where('id','=',$id)
                                ->first();
        if(!empty($data['result'])){
            return view('doctors_detail',$data);
        }else{
            return redirect(route('web.doctors'));
        }
    }

}
