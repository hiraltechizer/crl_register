<?php

namespace App\Http\Controllers;

use App\Exports\DashboardExport;
use App\HqMaster;
use Illuminate\Http\Request;
use App\Doctor;
use App\User;
use App\Product;
use App\Speciality;
use App\DoctorPotential;
use App\DoctorSupport;
use App\DoctorSpeciality;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    

   
    public function index(Request $request)
    {
		return view('home');
    }
	
    

  
}
