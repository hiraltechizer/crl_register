<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    //protected $guard = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pos_code', 'emp_code', 'parent_id', 'name', 'contact', 'contact', 'address', 'city', 'state', 'pincode', 'language', 'language', 'productuseflag', 'position_name', 'designation', 'active', 'effective_dt', 'join_date', 'dob', 'mobile_number', 'ios_app_version', 'android_app_version', 'user_type', 'status', 'is_freeze', 'hq_name', 'parent_access', 'give_support_update_access_date', 'give_potential_update_access_date','hq_code', 'cron_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
   
}
