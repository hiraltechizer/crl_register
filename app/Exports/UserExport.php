<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

use DB;

class UserExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
		if(isset($_SESSION['range'])){
			$arr = explode("-",$_SESSION['range']);
			$fst = date("Y-m-d", strtotime($arr[0]));
			$first = $fst." 00:00:00";
			$snd = date("Y-m-d", strtotime($arr[1]));
			$second = $snd." 23:59:59";
			$records = DB::table('users')->select('name','age','gender','address','city','userlanguage','state','mobile','pincode','doctorname','doctorcity','preurl','invurl','btlurl','created_at')->whereBetween( 'created_at', [$first, $second])->orderBy('id', 'DESC')->get();
		}else{
			$records = DB::table('users')->select('name','age','gender','address','city','userlanguage','state','mobile','pincode','doctorname','doctorcity','preurl','invurl','btlurl','created_at')->orderBy('id', 'DESC')->get();
		}
		 return $records;
    }
	public function headings(): array
    {
        return [
            'Name',
            'Age',
            'Gender',
            'Address',
            'City',
            'Preferred Language',
            'State',
            'Mobile',
            'Pincode',
            'Doctor name',
            'Doctor City',
            'Prescription',
            'Invoice/Bill',
            'Image of Bottle Purchased',
            'Created Date'
        ];
    }
}
