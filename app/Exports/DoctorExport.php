<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

use DB;

class DoctorExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $records = DB::table('doctor')->select('id','doctorname','speciality','hospitalname','city','message','downloadimage','status','createddate')->get();
		 return $records;
    }
	public function headings(): array
    {
        return [
            '#',
            'Name',
            'Speciality',
            'Hospital',
            'City',
            'Message',
            'Download poster',
            'Status',
            'Created Date'
        ];
    }
}
