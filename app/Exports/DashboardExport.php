<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

class DashboardExport implements FromCollection, ShouldAutoSize
{
    public function __construct($data){
        $this->data = $data;
    }

    public function collection()
    {
        $export_data = [[
            'USER TYPE',
            'POSITION CODE',
            'EMPLOYEE CODE',
            'POSITION NAME',
            'USER DESIGNATION',
            'USER DOB',
            'USER JOIN DATE',
            'USER EMAIL ID',
            'HQ NAME',
            'LAST LOGIN',
            'NO OF DOCTORS IN THE MSL',
            'TOTAL NO DOCTORS BASIC DETAILS  FILLED BY BE',
            'NO OF POTENTIAL FILLED BY BE IN APR',
            'NO OF DPM FILLED BY BE IN APR',
            'NO OF POTENTIAL FILLED BY BE IN MAY',
            'NO OF DPM FILLED BY BE IN MAY',
            'NO OF POTENTIAL FILLED BY BE IN JUN',
            'NO OF DPM FILLED BY BE IN JUN',
            'NO OF POTENTIAL FILLED BY BE IN JUL',
            'NO OF DPM FILLED BY BE IN JUL',
            'NO OF POTENTIAL FILLED BY BE IN AUG',
            'NO OF DPM FILLED BY BE IN AUG',
            'NO OF POTENTIAL FILLED BY BE IN SEP',
            'NO OF DPM FILLED BY BE IN SEP',
            'NO OF POTENTIAL FILLED BY BE IN OCT',
            'NO OF DPM FILLED BY BE IN OCT',
            'NO OF POTENTIAL FILLED BY BE IN NOV',
            'NO OF DPM FILLED BY BE IN NOV',
            'NO OF POTENTIAL FILLED BY BE IN DEC',
            'NO OF DPM FILLED BY BE IN DEC',
            'NO OF POTENTIAL FILLED BY BE IN JAN',
            'NO OF DPM FILLED BY BE IN JAN',
            'NO OF POTENTIAL FILLED BY BE IN FEB',
            'NO OF DPM FILLED BY BE IN FEB',
            'NO OF POTENTIAL FILLED BY BE IN MAR',
            'NO OF DPM FILLED BY BE IN MAR',
        ]];

        foreach ($this->data['result'] as $r){

            $doctor_msl = 0;
            $doctor_pending = 0;
            $total_potential = 0;
            $total_support = 0;
            $total_month_support = 0;

            if($r->user_type == 'SM'){
                $doctor_msl_res = @DB::select('SELECT id FROM `doctors` WHERE FIND_IN_SET(pos_code,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "BE" AND status = "enable" AND FIND_IN_SET(parent_id,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "ABM" AND status = "enable" AND FIND_IN_SET(parent_id,(SELECT GROUP_CONCAT(pos_code) FROM `users` WHERE user_type = "RBM" AND status = "enable" AND parent_id = '.$r->pos_code.'))))))');

                $ids_arr = [];
                foreach ($doctor_msl_res as $rr){
                    array_push($ids_arr,$rr->id);
                }
                $doctor_msl = count($doctor_msl_res);

                $doctor_pending = @DB::select('SELECT count(id) as tmp_count FROM `doctors` WHERE profile_status != "pending" AND FIND_IN_SET(pos_code,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "BE" AND status = "enable" AND FIND_IN_SET(parent_id,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "ABM" AND status = "enable" AND FIND_IN_SET(parent_id,(SELECT GROUP_CONCAT(pos_code) FROM `users` WHERE user_type = "RBM" AND status = "enable" AND parent_id = '.$r->pos_code.'))))))')[0]->tmp_count;

                /*$total_potential = @DB::select('SELECT COUNT(doctor_id), month, year FROM(SELECT  doctor_id, month, year FROM doctor_potential  WHERE doctor_id IN ('.implode(',',$ids_arr).')  GROUP BY doctor_id, month, year) as t GROUP BY month, year');

                $total_support = @DB::select('SELECT COUNT(doctor_id), month, year FROM(SELECT  doctor_id, month, year FROM doctor_supports WHERE doctor_id IN ('.implode(',',$ids_arr).')  GROUP BY doctor_id, month, year) as t GROUP BY month, year');*/

                /*$total_month_support = @DB::select('SELECT count(id) as tmp_count FROM doctor_supports WHERE doctor_id IN ('.implode(',',$ids_arr).') AND month = "'.strtolower(date("M")).'" AND year = "'.date("Y").'" GROUP BY doctor_id, month, year');*/

            }
            else if($r->user_type == 'RBM'){
                $doctor_msl_res = @DB::select('SELECT id FROM `doctors` WHERE FIND_IN_SET(pos_code,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "BE" AND status = "enable" AND FIND_IN_SET(parent_id,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "ABM" AND status = "enable" AND parent_id = '.$r->pos_code.'))))');

                $ids_arr = [];
                foreach ($doctor_msl_res as $rr){
                    array_push($ids_arr,$rr->id);
                }
                $doctor_msl = count($doctor_msl_res);

                $doctor_pending = @DB::select('SELECT count(id) as tmp_count  FROM `doctors` WHERE profile_status != "pending" AND FIND_IN_SET(pos_code,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "BE" AND status = "enable" AND FIND_IN_SET(parent_id,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "ABM" AND status = "enable" AND parent_id = '.$r->pos_code.'))))')[0]->tmp_count;

                /*$total_potential = @DB::select('SELECT count(id) as tmp_count  FROM doctor_potential WHERE doctor_id IN ('.implode(',',$ids_arr).') GROUP BY doctor_id, month, year');

                $total_support = @DB::select('SELECT count(id) as tmp_count  FROM doctor_supports WHERE doctor_id IN ('.implode(',',$ids_arr).') GROUP BY doctor_id, month, year');*/

                /*$total_month_support = @DB::select('SELECT count(id) as tmp_count  FROM doctor_supports WHERE doctor_id IN ('.implode(',',$ids_arr).') AND month = "'.strtolower(date("M")).'" AND year = "'.date("Y").'" GROUP BY doctor_id, month, year');*/
            }
            else if($r->user_type == 'ABM'){
                $doctor_msl_res = @DB::select('SELECT id FROM `doctors` WHERE FIND_IN_SET(pos_code,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "BE" AND status = "enable" AND parent_id = '.$r->pos_code.'))');

                $ids_arr = [];
                foreach ($doctor_msl_res as $rr){
                    array_push($ids_arr,$rr->id);
                }
                $doctor_msl = count($doctor_msl_res);

                $doctor_pending = @DB::select('SELECT count(id) as tmp_count FROM `doctors` WHERE profile_status != "pending" AND FIND_IN_SET(pos_code,(SELECT GROUP_CONCAT(pos_code) FROM users WHERE user_type = "BE" AND status = "enable" AND parent_id = '.$r->pos_code.'))')[0]->tmp_count;

                /*$total_potential = @DB::select('SELECT count(id) as tmp_count FROM doctor_potential WHERE doctor_id IN ('.implode(',',$ids_arr).') GROUP BY doctor_id, month, year');

                $total_support = @DB::select('SELECT count(id) as tmp_count FROM doctor_supports WHERE doctor_id IN ('.implode(',',$ids_arr).') GROUP BY doctor_id, month, year');*/

                /*$total_month_support = @DB::select('SELECT count(id) as tmp_count FROM doctor_supports WHERE doctor_id IN ('.implode(',',$ids_arr).') AND month = "'.strtolower(date("M")).'" AND year = "'.date("Y").'" GROUP BY doctor_id,  month, year');*/
            }
            else {
                $doctor_msl_res = @DB::select('SELECT id FROM `doctors` WHERE pos_code = '.$r->pos_code);

                $ids_arr = [];
                foreach ($doctor_msl_res as $rr){
                    array_push($ids_arr,$rr->id);
                }
                $doctor_msl = count($doctor_msl_res);

                $doctor_pending = @DB::select('SELECT count(id) as tmp_count FROM `doctors` WHERE profile_status != "pending" AND pos_code = '.$r->pos_code)[0]->tmp_count;

                /*$total_potential = @DB::select('SELECT count(id) as tmp_count FROM doctor_potential WHERE doctor_id IN ('.implode(',',$ids_arr).') GROUP BY doctor_id, month, year');

                $total_support = @DB::select('SELECT count(id) as tmp_count FROM doctor_supports WHERE doctor_id IN ('.implode(',',$ids_arr).') GROUP BY doctor_id, month, year');*/

                /*$total_month_support = @DB::select('SELECT count(id) as tmp_count FROM doctor_supports WHERE doctor_id IN ('.implode(',',$ids_arr).') AND month = "'.strtolower(date("M")).'" AND year = "'.date("Y").'" GROUP BY doctor_id, month, year');*/
            }

            $tmp_data = [
                'apr' => ['support'=>0,'pot'=>0],
                'may' => ['support'=>0,'pot'=>0],
                'jun' => ['support'=>0,'pot'=>0],
                'jul' => ['support'=>0,'pot'=>0],
                'aug' => ['support'=>0,'pot'=>0],
                'sep' => ['support'=>0,'pot'=>0],
                'oct' => ['support'=>0,'pot'=>0],
                'nov' => ['support'=>0,'pot'=>0],
                'dec' => ['support'=>0,'pot'=>0],
                'jan' => ['support'=>0,'pot'=>0],
                'feb' => ['support'=>0,'pot'=>0],
                'mar' => ['support'=>0,'pot'=>0],
            ];
            if(!empty($ids_arr)) {
                $total_potential = @DB::select('SELECT COUNT(doctor_id) as tot, month, year FROM(SELECT  doctor_id, month, year FROM doctor_potential  WHERE doctor_id IN (' . implode(',', $ids_arr) . ')  GROUP BY doctor_id, month, year) as t GROUP BY month, year');
                foreach ($total_potential as $rr){
                    $tmp_data[$rr->month]['pot'] = $rr->tot;
                }
            }

            if(!empty($ids_arr)) {
                $total_support = @DB::select('SELECT COUNT(doctor_id) as tot, month, year FROM(SELECT  doctor_id, month, year FROM doctor_supports WHERE doctor_id IN (' . implode(',', $ids_arr) . ')  GROUP BY doctor_id, month, year) as t GROUP BY month, year');
                foreach ($total_support as $rr){
                    $tmp_data[$rr->month]['support'] = $rr->tot;
                }
            }


            array_push($export_data,[
                @$r->user_type,
                @$r->pos_code,
                @$r->emp_code,
                @$r->position_name,
                @$r->designation,
                @$r->dob,
                @$r->join_date,
                @$r->email,
                @$r->HqMaster->name,
                '',
                $doctor_msl,
                $doctor_pending,
                $tmp_data['apr']['pot']?$tmp_data['apr']['pot']:'0',
                $tmp_data['apr']['support']?$tmp_data['apr']['support']:'0',
                $tmp_data['may']['pot']?$tmp_data['may']['pot']:'0',
                $tmp_data['may']['support']?$tmp_data['may']['support']:'0',
                $tmp_data['jun']['pot']?$tmp_data['jun']['pot']:'0',
                $tmp_data['jun']['support']?$tmp_data['jun']['support']:'0',
                $tmp_data['jul']['pot']?$tmp_data['jul']['pot']:'0',
                $tmp_data['jul']['support']?$tmp_data['jul']['support']:'0',
                $tmp_data['aug']['pot']?$tmp_data['aug']['pot']:'0',
                $tmp_data['aug']['support']?$tmp_data['aug']['support']:'0',
                $tmp_data['sep']['pot']?$tmp_data['sep']['pot']:'0',
                $tmp_data['sep']['support']?$tmp_data['sep']['support']:'0',
                $tmp_data['oct']['pot']?$tmp_data['oct']['pot']:'0',
                $tmp_data['oct']['support']?$tmp_data['oct']['support']:'0',
                $tmp_data['nov']['pot']?$tmp_data['nov']['pot']:'0',
                $tmp_data['nov']['support']?$tmp_data['nov']['support']:'0',
                $tmp_data['dec']['pot']?$tmp_data['dec']['pot']:'0',
                $tmp_data['dec']['support']?$tmp_data['dec']['support']:'0',
                $tmp_data['jan']['pot']?$tmp_data['jan']['pot']:'0',
                $tmp_data['jan']['support']?$tmp_data['jan']['support']:'0',
                $tmp_data['feb']['pot']?$tmp_data['feb']['pot']:'0',
                $tmp_data['feb']['support']?$tmp_data['feb']['support']:'0',
                $tmp_data['mar']['pot']?$tmp_data['mar']['pot']:'0',
                $tmp_data['mar']['support']?$tmp_data['mar']['support']:'0',
                //@count($total_potential),
                //@count($total_support),
                //@count($total_month_support)
            ]);
        }

        return collect($export_data);
    }
}
