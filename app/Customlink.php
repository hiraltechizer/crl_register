<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customlink extends Model
{
    protected $fillable = [
        'fullname', 'linkurl', 'expiryDate', 'status', 'created_at', 'updated_at'
    ];

   
}
