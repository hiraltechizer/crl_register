$().ready(function() {
	jQuery.validator.setDefaults({
		highlight: function(element) {
			//$(element).closest('.form-group').addClass('has-error');
			$(element).parent().addClass('has-error');
		},
		unhighlight: function(element) {
			//$(element).closest('.form-group').removeClass('has-error');
			$(element).parent().removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			if(element.parent('.input-group').length) {
				if(element.parent('.has-error').parent().find('.help-block').length)
				{
					element.parent('.has-error').parent().find('.help-block').remove();
				}
				error.insertAfter(element.parent());
			} 
			else if(element.attr('id') == 'discount' || element.attr('id') == 'frequency'){
				if(element.parent('.has-error').parent().find('.help-block').length)
				{
					element.parent('.has-error').parent().find('.help-block').remove();
				}
				error.insertAfter(element.next());
			}
			else {
				if(element.parent('.has-error').find('.help-block').length)
				{
					element.parent('.has-error').find('.help-block').remove();
				}
				error.insertAfter(element);
			}
		},
		onfocusout: false
	});
	jQuery.validator.addMethod("CheckPercentage", function(value, element) {
		// allow any non-whitespace characters as the host part
		return this.optional( element ) || /^[1-9][0-9]?$|^100$/.test( value );
		}, '');
	// validate signup form on keyup and submit
	$("#frm_manage_login1").validate({
		rules: {
			email: {
				required: true,
				maxlength: 200,
				email:true,
			},
			password: {
				required: true,
			},
		},
		messages: {
			email: {
				required: "Email field is required.",
				maxlength: "Maximum 200 characters are allowed in email.",
				email: "Please enter valid email.",
			},
			password: {
				required: "Password field is required.",
			},
		},
		errorElement: 'div',
		errorPlacement: function(error, element) {
			if(element.parent('.has-error').parent().find('.help-block').length)
			{
				element.parent('.has-error').parent().find('.help-block').remove();
			}
			error.insertAfter(element);
		},
	});
	$("#frm_rxber_range").validate({
		rules: {
			rxbers_value_green: {
				required: true,
				number: true,
			},
			rxbers_value_red: {
				required: true,
				number: true,
			},
		},
		messages: {
			rxbers_value_green: {
				required: "",
				number: "",
			},
			rxbers_value_red: {
				required: "",
				number: "",
			},
		},
		errorElement: 'div',
		errorPlacement: function(error, element) {
			if(element.parent('.has-error').parent().find('.help-block').length)
			{
				element.parent('.has-error').parent().find('.help-block').remove();
			}
			error.insertAfter(element);
		},
	});
	$("#frm_send_notification").validate({
		rules: {
			percentage: {
				required: true,
				CheckPercentage: true,
			},
			message: {
				required: true,
			},
		},
		messages: {
			percentage: {
				required: "",
			},
			message: {
				required: "",
			},
		},
		errorElement: 'div',
		errorPlacement: function(error, element) {
			if(element.parent('.has-error').parent().find('.help-block').length)
			{
				element.parent('.has-error').parent().find('.help-block').remove();
			}
			error.insertAfter(element);
		},
	});
	$("#frm_all_india_rio_file").validate({
		rules: {
			select_export_sm: {
				required: true,
			},
		},
		messages: {
			select_export_sm: {
				required: "",
			},
		},
		errorElement: 'div',
		errorPlacement: function(error, element) {
			if(element.parent('.has-error').parent().find('.help-block').length)
			{
				element.parent('.has-error').parent().find('.help-block').remove();
			}
			error.insertAfter(element);
		},
	});
});
// FOR PRELOADER MODAL
function cmnPreloaderPopup(hide){
	// console.log(2+' :: '+hide);
	// if(hide==''){
	// 	$('#cmn_preloader_popup').modal({backdrop: 'static', keyboard: false});
	// }else{
	// 	$('#cmn_preloader_popup').modal('hide');
	// }
}
$(window).ready(function(){

    // SHOW AJAX PROCESS
    $(document).ajaxStart(function(e) {
        if (typeof Pace != 'undefined'){
            Pace.restart();
        }
    });

	$(document).on('click','.switch',function(e){
		e.preventDefault();
		var that = $(this);
		var id = that.closest('tr').attr('id'); 
		id = id.replace('data-','');
		var action = 0;
		if(that.hasClass('toggle-on')){
			action++;
		}
		var form_data = new FormData();
		form_data.append('tbl',$('#tbl').val());
		form_data.append('id',id);
		form_data.append('action',action);
		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		$.ajax({    
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
            async: true,
			url: SITE_URL + '/status',
			success: function(obj) {
				if (obj.code == 1) {
					that.toggleClass('toggle-on');		
				}
			},
		});
	});

	$(document).on('click','.focus-switch',function(e){
		e.preventDefault();
		var that = $(this);
		var id = that.closest('tr').attr('id'); 
		id = id.replace('data-','');
		var action = 0;
		if(that.hasClass('toggle-on')){
			action++;
		}
		var form_data = new FormData();
		form_data.append('tbl',$('#tbl').val());
		form_data.append('id',id);
		form_data.append('action',action);
		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		$.ajax({    
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
            async: true,
			url: SITE_URL + '/products/focus-status',
			success: function(obj) {
				if (obj.code == 1) {
					that.toggleClass('toggle-on');		
				}
				else {
					
				}
			},
			error: function(obj) {
				//
			},
		});
	});

	///COMMON DELETE
	var currRaw = '';
	$(document).on('click','.delete',function(e){
		$('#cmn_delete_popup').modal('show');
		currRaw = $(this).closest('tr').attr('id');
		currRaw = currRaw.replace('data-','');
		currTr = $(this).closest('tr');
	});
	$(document).on('click','#cmn_delete_popup .confirm',function(e){
		e.preventDefault();
		var that = $(this);
		that.text('Processing...');
		var form_data = new FormData();
		form_data.append('tbl',$('#tbl').val());
		form_data.append('id',currRaw);
		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		$.ajax({    
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
            async: true,
			url: SITE_URL + '/delete',
			success: function(obj) {
				if (obj.code == 1) {
					that.text('Delete');
					currTr.remove();
					$('#cmn_delete_popup').modal('hide');
				}
				else {
					
				}
			},
			error: function(obj) {
				//
			},
		});
	});
	function readURL(input,that) {    
		if (input.files && input.files[0]) {   
			var filename = that.val();
			filename = filename.substring(filename.lastIndexOf('\\')+1);
			$('.custom-file-label').text(filename);             
		}
	} 
	$(document).on('change','.js-import-btn',function(e){
		var that = $(this);
		readURL(this,that); 
	});

	$('#select_export_sm').selectpicker({
		noneSelectedText:"--- Select SM ---",
	});
	$('#select_sm').selectpicker({
		noneSelectedText:"--- Select SM ---",
		selectedTextFormat: "count > 1",
	});
	$('#select_abm').selectpicker({
		noneSelectedText:"--- Select ABM ---",
		selectedTextFormat: "count > 1",
	});
	$('#select_rbm').selectpicker({
		noneSelectedText:"--- Select RBM ---",
		selectedTextFormat: "count > 1",
	});
	$('#select_be').selectpicker({
		noneSelectedText:"--- Select BE ---",
		selectedTextFormat: "count > 1",
	});

	$(document).on('click','.js-rxbers-color-div',function(e){
		e.preventDefault();
		$('#rxbers_cat_popup').modal('show');
	});
	$(document).on('click','#rxbers_cat_popup .confirm',function(e){
		if($('#frm_rxber_range').valid()){
			e.preventDefault();
			js_green_val = parseInt($(document).find('#rxbers_value_green').val());
			js_red_val = parseInt($(document).find('#rxbers_value_red').val());
			$(document).find('.js-rxbers-color-div.green span').text(js_green_val);
			$(document).find('.js-rxbers-color-div.orange span').text((js_green_val-1)+' To '+(js_red_val+1));
			$(document).find('.js-rxbers-color-div.red span').text(js_red_val);
			$('#rxbers_cat_popup').modal('hide');
			$( "#select_rxbers" ).trigger( "change" );
		}
		
	});
});
