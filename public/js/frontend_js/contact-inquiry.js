function contactInquiry() {
    var contactName = $('#contactName').val().trim();
    var contactNo = $('#contactNo').val().trim();
    var contactAddress = $('#contactAddress').val().trim();
	var contactEmail = $('#contactEmail').val().trim();
    var contactState = $('#contactState').val().trim();
    var contactCity = $('#contactCity').val().trim();
    var contactPin = $('#contactPin').val().trim();
    var contactCountry = $('select[name=contactCountry]').val();
    var allDataFilled = true;
    if (contactName === null || contactName === "") {
        document.getElementById('contactNameRequired').innerHTML = "Please enter your Name";
        allDataFilled = false;
    } else if (!contactName.match(/^[a-zA-Z\s]+$/)) {
        document.getElementById('contactNameRequired').innerHTML = "Please enter valid Name";
    } else {
        document.getElementById('contactNameRequired').innerHTML = "";
    } 
	if (contactAddress === null || contactAddress === "") {
        document.getElementById('contactAddressRequired').innerHTML = "Please enter address";
        allDataFilled = false;
    } else {
        document.getElementById('contactAddressRequired').innerHTML = "";
    }
    
    
	if (contactNo === null || contactNo === "") {
        document.getElementById('contactNoRequired').innerHTML = "Please enter contact number";
        allDataFilled = false;
    } else if ((contactNo.length > 0 && contactNo.length < 9) || contactNo.length > 14) {
        document.getElementById('contactNoRequired').innerHTML = "Please enter valid Contact No";
        allDataFilled = false;
    } else {
        document.getElementById('contactNoRequired').innerHTML = "";
    }
    if (contactEmail === null || contactEmail === "") {
        document.getElementById('contactEmailRequired').innerHTML = "Please enter your Email Id";
        allDataFilled = false;
    } else if (!contactEmail.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)) {
        document.getElementById('contactEmailRequired').innerHTML = "Please enter valid Email Id";
        allDataFilled = false;
    } else {
        document.getElementById('contactEmailRequired').innerHTML = "";
    }
    
    if (contactState === null || contactState === "") {
        document.getElementById('contactStateRequired').innerHTML = "Please enter your State";
        allDataFilled = false;
    } else {
        document.getElementById('contactStateRequired').innerHTML = "";
    }
    if (contactCity === null || contactCity === "") {
        document.getElementById('contactCityRequired').innerHTML = "Please enter your City";
        allDataFilled = false;
    } else {
        document.getElementById('contactCityRequired').innerHTML = "";
    }
	if (contactPin === null || contactPin === "") {
        document.getElementById('contactPinRequired').innerHTML = "Please enter your Pincode";
        allDataFilled = false;
    } else {
        document.getElementById('contactPinRequired').innerHTML = "";
    }
    
    if (allDataFilled) {
        addContactQuery(contactName, contactNo, contactEmail, contactCountryCode, contactCountry, contactCity, contactState);
        return false;
    } else {
        return false;
    }
}

$("#contactName").keyup(function () {
    var contactName = $('#contactName').val().trim();
    if (contactName === null || contactName === "") {
        document.getElementById('contactNameRequired').innerHTML = "Please enter your Name";
    } else if (!contactName.match(/^[a-zA-Z\s]+$/)) {
        document.getElementById('contactNameRequired').innerHTML = "Please enter valid Name";
    } else {
        document.getElementById('contactNameRequired').innerHTML = "";
    }
});

$("#contactCountryCode").keyup(function () {
    this.value = "+" + this.value.replace(/[^0-9]/g, '');
    var contactCountryCode = this.value;
    if (contactCountryCode.length < 2) {
        document.getElementById('contactCountryCodeRequired').innerHTML = "Invalid Code";
        allDataFilled = false;
        /* } else if (!contactNo.match(/^\d{10}$/)) {
             document.getElementById('contactNoRequired').innerHTML = "Please enter valid Mobile No";
             allDataFilled = false;*/
    } else if (contactCountryCode.length >= 6) {
        this.value = contactCountryCode.substr(0, 5);
    } else {
        document.getElementById('contactCountryCodeRequired').innerHTML = "";
    }
});

$("#contactNo").keyup(function () {
    this.value = this.value.replace(/[^0-9]/g, '');
    var contactNo = this.value;
    if ((contactNo.length > 0 && contactNo.length < 9)) {
        document.getElementById('contactNoRequired').innerHTML = "Please enter valid Contact No";
        allDataFilled = false;
        /* } else if (!contactNo.match(/^\d{10}$/)) {
             document.getElementById('contactNoRequired').innerHTML = "Please enter valid Mobile No";
             allDataFilled = false;*/
    } else if (contactNo.length >= 17) {
        this.value = contactNo.substr(0, 16);
    } else {
        document.getElementById('contactNoRequired').innerHTML = "";
    }
});

$("#contactEmail").keyup(function () {
    var contactEmail = $('#contactEmail').val().trim();
    if (contactEmail === null || contactEmail === "") {
        document.getElementById('contactEmailRequired').innerHTML = "Please enter your Email Id";
        allDataFilled = false;
    } else if (!contactEmail.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)) {
        document.getElementById('contactEmailRequired').innerHTML = "Please enter valid Email Id";
        allDataFilled = false;
    } else {
        document.getElementById('contactEmailRequired').innerHTML = "";
    }
});

$("#contactComment").keyup(function () {
    var contactComment = $('#contactComment').val().trim();
    if (contactComment === null || contactComment === "") {
        document.getElementById('contactCommentRequired').innerHTML = "Please Enter Your Message";
        allDataFilled = false;
    } else if (contactComment.length < 5) {
        document.getElementById('contactCommentRequired').innerHTML = "Please enter valid Message";
        allDataFilled = false;
    } else {
        document.getElementById('contactCommentRequired').innerHTML = "";
    }
});
$("#contactSubject").keyup(function () {
    var contactSubject = $('#contactSubject').val().trim();
    if (contactSubject === null || contactSubject === "") {
        document.getElementById('contactSubjectRequired').innerHTML = "Please Enter Subject";
        allDataFilled = false;
    } else if (contactSubject.length < 5) {
        document.getElementById('contactSubjectRequired').innerHTML = "Please enter valid Subject";
        allDataFilled = false;
    } else {
        document.getElementById('contactSubjectRequired').innerHTML = "";
    }
});

$("#contactCountry").change(function () {
    var contactCountry = $('select[name=contactCountry]').val();
    if (contactCountry === "-1") {
        document.getElementById('contactCountryRequired').innerHTML = "Please Select the product that you are Interested";
        allDataFilled = false;
    } else {
        document.getElementById('contactCountryRequired').innerHTML = "";
    }
});

//Send Message
function addContactQuery(contactName, contactNo, contactEmail, contactComment, contactSubject, contactCountryCode, contactCountry, contactCity, contactState) {
    var model = "addContact";
    var formData = new FormData();
    formData.append("model", model);
    formData.append("contactName", contactName);
    formData.append("contactNo", contactNo);
    formData.append("contactEmail", contactEmail);
    formData.append("contactComment", contactComment);
    formData.append("contactSubject", contactSubject);
    formData.append("contactCountryCode", contactCountryCode);
    formData.append("contactCountry", contactCountry);
    formData.append("contactCity", contactCity);
    formData.append("contactState", contactState);
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            alert(xmlhttp.responseText);
            location.reload();
        }
    };

    xmlhttp.open("POST", "include-js/index.php?", true);
    xmlhttp.send(formData);
}