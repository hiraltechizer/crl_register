<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'IndexController@indexform')->name('carenshare.index');
Route::post('carenshare/activities', 'IndexController@store')->name('carenshare.activities');
Route::post('carenshare/video', 'IndexController@video')->name('carenshare.video');
Route::post('carenshare/video1', 'IndexController@video1')->name('carenshare.video1');
Route::post('carenshare/modalvideo', 'IndexController@modalvideo')->name('carenshare.modalvideo');
Route::post('carenshare/modalvideos', 'IndexController@modalvideos')->name('carenshare.modalvideos');
Route::post('carenshare/eyevideos', 'IndexController@eyevideos')->name('carenshare.eyevideos');
Route::post('carenshare/eyevideosss', 'IndexController@eyevideosss')->name('carenshare.eyevideosss');
Route::post('carenshare/file', 'IndexController@file')->name('carenshare.file');
Route::get('carenshare/benefitscnsprogram', 'IndexController@benefitscnsprogram')->name('carenshare.benefitscnsprogram');
Route::get('carenshare/aboutglaucoma', 'IndexController@aboutglaucoma')->name('carenshare.aboutglaucoma');
Route::get('carenshare/termscondition', 'IndexController@termscondition')->name('carenshare.termscondition');
Route::get('carenshare/glaucomaeyedrops', 'IndexController@glaucomaeyedrops')->name('carenshare.glaucomaeyedrops');
Route::get('carenshare/activites', 'IndexController@backactivites')->name('carenshare.backactivites');
Route::post('carenshare/addcard', 'IndexController@addcard')->name('carenshare.addcard');
Route::post('carenshare/framesubmit', 'IndexController@frameinsert')->name('carenshare.framesubmit');



include('admin.php');
//Auth::routes();
Route::prefix('admin')->group(function () {
Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'AdminAuth\LoginController@login')->name('login');
Route::post('/logout', 'AdminAuth\LoginController@logoutAdmin')->name('logout');

Route::get('/', 'Admin\HomeController@index')->name('home');
Route::get('/home', 'Admin\HomeController@index')->name('home');
Route::get('/dashboard', 'Admin\HomeController@index')->name('home');
Route::post('/dashboard/filter-records', 'Admin\HomeController@filterRecords');
Route::post('/dashboard-export', 'Admin\HomeController@export')->name('dashboard.export');


// Doctor
Route::get('/doctor', 'Admin\DoctorController@index')->name('doctor');
Route::post('/delete', 'Admin\DeleteController@index')->name('delete');

// USERS

Route::get('/users/information/{id}', 'UserController@information')->name('users.information');

// DOCTORS
Route::get('/doctors', 'DoctorController@index')->name('doctors');
Route::get('/doctors/information/{id}', 'DoctorController@information')->name('web.doctors.information');

// REPORTS
Route::any('/all-india-rio-file', 'AllIndiaRoiFileController@index');

Route::get('/cron-job-doc-remaining-reminder', 'Admin\CronJobDocRemainingReminderController@index');
Route::get('/cron-job-month-change', 'Admin\CronJobMonthSupportController@index');
});
