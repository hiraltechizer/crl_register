<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {
    Route::get('/', 'Admin\HomeController@index')->name('home');
    Route::get('/home', 'Admin\HomeController@index')->name('home');
    Route::get('/dashboard', 'Admin\HomeController@index')->name('home');
    Route::post('/dashboard/filter-records', 'Admin\HomeController@filterRecords');
    Route::post('/dashboard-export', 'Admin\HomeController@export')->name('dashboard.export');

    //Route::get('/profile', 'Admin\ProfileController@index')->name('profile');
    // STATUS AND DELETE ROUTES
    Route::post('/status', 'Admin\StatusController@index')->name('status');
    Route::post('/delete', 'Admin\DeleteController@index')->name('delete');
  

    // USERS
    Route::get('/users', 'Admin\UserController@index')->name('users');
    Route::get('/users/createmr', 'Admin\UserController@create')->name('users.createmr');
    Route::post('/users/import-users', 'Admin\UserController@importUsers')->name('users.import');
    Route::get('/users/information/{id}', 'Admin\UserController@information')->name('users.information');
    Route::get('/users/editmr/{id}', 'Admin\UserController@edit')->name('users.editmr');
    Route::post('/users/update/{id}', 'Admin\UserController@update')->name('users.update');
	Route::post('/users/store', 'Admin\UserController@store')->name('users.store');
	Route::post('/users/changestatus', 'Admin\UserController@changestatus')->name('users.changestatus');
	Route::post('/users/updatedata', 'Admin\UserController@updatedata')->name('users.updatedata');
	Route::post('/users/updatelink', 'Admin\UserController@updatelink')->name('users.updatelink');
	Route::post('/users/viewdata', 'Admin\UserController@viewdata')->name('users.viewdata');
	Route::get('/users/export', 'Admin\UserController@export')->name('users.export');
	Route::post('/users/getcount', 'Admin\UserController@getcount')->name('users.getcount');
    // DOCTORS
    Route::get('/doctor', 'Admin\DoctorController@index')->name('doctor');
    Route::get('/download', 'Admin\DoctorController@download')->name('download');
    Route::get('/completedoc', 'Admin\DoctorController@completedoc')->name('completedoc');
    Route::get('/pendingdoc', 'Admin\DoctorController@pendingdoc')->name('pendingdoc');
	Route::post('doctor/viewdata', 'Admin\DoctorController@viewdata')->name('doctor.viewdata');
	Route::post('doctor/linkdata', 'Admin\DoctorController@linkdata')->name('doctor.linkdata');
	Route::get('doctor/export', 'Admin\DoctorController@export')->name('doctor.export');
	
    Route::get('/doctors/add', 'Admin\DoctorController@add')->name('doctors.add');
	Route::post('/doctors/storedata', 'Admin\DoctorController@storedata')->name('doctors.storedata');
	
    Route::post('/doctors/import-doctors', 'Admin\DoctorController@importDoctors')->name('doctors.import');
    Route::get('/doctors/information/{id}', 'Admin\DoctorController@information')->name('doctors.information');
	Route::get('/doctors/export', 'Admin\DoctorController@export')->name('doctors.export');

    // PRODUCTS
    Route::get('/products', 'Admin\ProductController@index')->name('products');
    Route::get('/products/add', 'Admin\ProductController@add')->name('products.add');
    Route::post('/products/import-products', 'Admin\ProductController@importProducts')->name('products.import');
    Route::get('/products/information/{id}', 'Admin\ProductController@information')->name('products.information');
    Route::post('/products/focus-status', 'Admin\ProductController@focusStatus')->name('products.focusStatus');

    // NOTIFICATIONS
    Route::get('/notifications', 'Admin\NotificationController@index');
    Route::post('/notifications/filter-records', 'Admin\NotificationController@filterRecords');
    Route::post('/notifications/send', 'Admin\NotificationController@send');

    // REPORTS
    Route::get('/potential-vs-support', 'Admin\ProductSupportController@index');
    Route::post('/potential-vs-support/filter-records', 'Admin\ProductSupportController@filterRecords');
    Route::get('/doctor-wise-report', 'Admin\DoctorReportController@index');
    Route::post('/doctor-wise-report/doctor-details', 'Admin\DoctorReportController@doctorDetails');
    Route::post('/doctor-wise-report/list-doctors', 'Admin\DoctorReportController@listDoctors');
    Route::post('/doctor-wise-report/filter-records', 'Admin\DoctorReportController@filterRecords');
    Route::get('/rxbers-category', 'Admin\RxbersCategoryController@index');
    Route::any('/all-india-rio-file', 'Admin\AllIndiaRoiFileController@index');
    Route::post('/rxbers-category/filter-records', 'Admin\RxbersCategoryController@filterRecords');
	
	 // PRODUCTS
    Route::get('/limits', 'Admin\LimitController@index')->name('limits');
	Route::get('limits/create', 'Admin\LimitController@create')->name('limits.create');
	Route::post('limits/store', 'Admin\LimitController@store')->name('limits.store');
	Route::get('limits/edit/{id}', 'Admin\LimitController@edit')->name('limits.edit');
	Route::post('limits/update/{id}', 'Admin\LimitController@update')->name('limits.update');
    
});


