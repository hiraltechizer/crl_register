<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'API\UserController@register');

Route::post('login', 'API\UserController@login');

Route::get('educationdetails', 'API\EducationController@educationdetails');

Route::post('uploadfile', 'API\UserController@uploadimage');

Route::post('doctordetails', 'API\DoctorController@doctorlist');

Route::post('insertdoctor', 'API\DoctorController@createdoctor');

Route::post('doctordetails', 'API\DoctorController@doctorlist');

Route::post('viewdoctor', 'API\DoctorController@doctordetail');

Route::post('editdoctor/{id}', 'API\DoctorController@updatedoctor');

Route::post('insertclinic', 'API\ClinicController@addclinic');

Route::post('editclinic/{id}', 'API\ClinicController@editclinic');

Route::post('editdoctor/{id}', 'API\DoctorController@updatedoctor');

Route::group(['middleware' => 'auth:api'], function(){

Route::post('details', 'API\UserController@details');

});