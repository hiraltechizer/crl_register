@extends('adminlte::page')
<title> Digidoc | Completed Doc </title>
@section('content_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
     <h1 class="page-title">Completed Doc</h1>
@stop

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="card-tools">
          <form method="get" action="">
            <div class="input-group input-group-sm">
              <input type="text" name="name" class="form-control float-right" value="" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
              </div>
              <a href="" class="btn btn-primary btn-sm m-l-5">Clear</a>
            </div>
          </form>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="10%">#</th>
              <th width="35%">Doctor Name</th>
              <th width="35%">Email</th>
              <th width="20%">Mobile</th>
              <th width="20%">Education</th>
              <th width="20%">Specialization</th>
              <th width="20%">Experience</th>
              <th class="status-td center" width="10%">Status</th>
              <th class="center" width="12%">Action</th>
            </tr>
          </thead>
          <tbody>
            @if(count(@$result)==0)
            <tr>
              <td class="center" colspan="5">No record found.</td>
            </tr>
            @else 
            <?php $j= ($offset!='')?$offset:0;?>
            @foreach(@$result as $row)
            <tr id="data-{{$row->id}}">
              <td>{{++$j}}</td>
              <td>{{$row->doctorFname.' '.$row->doctorLname}}</td>
              <td>{{$row->doctorEmail}}</td>
              <td>{{$row->doctorMobile}}</td>
              <td>{{$row->education}}</td>
              <td>{{$row->doctorSpecialization}}</td>
              <td>{{$row->doctorExperience}}</td>
              <td class="status-td center">{{$row->status}}</td>
              <td class="center actions-td">
               <a href="javascript:void(0);" title="view" id="{{$row->id}}"   class="viewBtn" ><i class="fa fa-eye" aria-hidden="true"></i></a>
			    @if($row->businesscardFlag == "Complete")              
				<a href="javascript:void(0);" title="link" id="{{$row->id}}" class="linkBtn" ><i class="fa fa-link" aria-hidden="true"></i></a>
				@endif  
                <!--<a href="" class="" ><i class="fa fa-trash" aria-hidden="true"></i></a>-->
              </td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div class="pagination-wrapper">
      {{ $result->links() }}
    </div>
  </div>
</div>
<input type="hidden" id="tbl" value="{{@$tbl}}" />
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Completed Doc</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="BindData">
				
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div> 
<div class="modal fade schedule" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Schedule list</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="SchData">
				
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>	
<div class="modal fade linkdata" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Url Link</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="linkBindData">
				
			</div>
			<div class="card-footer">
						<a href="javscript:void(0);" id="copyButton" class="btn btn-outline-primary waves-effect">Copy link</a>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					  </div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div> 		
@stop
@section('adminlte_js')
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
 $(document).on("click", ".viewBtn", function(){
 var id = $(this).attr("id");
 $.ajax({
			url:'{{ route("doctor.viewdata") }}',
			type:'post',
			data:{"id":id,  "_token": "{{ csrf_token() }}"},
			success:function(data)
			{
			   
				$(".bs-example-modal-lg").modal("show");
				$("#BindData").html(data);
			}
		  });
	
 });
$(document).on("click", ".linkBtn", function(){
 var id = $(this).attr("id");
 $.ajax({
			url:'{{ route("doctor.linkdata") }}',
			type:'post',
			data:{"id":id,  "_token": "{{ csrf_token() }}"},
			success:function(data)
			{
			   console.log(data);
				$(".linkdata").modal("show");
				$("#linkBindData").html(data);
			}
		  });
	
 });  
 $(document).on("click", ".schBtn", function(){
 var id = $(this).attr("id");

 $.ajax({
			url:'{{ route("doctor.viewschedule") }}',
			type:'post',
			data:{"id":id,  "_token": "{{ csrf_token() }}"},
			success:function(data)
			{
			   console.log(data);
				$(".bs-example-modal-lg").modal("hide");
				$(".schedule").modal("show");
				$("#SchData").html(data);
			}
		  });
	
 });
document.getElementById("copyButton").addEventListener("click", function() {
    copyToClipboard(document.getElementById("copyTarget"));
}); 
});
function copyToClipboard(elem) {
	  // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
    	  succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
</script>

@stop