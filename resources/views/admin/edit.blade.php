@extends('adminlte::page')


@section('css')
<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
      .custom-control-label{
        cursor: pointer;
      }
    </style>
@stop

@section('content_header')
<h1 class="page-title">Update Limits</h1>
    <a class="page-btn" href="{{url('admin/users/')}}"><button class="btn btn-block btn-default import-btn">Back</button></a>
@stop

@section('content')

<div class="row">
<div class="col-md-12">
  <!-- general form elements -->
  <div class="card card-primary">
    
    <!-- form start -->
    <form role="form" id="quickForm" action="{{ route('limits.update',$row[0]->id) }}" method="post">
    @csrf     
	<input type="hidden" value="{{ $row[0]->id }}" name="id" id="id">
      <div class="card-body">
		   <div class="form-group">
			<label for="firstname">User Name</label>
			<select id="doctorid" name="doctorid" class="form-control">
			<option value="">-select user-</option>
			@foreach($result as $data)
			@if($data->firstname == $row[0]->doctorid)
			<option value="{{$data->fullname}}" selected>{{$data->fullname}}</option>	
			@else
			<option value="{{$data->fullname}}">{{$data->fullname}}</option>
			@endif
			@endforeach
			</select>
		</div>
		  <div class="form-group">
			<label for="lastname">Limit</label>
			<input type="text" name="limitdata" maxlength="3" value="{{$row[0]->limitdata}}" class="form-control" id="limitdata" placeholder="Enter doc limit">
		  </div>
		  <div class="form-group mb-0">
			<div class="custom-control custom-checkbox">
			  <input type="checkbox" name="status" value="Active" @if($row[0]->status == "Active") checked  @endif class="custom-control-input" id="exampleCheck1">
			  <label class="custom-control-label" for="exampleCheck1">Status </label>
			</div>
		  </div>
		</div>

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
       <a href="../../limits" class="btn btn-danger">Cancel</a>
      </div>
    </form>
  </div>
  <!-- /.card -->
</div>
</div>
@stop
@section('adminlte_js')
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      $('#quickForm').submit();
    }
  });
  $('#quickForm').validate({
    rules: {
	 doctorid: {
		required: true 
	 },
	limitdata: {
		required: true 
	 },
	},
    messages: {
	  doctorid: {
		 required: "Please select user name",  
	  }, 
	  limitdata: {
		 required: "Please enter a limit",  
	  },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@stop