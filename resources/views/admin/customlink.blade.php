@extends('adminlte::page')

@section('content_header')
<h1 class="page-title"></h1>
    <a class="page-btn" href=""><button class="btn btn-block btn-default import-btn">Add</button></a>
@stop

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="card-tools">
          <form method="get" id="frm_search_users" action="">
            <div class="input-group input-group-sm">
             
              <input type="text" name="name" class="form-control float-right" value="" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
              </div>
              <a href="{{url('admin/users')}}" class="btn btn-primary btn-sm m-l-5">Clear</a>
            </div>
          </form>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">#</th>
              <th width="20%">Winner Name</th>
              <th width="20%">Created Date</th>
              <th width="12%">Link</th>
              <th width="13%">Expiry Date</th>
              <th class="status-td center" width="8%">Status</th>
              <th class="center" width="15%">Action</th>
            </tr>
          </thead>
          <tbody>
            
            <tr>
              <td>1</td>
              <td>Darshan</td>
              <td>10-05-2020</td>
              <td>Sdasd</td>
              <td>10-05-2020</td>
              <td>Active</td>
              <td class="center actions-td">
                <a href="javascript:void(0);"  class=""><i class="fa fa-link" aria-hidden="true"></i></a>
              </td>
            </tr>
          
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div class="pagination-wrapper">
     
   
    </div>
  </div>
</div>
<input type="hidden" id="tbl" value="{{@$tbl}}" />
@stop