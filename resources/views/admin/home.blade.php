@extends('adminlte::page')
<title> CareNShare | Dashboard </title>
@section('content_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />     
	 <h1 class="page-title">Dashboard</h1>
@stop
@section('content')
<?php
$currentdate = date("m/d/Y");
$beforeseven = date( "m/d/Y", strtotime( "$currentdate -7 day" ) ); 
?>
	<section class="content">
		<div class="container-fluid">
			<!-- Filter User -->
			<form id="homeform" name="homeform" method="get" action="{{url('admin/users')}}">
			@csrf
			<div class="row">
			<div class="col-12 col-sm-6 col-md-4">
					<div class="form-group">
						  <label>Date range:</label>
						  <div class="input-group">
							<div class="input-group-prepend">
							  <span class="input-group-text"><i class="far fa-clock"></i></span>
							</div>
							<input type="text" name="daterange" value="<?= $beforeseven?> - <?= $currentdate?>" class="form-control float-right" id="reservationtime">
						  </div>
					</div>
                </div>
				<!--<div class="col-12 col-sm-6 col-md-2">
					<div class="form-group">
						<label></label>
									<button type="button" style="margin-top: 19%;" class="btn btn-info searchBtn">Search</button>
						</div>
                </div>-->
			</div>
			<div class="row">
				
               <div class="col-12 col-sm-6 col-md-4">
					<div class="info-box">
					<span class="info-box-icon bg-secondary elevation-1">
						<i class="fas fa-fw fa-users "></i>
					</span>
						<div class="info-box-content">
						<a href="javascript:void(0);" class="sbmBtn" title="Total Registered Users">
							<span class="info-box-text">Total Registered Users</span>
							<span class="info-box-number" id="total_users">{{$usercount}}</span>
							</a>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				
			
				<!-- /.col -->
			</div>
			</form>
			<!-- /.row -->

		
				<!-- ./col -->
			</div>

            
            <!-- /.row -->


            

            
		</div>
		<!-- /.container-fluid -->
	</section>
	
   
	
	<input type="hidden" id="curr_sel_type" value="" />


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Doctor Detail</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="BindData">
				
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div> 
	
<div class="modal fade schedule" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Schedule list</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="SchData">
				
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div> 	
@stop
@section('adminlte_js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<script>
$(function() {
	
  $('input[name="daterange"]').daterangepicker({
     maxDate: new Date()
  }, function(start, end, label) {
	 
	//var times =   start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY');
    //$("#date").val(times);
  });
});
$(document).ready(function(){
$(document).on("click", ".sbmBtn", function(){
	$("#homeform").submit();
});
$(document).on("click", ".btn-primary", function(){
	 $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	var range = $("#reservationtime").val();
	$.ajax({
			url:'{{ route("users.getcount") }}',
			type:'post',
			data:{"range":range
				  },
			success:function(data)
			{
				console.log(data);
				$("#total_users").text(data);
			}
		  });
});
	
});
</script>

@stop
