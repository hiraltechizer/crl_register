@extends('adminlte::page')
<title> Sun Pharma | Doctor </title>
@section('content_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
     <h1 class="page-title">Doctor list</h1>
@stop

@section('content')


<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="card-tools">
          <form method="get" id="frm_search_users" action="">
            <div class="input-group input-group-sm">
             <div align="right" style="margin-right: 2%;">
				<a href="{{ route('doctor.export') }}" class="btn btn-primary waves-effect btn-label waves-light"><i class="bx bx-download  label-icon"></i>Download Report</a>
				</div></br>
              <input type="text" name="name" class="form-control float-right" value="{{@$name}}" placeholder="Search" style="height: auto;">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search" ></i></button>
              </div>
              @if(@$name!='')
              <a href="{{url('admin/users')}}" class="btn btn-primary btn-sm m-l-5">Clear</a>
              @endif
            </div>
          </form>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="10%">#</th>
              <th width="35%">Doctor Name</th>
              <th width="35%">Speciality</th>
              <th width="20%">Hospital</th>
              <th width="20%">City</th>
              <th width="20%">Message</th>
              <th class="status-td center" width="10%">Status</th>
              <th class="center" width="12%">Action</th>
            </tr>
          </thead>
          <tbody>
            @if(count(@$result)==0)
            <tr>
              <td class="center" colspan="5">No record found.</td>
            </tr>
            @else 
            <?php $j= ($offset!='')?$offset:0;?>
            @foreach(@$result as $row)
            <tr id="data-{{$row->id}}">
              <td>{{++$j}}</td>
              <td>{{$row->doctorname}}</td>
              <td>{{$row->speciality}}</td>
              <td>{{$row->hospitalname}}</td>
              <td>{{$row->city}}</td>
			  @if($row->message == "A")
              <td>I will stand up for “Helping patient to quit smoking”.</td>
			  @elseif($row->message == "B")
				<td>I will stand up for “Reducing Air Pollution ” so that every one can breathe fresh air.</td>  
			  @elseif($row->message == "C")
				 <td>I will Stand up for “Helping patient to COMBAT COPD”.</td> 
			  @else
				  <td></td>
			  @endif
              <td class="status-td center">{{$row->status}}</td>
			  <td><a href="javascript:void(0);" title="view" id="{{$row->id}}"  class="viewBtn"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div class="pagination-wrapper">
      {{ $result->links() }}
    </div>
  </div>
</div>
<input type="hidden" id="tbl" value="{{@$tbl}}" />
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Doctor Detail</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="BindData">
				
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div> 
<div class="modal fade schedule" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Schedule list</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="SchData">
				
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

<div class="modal fade linkdata" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Url Link</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="linkBindData">
				
			</div>
			<div class="card-footer">
						<a href="javscript:void(0);" id="copyButton" class="btn btn-outline-primary waves-effect">Copy link</a>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					  </div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div> 	
@stop
@section('adminlte_js')
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>


@stop