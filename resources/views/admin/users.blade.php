@extends('adminlte::page')
<title> CareNShare | Users </title>
@section('content_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />     
    <h1 class="page-title">Registered user list</h1>

@stop
<?php
$currentdate = date("m/d/Y");
$beforeseven = date( "m/d/Y", strtotime( "$currentdate -7 day" ) ); 
?>
@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
		<form method="get" id="quickform" action="">
			<div class="col-12 col-sm-6 col-md-6">
					<div class="form-group">
						  <label>Date range:</label>
						  <div class="input-group">
							<div class="input-group-prepend">
							  <span class="input-group-text"><i class="far fa-clock"></i></span>
							</div>
							@if(isset($_SESSION['range']))
							<input type="text" name="daterange" value="{{$_SESSION['range']}}" class="form-control float-right" id="reservationtime">&nbsp;&nbsp;&nbsp;
							@else
							<input type="text" name="daterange" value="<?= $beforeseven?> - <?= $currentdate?>" class="form-control float-right" id="reservationtime">&nbsp;&nbsp;&nbsp;
							@endif
							<!--<button type="submit"  class="btn btn-info searchBtn">Search</button>&nbsp;&nbsp;&nbsp;&nbsp;-->
							<!--<button type="button"  class="btn btn-info resetBtn">Reset</button>-->
						  </div>
					</div>
					</div>
         </form>     
        <div class="card-tools">
             <div align="right" style="margin-right: 2%;">
				<a href="{{ route('users.export') }}" style="width: 104%;margin-top: -37%;" class="btn btn-primary waves-effect btn-label waves-light"><i class="bx bx-download  label-icon"></i>Download Report</a>
				</div></br>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">#</th>
              <th width="30%">Date & Time</th>
              <th width="10%">Name</th>
              <th width="10%">Gender</th>
              <th width="10%">City</th>
              <th width="10%">Preferred Language</th>
              <th width="10%">Mobile</th>
              <th width="10%">Doctor Name</th>
              <th width="10%">Doctor City</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody>
             @if(count(@$result)==0)
            <tr>
              <td class="center" colspan="7">No record found.</td>
            </tr>
            @else 
            <?php $j= ($offset!='')?$offset:0;?>
           
            @foreach(@$result as $row)
			<?php
				$length = strlen($row->city);
				$output[0] = substr($row->city, 0, 10);
				$output[1] = substr($row->city, 10, 10 );
				$output[2] = substr($row->city, 20, 10 );
				$output[3] = substr($row->city, 30, 10 );
				$city = $output[0].'</br>'.$output[1].'</br>'.$output[2].'</br>'.$output[3];
				
				$length = strlen($row->doctorcity);
				$outputs[0] = substr($row->doctorcity, 0, 10);
				$outputs[1] = substr($row->doctorcity, 10, 10 );
				$outputs[2] = substr($row->doctorcity, 20, 10 );
				$outputs[3] = substr($row->doctorcity, 30, 10 );
				$citys = $outputs[0].'</br>'.$outputs[1].'</br>'.$outputs[2].'</br>'.$outputs[3];

			
			?>
	
            <tr>
              <td>{{++$j}}</td>
              <td>{{date("d-m-Y g:i A", strtotime($row->created_at))}}</td>
              <td>{{$row->name}}</td>
              <td>{{$row->gender}}</td>
              <td><?=$city?></td>
			   @if(!empty($row->userlanguage))
              <td>{{$row->userlanguage}}</td>
				@else
              <td>-</td>
			@endif
              <td>{{$row->mobile}}</td>
              <td>{{$row->doctorname}}</td>
			  @if(!empty($row->doctorcity))
             <td><?=$citys?></td>
			 @else
              <td>-</td>
			@endif
              <td class="center actions-td">
                <a href="javascript:void(0);" class="viewBtn"  id="{{$row->id}}"><i class="fa fa-eye" aria-hidden="true"></i></a>
              </td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div class="pagination-wrapper">
      {{ $result->links() }}
   
    </div>
  </div>
</div>
<input type="hidden" id="tbl" value="{{@$tbl}}" />
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myLargeModalLabel">Users Detail</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="BindData">
				
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>  
@stop
@section('adminlte_js')
<script src="../../plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="../../plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../plugins/jquery-validation/additional-methods.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<script type="text/javascript">
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left',
	maxDate: new Date()
  }, function(start, end, label) {
	//var times =   start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY');
    //$("#reservationtime").val(times);
  });
});
</script>
<script>
$(document).ready(function () {

 $(document).on("click", ".viewBtn", function(){
 var id = $(this).attr("id");
 $.ajax({
			url:'{{ route("users.viewdata") }}',
			type:'post',
			data:{"id":id,  "_token": "{{ csrf_token() }}"},
			success:function(data)
			{
				console.log(data);
				$(".bs-example-modal-lg").modal("show");
				$("#BindData").html(data);
			}
		  });
	
 });
$(document).on("click", ".resetBtn", function(){
	 $("#reservationtime").val('');
	 $("#quickform").submit();
	
 });
 $(document).on("click", ".btn-primary", function(){
	 $("#quickform").submit();
	
 }); 
});
</script>

@stop