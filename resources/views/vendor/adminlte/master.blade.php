<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title> {{config('adminlte.title')}} | @yield('title', config('adminlte.title', ''))</title>
    @if(! config('adminlte.enabled_laravel_mix'))
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
	<link rel="shortcut icon" href="../frontend/images/ft-logo.png">

    @include('adminlte::plugins', ['type' => 'css'])

    @yield('adminlte_css_pre')

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin/pace.min.css')}}">
    @yield('adminlte_css')

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @else
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/custom.css?v='.config('custom_config.js_css_version')) }}">
</head>
<body class="@yield('classes_body')" @yield('body_data') style="min-height: 310.391px;background-image: url(../images/admin_Bg.png);">

@yield('body')
<div class="modal fade" id="cmn_delete_popup" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this record? it cannot be retrived back.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary confirm">Delete</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="cmn_preloader_popup" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <i class="fas fa-spinner fa-spin" aria-hidden="true"></i>
      </div>
    </div>
  </div>
</div>

@if(! config('adminlte.enabled_laravel_mix'))
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>


<script type="text/javascript" src="{{asset('js/admin/pace.min.js')}}"></script>
<script src="{{ asset('js/admin/jquery.validate.js') }}"></script>
<!-- <script src="{{ asset('js/admin/select2.min.js') }}"></script> -->
<script src="{{ asset('js/admin/bootstrap-multiselect.js') }}"></script>


@include('admin.includes.js_messages')
<script src="{{ asset('js/admin/custom.js?v='.config('custom_config.js_css_version')) }}"></script>
@include('adminlte::plugins', ['type' => 'js'])
@yield('adminlte_js')
@else
    <script src="{{ asset('js/app.js') }}"></script>
@endif
<script>
  $(function () {
	var activeurl = $("#activeurl").val();
	var expiredurl = $("#expiredurl").val();
   var donutData        = {
      labels: [
		  'Completed Doc', 
          'Pending Doc',
		  
      ],
      datasets: [
        {
          data: [expiredurl,activeurl],
          backgroundColor : ['#1199DB','#F99F4B'],
        }
      ]
    }
	var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions      
    })
});
</script>
</body>
</html>
