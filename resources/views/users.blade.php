@extends('adminlte::page')
@section('title', $title)
@section('content_header')
    <h1 class="page-title">{{@$title}}</h1>
@stop

@section('content')
@include('admin.includes.notification')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="card-tools">
          <form method="get" id="frm_search_users" action="">
            <div class="input-group input-group-sm">
              @if(@$sm!='')
              <select class="form-control select2 m-r-5" name="rbm" id="rbm" onchange="$('#frm_search_users').submit();">
                <option value="">----- Select RBM -----</option>
                @foreach($list_rbm as $row)
                <option value="{{$row->pos_code}}" {{($row->pos_code==$rbm)?'selected':''}}>{{$row->name}}</option>
                @endforeach
              </select>
              @endif
              @if(@$rbm!='')
              <select class="form-control select2 m-r-5" name="abm" id="abm" onchange="$('#frm_search_users').submit();">
                <option value="">----- Select ABM -----</option>
                @foreach($list_abm as $row)
                <option value="{{$row->pos_code}}" {{($row->pos_code==$abm)?'selected':''}}>{{$row->name}}</option>
                @endforeach
              </select>
              @endif
              <input type="text" name="name" class="form-control float-right" value="{{@$name}}" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
              </div>
              @if(@$name!='')
              <a href="{{url('users')}}" class="btn btn-primary btn-sm m-l-5">Clear</a>
              @endif
            </div>
          </form>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">Id</th>
              <th width="20%">Employee Name</th>
              <th width="10%">Position Code</th>
              <th width="15%">Employee Code</th>
              <th width="25%">Designation</th>
              <th width="10%">Position</th>
              <th class="center" width="15%">Action</th>
            </tr>
          </thead>
          <tbody>
            @if(count(@$result)==0)
            <tr>
              <td class="center" colspan="7">No record found.</td>
            </tr>
            @else 
            <?php $j= ($offset!='')?$offset:0;?>
            @foreach(@$result as $row)
            <tr id="data-{{$row->id}}">
              <td>{{++$j}}</td>
              <td>{{$row->name}}</td>
              <td>{{$row->pos_code}}</td>
              <td>{{$row->emp_code}}</td>
              <td>{{$row->designation}}</td>
              <td>{{$row->position_name}}</td>
              <td class="center actions-td">
                <a href="{{url('users/information/'.$row->id)}}" class="" ><i class="fa fa-eye" aria-hidden="true"></i></a>
              </td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div class="pagination-wrapper">
      {{ $result->links() }}
      
    </div>
  </div>
</div>
<input type="hidden" id="tbl" value="{{@$tbl}}" />
@stop

@section('css')
@stop