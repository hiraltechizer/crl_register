@extends('adminlte::page')
@section('title', $title)
@section('content_header')
	<h1 class="page-title">{{@$title}}</h1>
@stop

@section('content')
@include('admin.includes.notification')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="card-tools">
          <form method="get" id="frm_search_doctors" action="">
            <div class="input-group input-group-sm">
              @if(@$sm!='')
              <select class="form-control select2 m-r-5" name="rbm" id="rbm" onchange="$('#frm_search_doctors').submit();">
                <option value="">----- Select RBM -----</option>
                @foreach($list_rbm as $row)
                <option value="{{$row->pos_code}}" {{($row->pos_code==$rbm)?'selected':''}}>{{$row->name}}</option>
                @endforeach
              </select>
              @endif
              @if(@$rbm!='')
              <select class="form-control select2 m-r-5" name="abm" id="abm" onchange="$('#frm_search_doctors').submit();">
                <option value="">----- Select ABM -----</option>
                @foreach($list_abm as $row)
                <option value="{{$row->pos_code}}" {{($row->pos_code==$abm)?'selected':''}}>{{$row->name}}</option>
                @endforeach
              </select>
              @endif
              @if(@$abm!='')
              <select class="form-control select2 m-r-5" name="rm" id="rm" onchange="$('#frm_search_doctors').submit();">
                <option value="">----- Select RM -----</option>
                @foreach($list_rm as $row)
                <option value="{{$row->pos_code}}" {{($row->pos_code==$rm)?'selected':''}}>{{$row->name}}</option>
                @endforeach
              </select>
              @endif
              <input type="text" name="name" class="form-control float-right" value="{{@$name}}" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
              </div>
              @if(@$name!='')
              <a href="{{url('doctors')}}" class="btn btn-primary btn-sm m-l-5">Clear</a>
              @endif
            </div>
          </form>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="10%">Id</th>
              <th width="25%">Name</th>
              <th width="20%">Email</th>
              <th width="15%">Contact No</th>
              <th width="15%">City</th>
              <th class="center" width="15%">Action</th>
            </tr>
          </thead>
          <tbody>
            @if(count(@$result)==0)
            <tr>
              <td class="center" colspan="7">No record found.</td>
            </tr>
            @else 
            <?php $j= ($offset!='')?$offset:0;?>
            @foreach(@$result as $row)
            <tr id="data-{{$row->id}}">
              <td>{{++$j}}</td>
              <td>{{$row->name}}</td>
              <td>{{$row->email}}</td>
              <td>{{$row->contact_no}}</td>
              <td>{{$row->city}}</td>
              <td class="center 
              ">
                <a href="{{url('doctors/information/'.$row->id)}}" class="" ><i class="fa fa-eye" aria-hidden="true"></i></a>
              </td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div class="pagination-wrapper">
      {{ $result->links() }}
    </div>
  </div>
</div>
<input type="hidden" id="tbl" value="{{@$tbl}}" />
@stop