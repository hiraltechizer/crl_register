<!DOCTYPE html>
<html lang="en">
<head>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title>Mammitas</title>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../frontend/css/style.css" type="text/css" />
</head>
<body>
<!--#wrapper-->
<div id="wrapper">  
  <div class="mainContentBox">
  
  <div class="setpsWrap redBg steps2 " id="step2">
  <div class="contentPart">
  <div class="logoRow"><img src="../frontend/images/intas-logo.png" alt="INTAS Animal Health" title="INTAS Animal Health"></div>
  <div class="topWelText">Welcome you to<br>
Exclusive Product Launch</div>
  <div class="formWrap">
  <form id="registerForm1"  method="post" action="">
  <div class="textareaBox">
  <label for="cheifGuest">Chief Guest:</label>
  <textarea id="cheifGuest" name="cheifGuest" required></textarea>
  </div>
  <div class="btnRow">
  <input type="submit" class="nextBtn commonBtn" value="Next"  />
  </div>
  </form>
  </div>
  </div>  
  </div>
  <div class="setpsWrap steps3" id="step3">
    <div class="curatinWrapper">
    <img src="../frontend/images/curtail-left.jpg" id="curtain1" />
    <img src="images/curtain-right.jpg" id="curtain2" />    
    <div class="btnRow">
    <a href="#" class="commonBtn launchBtn">Launch</a>
    </div>
    <div class="showMainProdcut"><img src="../frontend/images/product-img.png" alt="Mammitas" title="Mammitas"></div>
    </div>
  </div>
  <div class="setpsWrap steps4" id="step4">
    <div class="curatinWrapper">  
  <div class="contentCenter">
  <div class="benefitsWrap">
  <div class="imgDiv img1"><img src="../frontend/images/mammitas-benefits-head.png" alt=""></div>
  <div class="imgDiv img2"><img src="../frontend/images/benefits-img-1.png" alt=""></div>
  <div class="imgDiv img3"><img src="../frontend/images/benefits-img-2.png" alt=""></div>
  <div class="imgDiv img4"><img src="../frontend/images/benefits-img-3.png" alt=""></div>
  </div>
  </div>
    </div>
  </div>  
  <div class="setpsWrap steps5" id="step5">
    <div class="curatinWrapper">  
  <div class="contentCenter thankYou"><img src="../frontend/images/thank-you-screen.png" alt="Thank You Screen"></div>
    </div>
  </div>
</div>

<!--/#wrapper--> 
<!-- Start Javascript --> 
<script src="../frontend/js/jquery.js" type="text/javascript"></script> 
<script src="../frontend/js/all-functions.js" type="text/javascript"></script>
<script src="../frontend/js/general.js" type="text/javascript"></script>
</body>
</html>
