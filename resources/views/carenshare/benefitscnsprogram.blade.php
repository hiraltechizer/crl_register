<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
   <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../frontend/images/ft-logo.png" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../frontend/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../frontend/css/style.css" media="all" type="text/css" />
    <title>Bone & Joint</title>
</head>


<body class="body_index body_activities benefits_cns_program" style="background: #EEEEEE;">
    <!----------------header start------------------->

    <header class="header_activities">
        <div class="container">
            <nav class="navbar navbar-expand-lg  navbar-dark p-0 d-flex justify-content-between">
                <a class="" href="{{ url('carenshare\activites') }}"><span class="back_arrow">Back</span></a>
                <a class="navbar-brand m-0" href="/"><img src="../frontend/images/logo_dark.svg" alt="logo" class="logo img-fluid"></a>
                <div>
                    <div class="navbar-collapse collapse show" id="navbarsExample03">
                        <div class="navbar-nav ml-auto"></div>
                    </div>
                </div>
            </nav>
        </div>
    </header>

    <!-- Modal -->
    <div class="modal fade p-0" id="JoinNow" tabindex="-1" role="dialog" aria-labelledby="JoinNow" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="SignupModalCenterTitle">Benefits of Care N Share Program</h5>
                </div>
                <div class="modal-body">
                    <iframe width="640" height="360" src="https://www.youtube.com/embed/tgbNymZ7vqY" data-toggle="modal" data-target="#JoinNow"></iframe>
                </div>
            </div>
        </div>
        <button type="button" class="btn modal_btn_close" data-dismiss="modal">
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" width="16.99" height="16.953" viewBox="0 0 16.99 16.953"> <path id="Path_2-8" data-name="Path 2-8" d="M5622.466-2293.481a1.543,1.543,0,0,0,1.077.447,1.533,1.533,0,0,0,1.078-.447l5.895-5.9,5.894,5.893a1.546,1.546,0,0,0,1.077.447,1.525,1.525,0,0,0,1.076-.447,1.5,1.5,0,0,0,.014-2.123l-.014-.013-5.893-5.893,5.893-5.893a1.512,1.512,0,0,0,0-2.138,1.511,1.511,0,0,0-2.136,0l-5.893,5.892-5.892-5.893a1.51,1.51,0,0,0-2.137,0,1.511,1.511,0,0,0,0,2.136l5.89,5.889-5.893,5.894a1.469,1.469,0,0,0-.1,2.077C5622.422-2293.525,5622.444-2293.5,5622.466-2293.481Z" transform="translate(-5622.019 2309.987)" fill="#056371" style="isolation: isolate"/> </svg>
        </span>
    </button>
    </div>

    <main role="main" class="main_activities">
        <section class="">
            <div class="container">
                <div class="benefits_cns_program_title">
                    <h2>Benefits of Care N Share Program</h2>
                    <p>Watch Video</p>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="">
                            <img src="../frontend/images/careprost_pack.png" alt="logo" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="">
                                <iframe width="640" height="360" src="../frontend/careshare/benefits/Latoprost.mp4" data-toggle="modal" data-target="#JoinNow"></iframe>
                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- <div class="container">
            <div class="row">
                <div class="inner">
                    <p class="join_now m-0">50% Glaucoma Cases Runs Within The Family. Early Detection Can Prevent Progressive Vision Loss</p>
                </div>
            </div>
        </div> -->
    </main>


    <script src="../frontend/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="../frontend/js/popper.min.js" type="text/javascript"></script>
    <script src="../frontend/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>