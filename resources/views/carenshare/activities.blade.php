<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../frontend/images/ft-logo.png" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../frontend/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../frontend/css/style.css" media="all" type="text/css" />
    <title>Bone & Joint</title>
</head>

<body class="body_activities">
    <!----------------header start------------------->

    <header class="header_activities">
        <div class="container">
            <nav class="navbar navbar-expand-lg  navbar-dark p-0 d-flex justify-content-between">
                <a class="" href="/"><span class="back_arrow">Back</span></a>
                <a class="navbar-brand m-0" href="/"><img src="../frontend/images/logo_white.svg" alt="logo" class="logo img-fluid"></a>
                <div>
                    <div class="navbar-collapse collapse show" id="navbarsExample03">
                        <div class="navbar-nav ml-auto"></div>
                    </div>
                </div>
            </nav>
        </div>
    </header>

    <main role="main" class="main_activities">
        <section class="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="d-flex align-items-center box_1">
                            <div class="icon_bg">
                                <img src="../frontend/images/carenshare_icon.png" alt="logo" class="img-fluid">
                            </div>
                            <div class="content">
                                <h1> Benefits Of Care N Share Program </h1>
                                <a  class="box_custom_btn btn" href="{{ url('carenshare\benefitscnsprogram') }}">Learn More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="d-flex align-items-center box_1 box_2">
                            <div class="icon_bg">
                                <img src="../frontend/images/about_glaucoma.png" alt="logo" class="img-fluid">
                            </div>
                            <div class="content">
                                <h1> Know More About Glaucoma </h1>
                                <a  class="box_custom_btn btn" href="{{ url('carenshare\aboutglaucoma') }}">Learn More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="d-flex align-items-center box_1 box_3">
                            <div class="icon_bg">
                                <img src="../frontend/images/eyedrops_icon.png" alt="logo" class="img-fluid">
                            </div>
                            <div class="content">
                                <h1> How To Instil Glaucoma Eye Drops </h1>
                                <a  class="box_custom_btn btn" href="{{ url('carenshare\glaucomaeyedrops') }}">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>


    <script src="../frontend/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="../frontend/js/popper.min.js" type="text/javascript"></script>
    <script src="../frontend/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>