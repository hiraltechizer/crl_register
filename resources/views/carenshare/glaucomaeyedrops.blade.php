<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YT7TRLL0Q6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-YT7TRLL0Q6');
</script>
    <meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../frontend/images/ft-logo.png" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../frontend/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../frontend/css/style.css" media="all" type="text/css" />
    <title>Bone & Joint</title>
</head>
@php
$lan = isset($_SESSION['lan'])?$_SESSION['lan']:"";
@endphp
<body class="body_index body_activities benefits_cns_program" style="background: #EEEEEE;">
    <!----------------header start------------------->

    <header class="header_activities">
        <div class="container">
            <nav class="navbar navbar-expand-lg  navbar-dark p-0 d-flex justify-content-between">
                <a class="" href="{{ url('carenshare\activites') }}"><span class="back_arrow">Back</span></a>
                <a class="navbar-brand m-0" href="/"><img src="../frontend/images/logo_dark.svg" alt="logo" class="logo img-fluid"></a>
                <div>
                    <div class="navbar-collapse collapse show" id="navbarsExample03">
                        <div class="navbar-nav ml-auto"></div>
                    </div>
                </div>
            </nav>
        </div>
    </header>

    <!-- Modal -->
    <div class="modal fade p-0" id="GlaucomaEyeDropsVideo" tabindex="-1" role="dialog" aria-labelledby="GlaucomaEyeDropsVideo" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="SignupModalCenterTitle">Watch Video To Glaucoma Eye Drops</h5>
                    <div class="language-picker js-language-picker">
                        <form action="" class="language-picker__form">
                            <label for="language-picker-select"><svg id="language-24px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path id="Path_2472" data-name="Path 2472" d="M0,0H24V24H0Z" fill="none"/> <path id="Path_2473" data-name="Path 2473" d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z" fill="#fff"/> </svg></label>
                            <select name="language-picker-select" class="eyegetvideos" id="language-picker-select">
								@foreach($videos as $row)
								@if(isset($_SESSION['Eyevideos']))
										<option  value="{{$row->language}}" @if($row->language == $_SESSION['Eyevideos'][0]->language) selected @endif >{{$row->language}}</option>
								@else
											@if(isset($lan))
												<option  value="{{$row->language}}" @if($row->language == $lan) selected @endif>{{$row->language}}</option>
											@else
												<option  value="{{$row->language}}">{{$row->language}}</option>
											@endif
								@endif
							@endforeach
                          </select>
                        </form>
                    </div>
                </div>
               <div class="modal-body" id="eyevideoBind">
				@if(isset($_SESSION['Eyevideos']))
					<video controls width="670" height="390"> <source src="{{$_SESSION['Eyevideos'][0]->url}}" type="video/mp4"></video>
            			@elseif($lan == "English")
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/English.mp4" type="video/mp4"></video>
						@elseif($lan == "Hindi")  
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/Hindi.mp4" type="video/mp4"></video>
						@elseif($lan == "Bengali")  
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/Bengali.mp4" type="video/mp4"></video>
						@elseif($lan == "Gujarati")  
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/Gujarati.mp4" type="video/mp4"></video>
						@elseif($lan == "Kannada")  
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/Kannada.mp4" type="video/mp4"></video>
						@elseif($lan == "Malayalam")  
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/Malyalam.mp4" type="video/mp4"></video>
						@elseif($lan == "Marathi")  
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/Marathi.mp4" type="video/mp4"></video>
						@elseif($lan == "Tamil")  
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/Tamil.mp4" type="video/mp4"></video>
						@elseif($lan == "Telugu")  
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/Telegu.mp4" type="video/mp4"></video>
						@else
							<video controls width="670" height="390"> <source src="../frontend/careshare/eyedrops/English.mp4" type="video/mp4"></video>
						@endif
			    </div>
            </div>
        </div>
        <button type="button" class="btn modal_btn_close" id="videoclose" data-dismiss="modal">
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" width="16.99" height="16.953" viewBox="0 0 16.99 16.953"> <path id="Path_2-8" data-name="Path 2-8" d="M5622.466-2293.481a1.543,1.543,0,0,0,1.077.447,1.533,1.533,0,0,0,1.078-.447l5.895-5.9,5.894,5.893a1.546,1.546,0,0,0,1.077.447,1.525,1.525,0,0,0,1.076-.447,1.5,1.5,0,0,0,.014-2.123l-.014-.013-5.893-5.893,5.893-5.893a1.512,1.512,0,0,0,0-2.138,1.511,1.511,0,0,0-2.136,0l-5.893,5.892-5.892-5.893a1.51,1.51,0,0,0-2.137,0,1.511,1.511,0,0,0,0,2.136l5.89,5.889-5.893,5.894a1.469,1.469,0,0,0-.1,2.077C5622.422-2293.525,5622.444-2293.5,5622.466-2293.481Z" transform="translate(-5622.019 2309.987)" fill="#056371" style="isolation: isolate"/> </svg>
        </span>
    </button>
    </div>

    <main role="main" class="main_activities">
        <section class="">
            <div class="container">
                <div class="benefits_cns_program_title">
                    <h2>How to Instil Glaucoma Eye Drops</h2>
                   <p style="opacity: 0;">Watch Video To Glaucoma Eye Drops</p>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12 ">
                        <div class="">
                            <img src="../frontend/images/careprost_pack.png" alt="logo" class="img-fluid">
                        </div>
                    </div>
                   <!--<div class="col-lg-6 col-md-12 text-center about_glaucoma mt-4">
                        <div class="about_glaucoma_box_1">
                            <div class="language-picker js-language-picker float-right mr-3">
                                    <label for="language-picker-select"><svg id="language-24px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path id="Path_2472" data-name="Path 2472" d="M0,0H24V24H0Z" fill="none"/> <path id="Path_2473" data-name="Path 2473" d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z" fill="#656565"/> </svg></label>
                                    <select name="language-picker-select" class="getvideos" id="language-picker-select">
									@foreach($videos as $row)
										@if(isset($_SESSION['Eyevideos']))
											<option  value="{{$row->language}}" @if($row->language == $_SESSION['Eyevideos'][0]->language) selected @endif >{{$row->language}}</option>
										@else
											<option  value="{{$row->language}}">{{$row->language}}</option>
										@endif
									@endforeach
									</select>
                            </div>
                            <div class="icon_bg">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="226" height="86" viewBox="0 0 226 86"> <defs> <clipPath id="clip-path"> <rect id="Rectangle_335" data-name="Rectangle 335" width="226" height="86" transform="translate(569 482)" fill="#fff"/> </clipPath> </defs> <g id="Mask_Group_11" data-name="Mask Group 11" transform="translate(-569 -482)" clip-path="url(#clip-path)"> <g id="YouTube_Logo_2017" transform="translate(572.184 500.6)"> <path id="Path_2475" data-name="Path 2475" d="M69.3,7.967A8.674,8.674,0,0,0,63.188,1.86C57.834.4,36.282.4,36.282.4S14.731.444,9.377,1.9A8.674,8.674,0,0,0,3.27,8.012c-1.62,9.514-2.248,24.012.044,33.145a8.674,8.674,0,0,0,6.107,6.107c5.355,1.46,26.906,1.46,26.906,1.46s21.551,0,26.906-1.46a8.674,8.674,0,0,0,6.107-6.107C71.048,31.629,71.574,17.141,69.3,7.967Z" transform="translate(0 0)" fill="red"/> <path id="Path_2476" data-name="Path 2476" d="M64.2,52.31,82.078,41.955,64.2,31.6Z" transform="translate(-34.777 -17.393)" fill="#fff"/> <g id="Group_2093" data-name="Group 2093" transform="translate(77.57 2.303)"> <path id="Path_2477" data-name="Path 2477" d="M197.295,48.691a7.507,7.507,0,0,1-2.921-4.293,28.218,28.218,0,0,1-.841-7.744V32.274a25.249,25.249,0,0,1,.974-7.833,7.635,7.635,0,0,1,3.1-4.293,10.174,10.174,0,0,1,5.487-1.372,9.241,9.241,0,0,1,5.355,1.372,8,8,0,0,1,2.965,4.293,26.526,26.526,0,0,1,.929,7.789V36.61a26.2,26.2,0,0,1-.929,7.744,7.776,7.776,0,0,1-2.965,4.293,10.124,10.124,0,0,1-5.532,1.372A9.883,9.883,0,0,1,197.295,48.691Zm7.656-4.735a14.131,14.131,0,0,0,.575-4.824V29.707a13.715,13.715,0,0,0-.575-4.735,2.042,2.042,0,0,0-1.991-1.5,1.993,1.993,0,0,0-1.947,1.5,13.885,13.885,0,0,0-.575,4.735v9.426a15.652,15.652,0,0,0,.531,4.824,1.981,1.981,0,0,0,1.991,1.46A2.052,2.052,0,0,0,204.951,43.956Zm97-7.213v1.549l.177,4.381a4.4,4.4,0,0,0,.708,2.124,2.43,2.43,0,0,0,3.761-.531,10.2,10.2,0,0,0,.62-3.939l5.886.354a7.206,7.206,0,0,1,.044,1.062c0,2.788-.752,4.868-2.3,6.24a9.367,9.367,0,0,1-6.461,2.08c-3.363,0-5.709-1.062-7.036-3.142s-2.036-5.355-2.036-9.736V31.831c.15-7.523,1.474-13.032,9.249-13.055a9.469,9.469,0,0,1,5.355,1.283,6.948,6.948,0,0,1,2.655,3.983,27.936,27.936,0,0,1,.752,7.479V36.7H301.953ZM302.838,24a4.211,4.211,0,0,0-.708,2.08c-.133.974-.177,4.425-.177,4.425v2.168h4.956V30.5c0,2.168-.044-3.407-.177-4.425a4.074,4.074,0,0,0-.708-2.124,2.032,2.032,0,0,0-1.593-.62A2.065,2.065,0,0,0,302.838,24ZM180.744,36.168,173,8.2h6.771s3.164,14.028,4.248,20.622h.177c1.23-7,4.337-20.622,4.337-20.622H195.3l-7.833,27.924V49.532h-6.682V36.168Z" transform="translate(-173 -6.651)" fill="#282828"/> <path id="A" d="M288.365,33.4V63.625h-5.31l-.575-3.717h-.133a7.007,7.007,0,0,1-6.505,4.2c-5.209-.013-5.788-4.425-5.841-8.143V33.4h6.815V55.571a6.581,6.581,0,0,0,.443,2.876c.628,1.23,2.257.916,3.142.31a3.54,3.54,0,0,0,1.195-1.372V33.4Z" transform="translate(-227.075 -20.699)" fill="#282828"/> <path id="Path_2478" data-name="Path 2478" d="M327.879,11.736h-6.771V47.625h-6.638V11.736H307.7V6.249h20.135v5.487Zm38.9,10.488a7.8,7.8,0,0,0-1.991-4.16,5.42,5.42,0,0,0-3.806-1.283,6.24,6.24,0,0,0-3.5,1.062,7.286,7.286,0,0,0-2.522,2.788h-.044V4.7h-6.549V47.581h5.62L354.7,44.7h.133a6.2,6.2,0,0,0,2.345,2.434,7.2,7.2,0,0,0,3.5.885,5.392,5.392,0,0,0,5.089-3.186c1.062-2.124,1.637-5.443,1.637-9.913V30.145A40.543,40.543,0,0,0,366.778,22.224Zm-6.24,12.347a38.809,38.809,0,0,1-.266,5.178,5.409,5.409,0,0,1-.929,2.655,2.141,2.141,0,0,1-1.726.8,2.986,2.986,0,0,1-2.7-1.593V24.437a4.1,4.1,0,0,1,1.195-2.036,2.17,2.17,0,0,1,3.407,0,6.334,6.334,0,0,1,.8,2.744c.133,1.283.221,3.1.221,5.487Z" transform="translate(-248.091 -4.7)" fill="#282828"/> </g> <path id="Path_2479" data-name="Path 2479" d="M288.365,33.4V63.625h-5.31l-.575-3.717h-.133a7.007,7.007,0,0,1-6.505,4.2c-5.209-.013-5.788-4.425-5.841-8.143V33.4h6.815V55.571a6.581,6.581,0,0,0,.443,2.876c.628,1.23,2.257.916,3.142.31a3.54,3.54,0,0,0,1.195-1.372V33.4Z" transform="translate(-114.589 -18.397)" fill="#282828"/> </g> </g> </svg>
                            </div>
                            <div class="content">
                                <h6> Know More About Glaucoma Watch Video To Know About Glaucoma </h6>
                                <button type="button" class="box_custom_btn btn" href="#" data-toggle="modal" data-target="#GlaucomaEyeDropsVideo">Click Here</button>
                            </div>
                        </div>
                    </div>-->
					<div class="col-lg-6 col-md-12 text-center about_glaucoma mt-4">
                        <div class="about_glaucoma_box_1 ">
                            <div class="language-picker js-language-picker float-right mr-3">
                                <form action="" class="language-picker__form">
                                    <label for="language-picker-select"><svg id="language-24px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path id="Path_2472" data-name="Path 2472" d="M0,0H24V24H0Z" fill="none"/> <path id="Path_2473" data-name="Path 2473" d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z" fill="#656565"/> </svg></label>
                                    <select name="language-picker-select" class="getvideos" id="language-picker-select">
                                    @foreach($videos as $row)
										@if(isset($_SESSION['Eyevideos']))
											<option  value="{{$row->language}}" @if($row->language == $_SESSION['Eyevideos'][0]->language) selected @endif >{{$row->language}}</option>
										@else
											@if(isset($lan))
												<option  value="{{$row->language}}" @if($row->language == $lan) selected @endif>{{$row->language}}</option>
											@else
												<option  value="{{$row->language}}">{{$row->language}}</option>
											@endif
											
										@endif
									@endforeach
                                  </select>
                                </form>
                            </div>
                            <div class="icon_bg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="100" height="86" viewBox="0 0 100 86"> <g id="Group_2101" data-name="Group 2101" transform="translate(-11020 -20874)"> <rect id="Rectangle_335" data-name="Rectangle 335" width="100" height="86" transform="translate(11020 20874)" fill="rgba(255,255,255,0)"/> <g id="multimedia" transform="translate(11034 20854)"> <g id="Group_2097" data-name="Group 2097" transform="translate(2.138 33.138)"> <path id="Path_2486" data-name="Path 2486" d="M15,406v8.551H83.695V406H15Z" transform="translate(-15 -354.693)" fill="#fff"/> <path id="Path_2487" data-name="Path 2487" d="M49.347,46H15v8.551H83.695V46Z" transform="translate(-15 -46)" fill="#fff"/> </g> <path id="Path_2488" data-name="Path 2488" d="M256,46h34.347v8.551H256Z" transform="translate(-219.515 -12.862)" fill="#fff"/> <path id="Path_2489" data-name="Path 2489" d="M256,406h34.347v8.551H256Z" transform="translate(-219.515 -321.555)" fill="#fff"/> <path id="Path_2490" data-name="Path 2490" d="M62.21,106H30v42.756H94.419V106Z" transform="translate(-25.724 -64.311)" fill="#29a9ac"/> <path id="Path_2491" data-name="Path 2491" d="M256,106h32.21v42.756H256Z" transform="translate(-219.515 -64.311)" fill="#31bfa7"/> <g id="Group_2100" data-name="Group 2100" transform="translate(0 31)"> <g id="Group_2098" data-name="Group 2098"> <path id="Path_2492" data-name="Path 2492" d="M0,391v12.827H72.97V391H0Zm11.544,8.551H4.276v-4.276h7.269Zm11.4,0H15.82v-4.276h7.126Zm11.4,0H27.221v-4.276h7.126Zm27.079-4.276h7.269v4.276H61.426Zm-11.4,0h7.126v4.276H50.025Zm-11.4,0h7.126v4.276H38.623Z" transform="translate(0 -339.693)" fill="#056371"/> <path id="Path_2493" data-name="Path 2493" d="M36.485,31H0V43.827H72.97V31ZM11.544,39.551H4.276V35.276h7.269Zm11.4,0H15.82V35.276h7.126Zm11.4,0H27.221V35.276h7.126Zm11.4,0H38.623V35.276h7.126Zm11.4,0H50.025V35.276h7.126Zm11.544,0H61.412V35.276h7.283Z" transform="translate(0 -31)" fill="#056371"/> </g> <g id="Group_2099" data-name="Group 2099" transform="translate(36.485)"> <path id="Path_2494" data-name="Path 2494" d="M265.264,39.551h-7.126V35.276h7.126Zm11.4,0h-7.126V35.276h7.126ZM256,31V43.827h36.485V31Zm32.21,8.551h-7.283V35.276h7.283Z" transform="translate(-256 -31)" fill="#056371"/> <path id="Path_2495" data-name="Path 2495" d="M292.485,391H256v12.827h36.485Zm-27.221,8.551h-7.126v-4.276h7.126Zm11.4,0h-7.126v-4.276h7.126Zm11.544,0h-7.269v-4.276h7.269Z" transform="translate(-256 -339.693)" fill="#056371"/> </g> </g> <path id="Union_9" data-name="Union 9" d="M-10891-20160.932l6.415,4.275,12.4,8.268-12.4,8.273-6.415,4.275Z" transform="translate(10921.072 20211.455)" fill="#fff"/> </g> </g> </svg>
                            </div>
                            <div class="content">
                                <h6> Watch Video to know about how to take Glaucoma Eye Drops </h6>
                                <button type="button" class="box_custom_btn btn" style="background: #2FA6A5;" href="javascript:void(0);" data-toggle="modal" data-target="#GlaucomaEyeDropsVideo">Click Here</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="inner">
                    <p class="join_now m-0">50% Glaucoma Cases Runs Within The Family. Early Detection Can Prevent Progressive Vision Loss</p>
                </div>
            </div>
        </div>
    </main>


    <script src="../frontend/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="../frontend/js/popper.min.js" type="text/javascript"></script>
    <script src="../frontend/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function(){
    var video = document.getElementById('video');
    $('#videoModal').on('shown.bs.modal', function () {
        console.log('playing video');
        video.play();
    });
    $('#videoModal').on('hide.bs.modal', function () {
        console.log('pausing video');
        video.pause();
    });
});
</script>
<script>
$(document).ready(function(){
$(document).on("change", ".getvideos", function(){
 $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
  var lan = $(this).val();
	$.ajax({
		url:'{{ route("carenshare.eyevideos") }}',
		type:'post',
		data:{"lan":lan},
		success:function(data)
		{
		  if(data == "update"){
			window.location.reload(true);
			}
		}
	  });


});
$(document).on("change", ".eyegetvideos", function(){
 $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
  var lan = $(this).val();
	$.ajax({
		url:'{{ route("carenshare.eyevideosss") }}',
		type:'post',
		data:{"lan":lan},
		success:function(data)
		{
		 	$("#eyevideoBind").html(data);
			
		}
	  });


});
$(document).on("click", "#videoclose", function(){
	window.location.reload(true);
});
});
</script>	
</body>

</html>