<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YT7TRLL0Q6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-YT7TRLL0Q6');
</script>

    <meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../frontend/images/ft-logo.png" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../frontend/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../frontend/css/style.css" media="all" type="text/css" />
    <link rel="stylesheet" href="../frontend/css/indexpage.css" media="all" type="text/css" />
    <title>Bone & Joint</title>
<!--<style>
 .body_index {
            background-repeat: repeat;
        }
        
        .footer_managed_by {
            position: absolute;
            left: 0;
            bottom: -45px;
            width: 100%;
            background-color: #31bfa7;
            color: white;
            text-align: center;
        }
        
        .body_index .inner {
            bottom: -1px;
        }
        
        .managed_by {
            font-size: 14px;
            padding: 12px;
        }
        
        .sec_one {
            -webkit-transform: none !important;
                -ms-transform: none !important;
                    transform: none !important;
            position: unset;
            width: 100vw;
            height: 100vh;
            padding: 0!important;
            display: table-cell;
            vertical-align: middle;
        }
        
        @media screen and (max-width: 1919px) {
            .footer_managed_by {
                bottom: -44px;
            }
        }
        
        @media screen and (max-width: 1279px) {
            .managed_by {
                padding: 12px;
            }
        }
        
        @media screen and (max-width: 991px) {
            .footer_managed_by {
                bottom: -90px;
                padding: 18px;
            }
            .managed_by {
                padding: 3px;
            }
            .body_index .inner {
                position: absolute;
            }
        }
        
        @media screen and (max-width: 767px) {
            .body_index .inner {
                padding: 16px 0;
            }
        }
        
        @media screen and (max-width: 599px) {
            .managed_by {
                font-size: 12px;
            }
            .footer_managed_by {
                bottom: -72px;
                padding: 12px 0;
            }
        }
        
        @media screen and (max-width: 413px) {
            .footer_managed_by {
                bottom: -90px;
            }
        }
        
        @media screen and (min-width: 992px) and (max-width: 1279px) and (orientation: landscape) {
            section.sec_one {
                zoom: 1;
            }
            .footer_managed_by {
                bottom: -44px;
            }
            .body_index .sec_one .typography {
                margin: 20% 10% 20% 10%;
                zoom: 0.60;
            }
            html {
                zoom: 0.8;
            }
            .footer {
                position: relative;
                width: 100%;
                bottom: 0 !important;
				height: 20vh;
            }
        }
        
        @media screen and (min-width: 768px) and (max-width: 991px) and (orientation: landscape) {
            .footer_managed_by {
                bottom: -88px;
            }
            section.sec_one {
                zoom: 0.8;
            }
            .body_index .sec_one .typography {
                margin: 20% 10% 20% 10%;
                zoom: 0.60;
            }
            html {
                zoom: 0.8;
            }
            .footer {
                position: relative;
                width: 100%;
                bottom: 0 !important;
                 height: 10vh;
            }
        }
        
        @media screen and (min-width: 320px) and (max-width: 767px) and (orientation: landscape) {
            section.sec_one {
                zoom: 0.6;
            }
            .body_index .sec_one .typography {
                margin: 26% 10%;
                zoom: 0.65;
            }
            .body_index .inner {
                bottom: -97px;
            }
            .footer_managed_by {
                bottom: -186px;
            }
            html {
                zoom: 0.8;
            }
            .footer {
                position: relative;
                width: 100%;
                bottom: 0 !important;
                height: 10vh;
            }
        }
    </style>-->
	
</head>

<body class="body_index">
    <!----------------header start------------------->

    <header class="header">
        <div class="">
            <nav class="navbar navbar-expand-lg  navbar-dark p-0">
                <a class="navbar-brand" href="#"><embed src="../frontend/images/logo.svg" alt="logo" class="logo img-fluid"></a>
                <div class="navbar-collapse collapse show" id="navbarsExample03">
                    <div class="navbar-nav ml-auto">
                        <button href="javascript:void(0);" type="button" class="btn home_custom_btn" onClick="window.location.href='javascript:void(0);';" data-toggle="modal" data-target="#JoinNow">Join Now</button>
                    </div>
                </div>
            </nav>
        </div>
    </header>

    <!-- Modal -->
    <div class="modal fade p-0" id="JoinNow" tabindex="-1" role="dialog" aria-labelledby="JoinNow" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="SignupModalCenterTitle">Register Yourself For Free Patient Support Benefits </h5>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{ url('carenshare/activities') }}" id="quickform" enctype="multipart/form-data" method="post">
						@csrf
							<div class="input-group">
								<div class="col-lg-6">
									<input placeholder="Name *" maxlength="50" id="name" name="name" type="taxt"  class="input_box_1 form-control nickname">
									<span id="name_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>
								<div class="col-lg-6">
									<input placeholder="Age *" maxlength="3" id="age" name="age" type="taxt"  class="input_box_2 form-control">
									<span id="age_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>						
							</div>						
                        	<div class="input-group">
								<div class="col-lg-6">
									<select placeholder="Gender *" id="gender" name="gender" class="input_box_1 form-control">
										<option value="">Gender *</option>
										<option value="Male">Male</option>
										<option value="Female">Female</option>  
										<option value="Others">Others</option> 
									 </select>
									<span id="gen_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>
								<div class="col-lg-6">
									<input placeholder="Address of Patient *" id="address" maxlength="200" name="address" type="taxt" class="input_box_2 form-control">
									<span id="address_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>						
							</div>						
							<div class="input-group">
								<div class="col-lg-6">
									<input placeholder="City *" type="taxt" id="city" maxlength="50" name="city" class="input_box_1 form-control">
									<span id="city_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>
								<div class="col-lg-6">
									<select placeholder="State *" id="state" name="state" class="form-control input_box_2">
										<option value="">State * </option>
										@foreach($states as $row)
										<option value="{{$row->state_name}}">{{$row->state_name}}</option>  
										@endforeach
									</select>
									<span id="state_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>						
							</div>						
							<div class="input-group">
								<div class="col-lg-6">
									<input placeholder="Mobile Number * " type="taxt" id="mobile" maxlength="10"  name="mobile" class="input_box_1 form-control">
									<span id="mobile_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>
								<div class="col-lg-6">
									<input placeholder="Pincode * " type="taxt" id="pincode" maxlength="6" name="pincode" class="input_box_2 form-control">
									<span id="pin_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>						
							</div>						
							<div class="input-group">
								<div class="col-lg-6">
									<input placeholder="Doctor Name * " type="taxt" id="doctorname" maxlength="50" name="doctorname" class="input_box_1 form-control nickname">
									<span id="dname_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>
								<div class="col-lg-6">
									<input placeholder="City of Doctor" type="taxt" id="doctorcity" maxlength="50" name="doctorcity" class="input_box_2 form-control">
								</div>						
							</div>						
							<div class="input-group">
								<div class="col-lg-6">
									<select placeholder="Language *" id="langugae" name="langugae" class="form-control input_box_1">
										<option value="">Preferred Language * </option>
										@foreach($language as $lrow)
										<option value="{{$lrow->language}}">{{$lrow->language}}</option>  
										@endforeach
									</select>
									<span id="language_error_msg" style="color: red; margin-left: 3%;"></span>
								</div>
								<div class="col-lg-6">
								</div>								
							</div>						
                      
						<!--<div class="upload_document_file" id="firstdiv" style="display: none;">
                            <div class="input-group">
								<div class="col-lg-6">
									<div class="upload_document_1">
										
									</div>
                                </div>
								<div class="col-lg-6">
									<div class="upload_document_2">
									</div>
							    </div>
                            </div>
                        </div>-->
						<div class="upload_document_file">
                            <div class="input-group">
								<div class="col-lg-6">
									<div class="upload_document_1">
										<svg xmlns="http://www.w3.org/2000/svg" width="81" height="56" viewBox="0 0 81 56"> <g id="Group_2096" data-name="Group 2096" transform="translate(-857 -487)"> <g id="Rectangle_20" data-name="Rectangle 20" transform="translate(857 487)" fill="#f5f5f5" stroke="#d1d1d1" stroke-width="1"> <rect width="81" height="56" stroke="none"/> <rect x="0.5" y="0.5" width="80" height="55" fill="none"/> </g> <path id="Icon_ionic-md-add" data-name="Icon ionic-md-add" d="M29.25,19.5H19.5v9.75h-3V19.5H6.75v-3H16.5V6.75h3V16.5h9.75Z" transform="translate(915.25 497.25) rotate(90)" fill="#477cc0"/> </g> </svg>
										<label class="upload_document" for="inputGroupFile01">Upload Prescription </label>
										<input type="file" name="inputGroupFile01" accept="image/x-png,image/gif,image/jpeg,application/pdf" class="custom-file-input" id="inputGroupFile01">
										<span id="file1_error_msg" style="color: red;"></span>
										<div class="d-flex flex-row flex-md-row align-items-center mt-2">
										<img src="" id="prescriptionpreview" style="height: 40px; display: none;"></img>
										<label class="upload_document"><span style="font-size: 12px;color: #66666;" id="prescriptionname"></span></label>
										</div>
									</div>
                                </div>
								<div class="col-lg-6">
									<div class="upload_document_2">
										<svg xmlns="http://www.w3.org/2000/svg" width="81" height="56" viewBox="0 0 81 56"> <g id="Group_2096" data-name="Group 2096" transform="translate(-857 -487)"> <g id="Rectangle_20" data-name="Rectangle 20" transform="translate(857 487)" fill="#f5f5f5" stroke="#d1d1d1" stroke-width="1"> <rect width="81" height="56" stroke="none"/> <rect x="0.5" y="0.5" width="80" height="55" fill="none"/> </g> <path id="Icon_ionic-md-add" data-name="Icon ionic-md-add" d="M29.25,19.5H19.5v9.75h-3V19.5H6.75v-3H16.5V6.75h3V16.5h9.75Z" transform="translate(915.25 497.25) rotate(90)" fill="#477cc0"/> </g> </svg>
										<label class="upload_document" for="inputGroupFile02">Invoice/Bill</label>
										<input type="file" class="custom-file-input" name="inputGroupFile02" accept="image/x-png,image/gif,image/jpeg,application/pdf" id="inputGroupFile02">
										<div class="d-flex flex-row flex-md-row align-items-center mt-2">
										<img src="" id="invoicepreview" style="height: 40px; display: none;"></img>
										<label class="upload_document"><span style="font-size: 12px;color: #66666;" id="invoicename"></span></label>
										</div>
									</div>
							    </div>
                            </div>
                        </div>
						<!--<div class="upload_document_file" id="seconddiv" style="display: none;">
                            <div class="input-group">
								<div class="col-lg-6">
									<div class="upload_document_1">
										
									</div>
                                </div>
					        </div>
                        </div>-->
                        <!--<div class="upload_document_file mt-0">
							<div class="col-lg-6">
								<div class="upload_document_3">
									<svg xmlns="http://www.w3.org/2000/svg" width="81" height="56" viewBox="0 0 81 56"> <g id="Group_2096" data-name="Group 2096" transform="translate(-857 -487)"> <g id="Rectangle_20" data-name="Rectangle 20" transform="translate(857 487)" fill="#f5f5f5" stroke="#d1d1d1" stroke-width="1"> <rect width="81" height="56" stroke="none"/> <rect x="0.5" y="0.5" width="80" height="55" fill="none"/> </g> <path id="Icon_ionic-md-add" data-name="Icon ionic-md-add" d="M29.25,19.5H19.5v9.75h-3V19.5H6.75v-3H16.5V6.75h3V16.5h9.75Z" transform="translate(915.25 497.25) rotate(90)" fill="#477cc0"/> </g> </svg>
									<label class="upload_document" for="inputGroupFile03">Image of Bottle Purchased</label>
									<input type="file" class="custom-file-input" name="inputGroupFile03" accept="image/x-png,image/gif,image/jpeg,application/pdf" id="inputGroupFile03">
									<span id="file2_error_msg" style="color: red;"></span>
										<div class="d-flex flex-row flex-md-row align-items-center mt-2">
											<img src="" id="bottlepreview" style="height: 40px;  display: none;"></img>
											<label class="upload_document"><span id="bottlename"></span></label>
										</div>
								</div>
                            </div>
                        </div>-->
                        <div class="upload_note">
                            <span class="">(Only PDF, JPG, JPEG, & PNG files are allowed to upload with Max 5MB size.)</span>
                        </div>
						<div class="form-group form-check float-right w-100 mt-4" style="color: #000; padding-left: 47px;">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">I accept <a href="{{url('carenshare/termscondition')}}" target="_blank" style="color: #477cc0; font-weight: 500;">Terms & Conditions</a></label>
							</br><span id="check_error_msg" style="color: red;"></span>
                        </div>
						<div class="d-flex justify-content-start w-100">
					 		<button type="button" id="subBtn" class="modal_custom_btn btn">Submit</button>
						</div>
					</form>
                </div>
            </div>
        </div>
		<input type="hidden" id="checkboxid" name="checkboxid" value=""/>
        <button type="button" class="btn modal_btn_close" data-dismiss="modal">
            <span>
                <svg xmlns="http://www.w3.org/2000/svg" width="16.99" height="16.953" viewBox="0 0 16.99 16.953"> <path id="Path_2-8" data-name="Path 2-8" d="M5622.466-2293.481a1.543,1.543,0,0,0,1.077.447,1.533,1.533,0,0,0,1.078-.447l5.895-5.9,5.894,5.893a1.546,1.546,0,0,0,1.077.447,1.525,1.525,0,0,0,1.076-.447,1.5,1.5,0,0,0,.014-2.123l-.014-.013-5.893-5.893,5.893-5.893a1.512,1.512,0,0,0,0-2.138,1.511,1.511,0,0,0-2.136,0l-5.893,5.892-5.892-5.893a1.51,1.51,0,0,0-2.137,0,1.511,1.511,0,0,0,0,2.136l5.89,5.889-5.893,5.894a1.469,1.469,0,0,0-.1,2.077C5622.422-2293.525,5622.444-2293.5,5622.466-2293.481Z" transform="translate(-5622.019 2309.987)" fill="#056371" style="isolation: isolate"/> </svg>
            </span>
        </button>
    </div>

    <main role="main" class="main">
        <section class="sec_one">
            <div class="container">
				<div class="typography p-0">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="410" height="565" viewBox="0 0 410 565">
					  <defs>
						<style>
						  .cls-1 {
							font-size: 36.487px;
						  }

						  .cls-1, .cls-2, .cls-3, .cls-4 {
							fill: #fff;
							text-anchor: middle;
						  }

						  .cls-1, .cls-2, .cls-3 {
							font-family: Roboto;
						  }

						  .cls-1, .cls-3 {
							font-weight: 800;
						  }

						  .cls-2 {
							font-size: 25px;
						  }

						  .cls-2, .cls-4 {
							font-weight: 700;
						  }

						  .cls-3 {
							font-size: 34.055px;
						  }

						  .cls-4 {
							font-size: 56.017px;
							font-family: Arial;
						  }
						</style>
					  </defs>
					  <image id="Background" width="410" height="565" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZoAAAI1CAYAAADvpDzFAAAIHklEQVR4nO3VMQHAQAzEsLT8Od+j8CZB8OJv2w4AIr+wAJSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgA6d/cA5eoIZl3kCQ0AAAAASUVORK5CYII="/>
					  <image width="410" height="445" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZoAAAG9CAYAAAAsr7DtAAAGbklEQVR4nO3VMQHAIBDAwKfmMIU4nBUV2e4kZMna5/4DAJFPWABKRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AKaMBIGU0AKSMBoCU0QCQMhoAUkYDQMpoAEgZDQApowEgZTQApIwGgJTRAJAyGgBSRgNAymgASBkNACmjASBlNACkjAaAlNEAkDIaAFJGA0DKaABIGQ0AnZl59DgF/ASwVfoAAAAASUVORK5CYII="/>
					  <text id="OSTEOARTHRITIS" class="cls-1" transform="translate(205 206.023) scale(1 1.713)"><tspan x="0">OSTEOARTHRITIS</tspan></text>
					  <text id="Welcome_To" data-name="Welcome To" class="cls-2" x="121.75" y="128"><tspan x="121.75">Welcome To</tspan></text>
					  <text id="PATIENT_SUPPORT" data-name="PATIENT SUPPORT" class="cls-3" transform="translate(204.329 266.023) scale(1.022 1.713)"><tspan x="0">PATIENT SUPPORT</tspan></text>
					  <text id="PROGRAM" class="cls-4" transform="translate(204.667 357.653) scale(1.059 1.86)"><tspan x="0">PROGRAM</tspan></text>
					  <image x="161" y="485" width="180" height="47" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAAvCAYAAABKbOCyAAAgAElEQVR4nO2dCZQc1XX3f7X2Pj37jEYzo9FIGiEkJEBYgECAWWSwA9gQb4n3JZtxbH9x7Dgcn2z29yWOyQK2Yx8Hx8SO4y12vGFsQCAJLSySQEL7rhlp9q33ruqq+s57Vd3TM1qQQHZiy/egQ09X96tX9913373/+3+vFdd100Cc38g5iIuCByiAhqd4wVcVPPnSk1d+I790cXVFUc7KoD3P++V373+pKIoaGLMvSvVr+dL/23M9zpdlK8pLN3Q+x8h/jKl7/oqM/5Au3c1ZyJRCveDhLjwfVD3IJwYmOH58nImJAlbRlu+FQyaJmjCzZiXpaK9DUZXzYAhT3v/sPnveZtBJf55vm66e/EBldXsF4ulrn9xDoegADoqngzLVoHil4pGImzTPStLe0YBp6NK4LzSPXZ7QgwNjfO97u9iw+RDDw2myWRfbdeQ1XdGIREM0N8e4akULd999GbPbGuS1l6cvRQ56yXHYu7efw4dH0DUNtWxsnj9GLg4XL2lkTvssGQwJy3i5Zi1XGwUGBsZ5fkcvxaJHIhbjVVfMJpGInLdxVxQP23VxHUXamPhbU1UUVX1FE0d/4IGNjAwXcN0immJIBfntTXliM6IST5rMm9vMHb91EatWzUNV9QvGqMvGfPTIOPf93Tp+vmYPRtif2E7JrThGD4dUymZoeILnn+9j5/YJPnHvDXR1Nb4CJ6BQsks88pPtfOMbLxIKhTCNKXMtlTxcPD768ZXMaW8LRu8VeGrF/+7ePf38379Zw8i4w/w5jdx//+3SoM+P+Pc4sH+Ihx56BjtvM68ryVvedhX1DTWvyEvrRdfDcsVs8VA8h1zOQjgcOQAEbU+CNpJn3940Tz11iPe863Le+Y6VhCPGaQZJ8SdEYAxBROm/5/rXmLHcTH9dHhC38n4l5PG8yoQ7+XszxZPpmTIjBvW86ijrpQbe70s+X+Tb336OnzxyiNqmGkKGRShsMqejloY6U7Y/kcly+MgE2YxOKRzi8Sd76ZzzHB/+8KuJxUMvcZ8z9MDzKOSLjI/n0cMuhqGiairRiIHjuHhSpSJ6ZIYxVOvIC3SgVNosv38qsewS4yN5xsYdJuomcLziKfRSbqM6hyiPtnCI1WPPjImmMDqS5uEf72R80uOq5Z3c/nqH+oZpo3bOxq2bpkIopOJZBuFImJ6eeqIhk5KryEaFwsbH0owMZvDCJtmixZe+/Awts2p5/R1LfMN3VVDcSqBVjjVnRGH+gqYGdup6eGrwgMIreMFDVAzXCV7PMLjqdivGPVNEO1ZwR/Pkq7ITXvD9MylNTDxHBl69x8bZ9HQfWriEqtrMag3z3vdey5VXzsXQVXnLUqnElq1H+PKXtnLkeJpwrMRTm3p5/d3jLF7U+jIGyNePqmp0zGnh6pWd6CENM6yRnvTo709h2y6K6qLqJ7erCKMVz3oKPVYMzwu8sse0zzQ1x7nltV1k0hqtLTHCMWPqu5QDYFeOk4ce6LSq/SBc8ld6/M/Kx1cqn42GDWriYWzPI56IYpj+pFSD8ZGTzlOmhcEvJboiYmegmLNpn53kox+9jjltDViOjaYoiBV1dCzDmkf38P3/3oOrakxO2jzy8B6uWdlNU2OiEm3L8Q8eIpUqkJrIMjqSxXZKaJpGMhmjsTEuEyc/YQpiNlfBU22sgk0q7RCNGsRiYTlTM+kiY2MZxsYzcnlPJMPynrW1Uf/BT0okvKAfJq7rMDaWZnw0SyqdpeR6hEyDhoY4dXVR4vGIr+Mz6EsJjLD32ChDg2lpCKpncefrl3PbbRefNJA33bSY4RGLz92/AV1VGJ3I0nsiMOhzdzjyC7qpc8edl3LL6kWoqksopLFtSx+f+n9PUCg46KIPbnU/FN/Hqb6Bjo/lmJjIMDGewS45GCGDuroYDXUx4omwfMbyxPa9qkvPonbu/WQTiluUTiEajwZe3ZNtpNJFVEWjtjYix7yQtxgfzzIylsHKW0SjIeoaEtTXxzCFoXp+bCxzgpJLLlsknS2BbmBoJSBPKp0jFtcpFl3CpkI0JmJqU373bI1a9wJEVazCJhotjQnqGqajeK0ttczvbiFXVPj2t7YTMUz6jmYYGkj7Bh0YsRhsq+iyeeM+Hluzm70HxhkbLuE4RTTdpSYRY153Azet7mHVqgVEIyFc15VJjkhIX3j+KF/5ymbmLWzmrruu4OiRYZ588iD79kwwPjopDVQMQGd3jJtvWcRNr15CNKLLvlc7IDGQo6NpHn9sD0+sPUBfX5ZcxsJzXAzdoL4pzEWLk7z2dUu5/NK5aContVE2DNBlHycnCxSKNq7jEosnmDe/teLpPa9sqw6qorNgQYPsQyblYuRtxgZy0hjkAnau5uwJV6FIwxH/ylJbN1GFe5/0LanTdLbIurX7eOKxAxw+OkpqMkfJ1tD1MDW1Cgt7ktz6mku4auV8TFMNjNn36r1Hx/j6159mfLxIZ2cj737XCpoa4/KeA0NjPPD59Vh5jXe8bTma6rHmsRd5YftxhgaLFC0VM+oxqy3G1SvmcvtvLaG1ra68HEh06MEHN3CwNy1iBOIxkWzn+Pz96zGiGqmJAlcsm8Vb3/4qEjUKp1uHTyV6eTGSCY6jyBmPjAjcqSVPEWGJwdJLZvPwD/bLmZQrFBmbyAXDrgRLrsP3vvMsX3zwGfqHMpRkUxqG6rfnOXm2vzjCk08d5B3v7uc971hFNGIGRqEyMJJn7frDbN5ynOe2DnL06BgDxzNoio6pa1LZDnl27h1hw4bjHD08we+97xrMkB5gvv5yNjSc4/MPrOfb39mOJWJ2T8NARVNdHCfPkWNZtm4/wfr1R/jYR29h9eqLZdveaT2Bh+eIf/5zimzc0PUpixPfVgKvANQlQqxYPovB4TymCs11Idk/RVNfVsIjzawc8yoCx1ApFEqnNGY1MMhsNs+Xv/IUX3/oRTKpglgEZeiiyglq4/ZZvLBjkPVP9vLBD13DG+5eimGEJdolJvHocIZHfrCfw/1Zli+fxZvfeCk0+vfIph2efe4Eh/eP0d83RnqixP4Dg5TwMIyQuBEuNvsPTbBpwwA7dw7ysT+7ibZZdfL7o2N5fvCjPUxmLDrb6+T4TaRtHn50P7mCTTpVwLJs7nrzchIzcqmXNOgZmqO8ggqlzUy8PJEtBjGyULDrTrvMtm3HePCrzzA0YlGXqGVut87Fl8yiJlFDNmuxa+dx9u9PMzlu87WvbqGnexa33LxEQjbiXqGQTsvsBiYniuzcPihqcCy7pIV5CxpobU6guC67Dw6ye3dWept/f2gj8+bVcduty4J+K5ScEj/+yU6+8c29qCGDtsYIS+bX07OwUYYyQ6PjPLN1kMH+GH2943zpi0+x6OJWOtrrT+em5SSJxQx0Q0XXVTKZLC/u6Gf5ZR1ocqKVP+jHmXPntfDJv1iNbTuyvZq4iap6LxsVqvZQ5QxCVU83xv6Ftev28/WHtpLPmzS2RFkwr4GLLmonUaszMTHOjm39HDleZGA0yxe/sJmFPW0su7yj0ophaDQ01pBzFerrI2jaVEijaQoNjVFGhjIc2j9GLu/SOa+W+Re3MLujjpjuceLoOFt2DDGasvnxT4/Q3r6FD3341bLdRDzMlSs66D8xSTrlYHkQjoa4tCuGrmukJgssWNQiX5eBSeEYz0YqBi2gH/FXJG4Gg8g0jeVzRZ7fdpRMwcLFQNdUksmphMtxHDZuPsjQuEs4bHBRT5gPfeQGFl/SIb2riF8FlvqFL2xg3boBsimD9et7ufaaBTLeqoCFjkrJUmmbHef2Wxdy7XXdzJlbT23SRwkOHOrn85/fyNoniqRzsHbdXq6/biHRaFheHx3Ns3HTHizXormmhpte3cF737mCjjmNcrAtu8C6DXv43D9twXUSHO2dZNOmw3S8saFiMtO9qCsNunNuE40NEYZG0zixGv7rOzsIR12uX7WQeCzqG7bir3Ri0Orra6YbpRugAmdR8XtlopAv5Hlq4zGJkccSDssv7+CDH7iW7nnNctxKnsXWZw/zwBc2srNU4vhgjo1PH2HRklZMc8pwfKflBX2fPmfEtVJJIV4fZ/VrZ3Hr6+axYEEbDQ0JDA1GBtN87Vtb+fZ/7aRYKLJ+00HufP0lLFjQQkdHkk/82c28uP04n71vPamMzeIlMT78x9fQVF9L0SoRjxtERSLqBfnZWYpe7riqKRTcEkeOjGLnXYluKAFqMZlKs+bnh3j0Z73oIYOinaN7fh2zZycrdxFhyJHecRkLlyyHm19zMZdd1l2Bd3RVY/Gidm677SKefnaI1AT0DU4wmSoEBu1n2yJezaQLtLe38Oa3XUazTDqnpGd+O2+4YykbN/STLbqc6EszOpqpGPTQcIbBwQy64tKQiHL7a5fSMae58n3TCHPzDZfyzIZBjpzYg1pSOLh3IMgBTi6AlIGm7nlNvO62+fT1pkAzGBzJc/8Dm/npI/uZ3VZHJCK8mCe9eCwaprkhwpyuBjrn1DFrVo0sGJwelXnlUt3y6GiO3r4Ujieci8nrbltCT09r5bqumKxYsZCbdk+wf/9mMirsPzJCLl/CNMtoxkv11KNYtDETCnf+9jJWXN457WpjSy1vfdNlbFh3hPGRLCMjFidOjEuDDod15s5tJJcp4LgOJduvsl40v5Vk7QwWhutOQchnIRLs8RSFcMggNWLzT/+4kXBExS0F8YSjMjaaYaB/HFWP42kOszs03vr2K2hsrJGYrvBg+bxLJqNKJCIUtuie7xuRFyzjZcfU3FIjk5uxkbyMlUTpeFZrsmox8GQ8ahg6JTvw2lVGJh6upTVJXZPJyKhCPuORy9mV6+nJPJmUI7/T2mbS0lZzSi3M62kkZOrYBRgfzcgVRtP0kwfS85MSseq8811XY1sa//HNbRimiIvDbNkyxsbNJ/CLhaLapaAbOolwiLraEO1zklx1dSerVy+gq7O5Eq5VUv7zJVXdTqddUhOinuBRUxue5njKsbi4dUdnnERcZ2JMYXw0T8l2qjpzhr6JrnsBPauqvF9GQURoiKZRm4wyqy3Bjp3D2AWH1HixPMIydBIIlkx5FU/aTSZtkaydGm+ZO5yjjvQyHqvpCoV8ic1Pj1K0LT9b9PzGRMhQV28QS6pMThRIxJI0NyQDuMe/ebHoUMwVUdwS0bAoAZeXLm0ani6QjVhEFjspFlyKeatKgeUiiipXCMvPKk8KAlQNolETFJ2MBfnC1EAULZt8wZUDlkioxCv9cKuUqVFTY2KKZxZoQNGjUCwRi05PKSpDGyjVwcH2AsRCVOkoUvJsmSSauh+7ChTEKhQYzohCiE7fiUme23KCdev38XvvW8V1q+ZXVr7z5a1VMYn0qb6L4pgYS4G4hMMa5kkFPn9AwqafbHtoZLO2hOPORZQglLKK5XEKEDPFr2G4nkJU6F8VuY1weHblc37HZyp6Zi/PXUP6VMLnousubc1hHKI4ARCuKq6cuemsg+UUSNZE6evN88UH1vHxe1cze7afuYrKkl0s4HkOphHDVKsqV1NZkxz4kOrJZ3EcD8cpVXW/6gGU0/sIkSwYmilndt4uUbAKlWslqyRnu4D3hvoLfONrzxJLaBQLXqBwVwL6O/eNoKolVNOh4LkvOZj5vMVXv7qRf//aC4TiSYqlrIwVb7xuHvPnNxJPhGTYZlkWkxMpCWvu3TnM8EhG9nfLliE+3f8E2iddrlnVE2Tf58ukNQxjqvBhWQXskoXmqXIcREx/ym+pOprix/7i+Z1zNOiynOxEfUsV7xuaJusZjlsib+XOkWx17qITzBjhYTtmRXnTWy6lqamevO2gqD6eKZb+Y0fGWPOEwHQnJdC9dsNhXvXEXt72u1f65e1ysUkkUaccJyVQIhU81nUcSo494xNnId7UMieWN7cqaRETShQGhAfvH0jzb1/dEoA+WmXFcTUHVReDrckqZ8lCxu5nGqxDB4Z47OHD5DKgxyxaW2O8/51XserahSSSIbn0lsMJUTEUhYeD+4b53ndf4KnNfYRr4wwOZfjc59bR0dkkY+sp3K28OgmCmG8McnKXPVjl8ZSqavMMSsG02N/DFQmdF1TnKgjB9IFRVVWGSOIZhWOZci7nSeTiP1WRdL1frDFTwaFFDGN7RMJhrrtuAW1tjTMUpMgBX7a0hc/et47e3rRcuLduO8add14iCybSis+imiPL5J4gxTu+F1bOPoM9lSgzPbkXeHovuChiPEcU0kqyyikrVsK8XbAVB8cBrWSdmRHieRw7NsrwWB4jpAkkl7vfsIw77lwmK6AzRVTGRJLa2pJk8ZJWPnPfWh7+2X7iCZP9h9L84L938MEPXRcQlqjiWmiVhylXKD0cyhsJJL1ATk8dr0TAi1FQNREyntRrXNXDlTPyNDr2/Ikg/IGoup4N5/rlilKBgn+xopfVJ8RxBawVvDPt4TyJQ15xRReXXnaEI8d2oSsG4yM5srmib9BlpcsigHva+McJFChsXxiDdvJIvDLFBQBtJpXnssvbuPuulSRrY+SLxWCZL3NF/CKFa7syeYtFTk8eEoOeyRWxSjYlxyORiLJ48SzZf/GkPozucw6E2tKpPC/uOkEub1HfEGH1rZ3s3j9A79G8xLGf3nyMN7xxhPa2xsCQfV0JFEAUKET+EDJVLuppo6EpDhUu8pRObdkXf1VRAgy/ogMRRlQmhFu1+kzPRjwJyfmcD1XQUk8xOX/VZEZhxZaxNGV0omqFEAYuUIGGupBcphzHpFRQcEv+hwzDJBwKScMv2jaWfep4TJDhRRVIhAUCoNfPsxLNUAjDNBkbTxGP66xcOZemptpTxKsz2H1iEp6h8CF5IwE3Asdf0aiYSPm1j5kePDDEX/3NI/QPigQ6zF/+xXXcdH0P//rQNgzdY2SwwIsv9FcMuuy/tm45xmf+YS2ZvE1z3ODeP7+ZVTdePNXHquxaTC7X83UsJlYkPDUhTSMkK7sihLGKlsxtTvXsIu8puY6ci35ecn6dy9lJWXfqeYlEKmuADHVUpcJ4knFxQLguhwVWscjIiUlJOC8pHmbIwAwSjnBIlcmWaDFvq+Ry5dK5EyyVvqTSBVK5PJ6mEBFoSOxkNty5y5QmBF9YZPbCONMpm2y2PLGUGf+mh1Qy1juNPQtd1NYkJKYrWG0Cbtq7a8j3rpIaoEqkoaynbZLTkMcquOQzDk21NSxb0kw0JEI3hYxV4MChUb/tqvuGwgZ2USGXhpFJRZaDq/soCUhBjD0xlse2fIgzamrUJab0GItpxKUOFOycgl2syg+qcvSJtEu26LN5EjFDFod+mSJXcsWW9xcJ7CsNPykbtFdOKoKMV6xvAoFw5Y4CF9suMTKS5oc/epHNz53ACOlYTob2rhjxmkigRJ1EUpfJkWDN7dnVH+Cdup+QBbJv7wCTk3k0PUw8GSZZa1QUfT6kpjbk90NRGRxMc+LE2Gla9YdV7DrpH5rAFZOuXPb3fKJVmT8s/i/w5Mb6iExkUT1+9OPd/PiHO+ntHWdsLMvYeI4Txyf52c9f5Ac/2k0oFMbQNdo7ItQ3JWhqqiGZiMiYXXjF/v60DC2k1w9WhpaWOHUNJvFIBNez2Lazl3zB964yV1D9jHp4eJR16/pwRT7gImPzpia/sCQkWaP7bETVJD1Z5MjhyamnVv2kQxC1XtzeSzZTlMpvaAhjmL/oKuZ0EUUcVTPxvBL5bHFq1Svr/mXE9HrZmAVBZDKV49+/tpm6RELGZyK2E7fIF0ocOzbGCzsHKRR0ucOgKW5w880XB0udJ6mY3d1NoB6UmPYjD++ju6uRxctaiEVD5LMWW7Yc4kc/2kXJ8SmDXe311NZEz6uSWpqjtLfH2f78CBOpAt/93rNEEzpdnU2Ymk5ekqryDA6l2L2rn63PneCyK2ZLjFgmv56oJQWZedHBzdloCYOuha2sWj2fbdtGsBMa/aN5PvsPT9HVFaeuMSadQWqswMHDI+Qsv2CQyeRYufJSmpuT9LsT1NSEGR0RZCuVzKRNJlMgmYwKbpycXi1tCVpnxeg7NkI0ofD4YwcIGTrXXrtA8h8EtDY4MMGjP9/L01sGpEcXRaGLLm4hUTdVUa1vSNDV2YC+8TCFksv3f7iDlrYQPfPbCEVUJieLrH/8IGvXHpCJo/Dkixa2zsDhf1E1zSlJ1sRIRKOMTRQZHsvx2GP7WH3rQmJxQ5DwiIZDgTP0ONt4RPeXS5VwWCGbLfH97+/BD82mokMBi4lKmKNpWFaOWlPn9999LVdd1e0/uueD+9dfP4dHfrqHvQez9B7P8dm/X8u8nlqSNXHSkwX27OtjbMKlaOnMblV5zY1zMc1wVQyKjMH9vWVnfgABJ4p7qup0mKOuNs7NN/Swfs0AqbTLxo1DHD7yON1zG4iYETK5PEMjkwwNpxkbKTLUn6Kl2QxYZpo05sLBMawNB9Ge76fYl8NIhIi9YTFvuvtSjven+NE3d2KGTDKazuBIVrL4kEm1iyagQDyJR99ySze/85YlMmETtFVBVhqdyMsNASOjBVnMSMoinv/8jQ1JbrlxPls2D2AVDWnm3/nOTn7+6BFp0IJ4JbjNhbyNGQozNmzR1Rnh7ruW+PcNCj6maXLTTV089vMXGZi02bG3n0//9WMsXNBONK4xPJrhwJ5RUkWPyQmPi+bXcs01XejCiiq2o1R0PHMsJHgU6F49CWaq/qDvbf3PVjtcH0htbI7T051k1+5B8gn4z/98ns0bj2GEVJZd0cKb37ScmvjJ9OAzGvREOsf4hC2LJ6pqSCyyjB364gUPYEge9BXLannTby/lxhuXBhxat2L8C3tmcc8913D/A5s4eGhAYrbHjgnugyLphKoYopJOZ5vOH3zgapavmDuNI2xZDuMTOUbH0qRSRRn2VCy9ymGI9yfSBUbHciTrDElbrRi6qrJ69RKGBgv820ObGBuzmZhIsWfPCKh+YVQsbYrqyJCgsytJV3etNOaC45L+5gvoX96Our2fAkWctgReQaXwyF7qPrCcP//I9Vx26RzW/HCPDDFEAme5tmw3pKrEYwat7XVcvbKLO+9YyqzWqOy8YUJXVxjLrpGTtq3TRGKHMgzwvaTw3HfcvoSx0ZzcPziZtmUCLuLxoYFisOHDhzstJ8clS+q45wPXsmRJ+1T5OVDXq67q5o/++Cr+5V820T/ssH8izYEjuyUVVxF6VS1sz6Knu5kPf+Q65s1vCSaEP/YiT5pI5WVfRN5TjfULgv7EZE5yRppa8qctSokuZXKWDMcMzSOfL+cEngQfamp03vr2ZRztG+XgvklSmsvhvlHSkyWJtr3hDu+cT4zRb7u5Ry5/AoQXBodaXaSQlQu5e6SltZnu7jp6emppakyeRC73qQkaN964kPb2Oh5fs49d248zPGSTsYpoYUXGoJcsaOHmWxZw8ZLZMjuv1P8VhY72BG+4fT7jo0UuXjJL0i4r/ahYNtQkTG67eS6L5iVobI4Eu2aolJNjsRDvfs8KCdutffIgB/cPMDyRJV/0iJomjQ1hOjuT9FzUytwFjXTPbSaXtcndvw6+sAVnqIiztAnjA68isbJLjC7Fz20g+8+biM9t4m1vvYzbru9hcHCcyVRecnjFnWOmSTIZprklSUND1IckPb/IkayN8LE/XY1tOdJodFOlriFS5RD8am0sFuN977uW5Vd0s37dIQ7uG2Q8ZZEr2hIRSkY1mlsSLL28g1XXdjKns3kG7dXfcCD4JHf99grm97Sx5vFD7No7IEOtUgEihk59k8kly1q5+ZaLWLigtbKJt8yzaGqIc/vt3YylcnR01suJWpbaZITX3tzNogVJWpvraWtKVO5dNUwSorxqxWx0UaDTPC66qCmwE7UCGKxY0c0//GMdG9bu44WdffQN5hkcSNPaoqNpp4YbzyRKPl/s8zxmn+lDYskQZCG1auk5HcRVxq/FXrdCwZIl44Jly32L4ZCAl6L+lpwZbUiaoihB2z58JryYKNmeBPaLZN/x5PJd3u2iG4r0zNVZcvl7VrEkWWGiD/miTSSsEzZMDFOT0JbcZeN6ZP55PaXPbETJODiXN2J+5nVEV8wmrCgIv+IcHCX9jm/h1kep+4+3YNaEeSmZSao6lZzqLIrqvotdMpZtUyjYaLpKxDQlEhWJmJXxeKmxsKwS+XyWvOVIMlY4pMsNvqKaquvqKdrwaaNi8rnB7hcxFqqqBmPl0yEcqX812LSLj/N7U33yAnChXFLXDU2Oa7Uuyq9Fm8JWirZLsVhCVzzq60003cRztbPdgtWvC7jtzNnk1M4VvwB35obLijF0ZOyZSISr2iHw7Ce34cfCuuSTVLjRp9w8okiyvIj5fVhCC/o1/Rn85dOTYZEZipCgmqHjV9/E3g/xqvDYfuwvPYMqoMb2ONGP30D0ynZZRpeVRFF0aIoS6a4nszdNYbTgG7Q3c/e6d5otUed2LsdULKzKWBlmsovcoMr30u0g29EwzTjJmSR573T9UtE0G03eNuSz5as+JsZKJKRUdpOUGYTTDVX8k2UGszxpTrazCoVBbqIIE6sETb4H9851k6zHWRyJo/hcXr8adpYty8KMEpS5PbkFSCjKdyqnHXVZ2QpuerqG/Unh6qCW73Hqh/bKxQj5n+p/0wt4HX5VnMJAGvsLm1H7c5IIoN21gMir5/r7PgT/QPMLtgK+FJ5GiRqywFR5ilN42LNSjxhwN3ha5eTitH/F9fVXeZZy8biso7PDjeW887SyBVcM0Ye0T4H9VmyzbBszSSXqjDCwzDE5xRjID7pTnzt9L4PHcgL9qMHRDG7lrJCzEf2sdgN4wYw5h5niKYGyg++o5Vn3kk2cJbivVHmE0/arKgQp+3Cl8oZ8x358P+4z/VghnXBbnNCdS9HEci552aqcM44K1oRFsT+D2hhBrX1lxaAyYd0V2b/nVdR7ct+VCimznKwFFK9z32xb1lHFoZxh3KVu9SpPOfNup4bg588AAAj5SURBVPj7tPamnP3Em0ak4pwMuSxnWZr5xWOSvywJ6kbSRJyJPM73fJhS91zcG+egLW6a9sRu4KSsHUN4O0fxumopxvRzQEani9yMXCyR3dZL8diYz5o77emCyrT/e/5xJgH/4uUq7KU8ZbX86o37L57+9L9JFH9Bc4OlPvfiAKW9I8joJR5GWTkPNRaS0XV5MRFstVK+SOGnO/EmM5gr56KL9eYMY33GjMSD7LqDpN73bTLffAFLxMtqmQdyZhGfsEQlVlNwfn18zHmVC8ugA+6EOE5AGJC16QhOOo9acrDnJDEX+Vuk3Knj6qSIapu2oAb1T67GuGEeRpnMVG60vJpXlWynji6rvrlCKVug+N2dRLYOYOwexsvkqwOiU4vnty2I8pos4HjY2jnkMxeQ/E/Qq/7nxPPPrXDlgTge3u4RNMEKdD2MebUYXUnfoAUUGHRSdTwikTDK+6+RlVIzaqK5pUqOUEZ9BHwlvL59fJLS8QxGSwyjMxkgSF5A3wcrbeHuGkUJ1ckytBogLaqnBDn3yfFphRx2cIzMN7fjFooYb7scY2HjheeRXkIuLINmKi+1h7I4/Wk0cQyZpmB2JzFjoWmnkeATEAnpCl4iglHJRX1j9o3fk0di2QWbzPd3YD/4DMWdo5j3XE3dvTfKk/XKiID0+iUPpVDCCikoWQut4KAkqJwvV42glg/wkRNl6zGyf/JzUoNpzEwJfSCPd9+tUIYPfyP+eF1oaiij3Np4BrJFmVo7gpTTURdsT5ixkiuBF/bKhzsGuGlgQxoaxUyBkfvWUvyjH+A9fpjI1R1EXtNDaaYxC5pk3KC0uImwq6A+NUj2754kt29YwniqOgNCDWAZ6/A46Q8/grV3mOQXbify5mUoO4chV/pN1DFDLiiDVsqohTCwbA69YPlYc8hADfjEZ+PrygVi4U7z4kDIr2yCTz+OJMre9zrC/3oXWraIt+4wVuB2fbTMRU2GCN++CC8Mdm8a+/7nKLz/v8k+cUCUEYIdN4FXFvG75VD8ty3Y64+g/cGVxG6YjzKZhctaUGPGy9oZ/essF5RBl2sgSrmwEeCzuqDPnsOGZ5FYukEoUHj0AMp9T6OGw3h/eyuxj6zCOzBC+ve+Q3HzsYoxl5FfwYozdR2vK4b3savxLp+Fsm4/9l8/irVv0G8/oAKLAx6yu4cofmsr2vwk5juXk//ZLgq7+wi96RLUROgXvYn6V04uuJCjHFZ4iShOJOQTeQqeXL45C7uQZ2b7JwCTG83jfHE7Zl8e7rmGyHtWoBZKFB/cgpv30K/sYNrRNYqGl7PJPrkf7YpOaj51C5HfvwKlJoa39QQlgXpU9sEoEn1RHztEaV8K5a6lqOII2n/eiPH6SzBWdlYxPad241zo/vrCg+0U/0wQbVYSpS6OJkhRVolsypqqiZ3BKsrcA+E9rScP4605gLOqjei7LiVkqOS39eM+fBDtitkYy9t9g3P9aqD06DsGKG08hPH6RTJh1Bc14zZEcHQF1VSnMRnUfBFl61G0WAhnJEfqLx/GvKyN+HuvxDM0PEHQEr+wYruktxwj3zeJo1RXFC88ufCSQsWPgLXaMM7CelnhFYZR6pvEdlz/0Erl9PGHnBCSxO3irjmImi+gvXEJZncjXraI9eCzuH0T6AvqMeKmhOS8YE+KWyyRf2AdRncDxs3zpdGpWQuvYOE116DMCX5gKDDI0kgO69ikX7becZzwNXNI/NmNKOKQcdcnCdmKRu7Rw2TufRR7PBcUzC/cyPrCgzE9VVIVPcEYe/U8PFEZLCmoL/Rh9Y4jcANBi8StIswH/BppJgEm7D4/gPPEIUqdNYQvbZehRfaRPZQe24sjTkqtjVY8vtgFI7xz/utPU9rah/l/bpA0SnFNT+cxU0VpzNKog26KSqD9rd242wZRGiNEPnETsQ+9Gj0RRvNcNLFSiDYErfVv12Asm0V0Ucu0w10uRLkAQw5FekthgJK8v7IdLV9C2z5B/mcHJNSmqIYP702R9Xy2YGDMxRcHmPzzR3B3D8DiBrxLWinuH6H4L1vQbl2IvqgJxXJlWFIuq+Q3HCPzT5sIf/A6Yq+aXSFein2L4gZaZwI1okuPLtCN1Le2kf3SBlTLReuowbhytoz/Vdfzj1TQFHL9WTJ/+hPMuhA1H78eTfCSL/Ca+AVYaPJ3xwh6kVYTwrxnBdaCKMqJPO6Dz1N8ZJdEQDxNreybE6w4UWERJefRh55j7J7vy2N41fnNGJ5K8emjZD72MNrsWvQPX4cWNfDSBTlpxD97cy+ZP/w22i09RN63QpbOxWQRk6foqZQk3zqCFtKkQee/tRvr85swf3cxyvI23H0jFA4OyQniBt4+u+4Qw/d8V25Vivz9a1HrY3JnzKl+Z+lCkguvUhhIeadI6Mou7E+tJveJnxF/9jDFTxQYODxAzYpuzPqk/KUud7KILTbMfm8n9vZeQu9cTug9V1P4zDrsr2zFfq4P49p2Yp+8nmJbEqspCjt6cQ+MMvlCH+lPPYmxop36e29EMzScUglH1327ixmUxMmhuomja1jfeAHrU49jvGc5oT9cQSq6Hufew2gfW0fh7Snypofz/HGc7QNELm8h+sHr0Oc1oDk+ycm7sGvhnvjxenGARutZfPjXTkRi5Wi+5y08dZjMA0/BY/vlMbTm7HrcugiK4+Cmc1iug7t0NvHfuZTQLQtQQjql50+Q+8qzMkmLvfNyzHn12J6K87N95D/+sHTB+XqD0G091Lx/JaGmGGpwIpWj+tCcfXSC9O//EPvION7lDXjDaUJ3LyX5jlehRk2s/hS5+5/G/vEuXFNBbU7AxQ1Erl9AYlUXal0kOAnxwlxvZ8iIMOg14vxvsYr9WhGfz0LKJ6aWxPnFIhEbyeBu6KX46BFy+wZQChaxxlqU5S2Y13aiXzILT/zeiPhFgpL4nUWVUr7g78sLiSMASmLzkjz/Lr92P/ld/cSumENkeYfchiQmhxIcMywTTdU/B0RUCSc//QRqXYzkh1aiX90h2xS/dmbpijzTRD0+IcqGaNEIRnMMtcb0NzX/hsdRFg0Y/v+J9MqHFpBWmAAAAABJRU5ErkJggg=="/>
					  <image x="78" y="468" width="77" height="75" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABLCAYAAAA1UAqtAAAcEElEQVR4nNWcCXydZZ3vv+969pyTfU+zNG26t7RQCrIjFkTREUYcRAUddYRx++CC43VBnLl6cUVwVBRBGFlElIsgWEDWgkBblraU7mnSJs3WnP0973Y/z3OStKUF0p60cP+fT07S02f9vf/nvz+v4nnek8A8IAP4vI1IUSBnuQyM5tkxnGXVpmG27UwykHIYShfwPQXP81FUj5oyg+n1UU6YW82itioqYkG5Ed8/+JYUMfihk+iUE6BtA6a9ncBibFOWbfHomj7+sSlJ74jFnj1ZekZy7E7bKJ5GeTRAZcwgW3BI5x2svIuvQkuVznuXNfLBkzqIh82DAneYoAkqCNDWAbNK3eTUUXEz4tN2XYbTNqZhEI9oqPI7j10jeR5d3c8dT21n864MzZURAqaO5/vYjs9oxmIwaTGnJcbVl8xnXks5Rdz2glcCaJLT1gNdbzlU+2zC9y1830NVQ/jOEDf/+UX2pB0CoQDz2+pYNKeFUDiI7Xlc+6e1/OZvO4nHDcoDBo7noakqPj67hnL4isd/X3Ysx3dV4fkuCpoE7/9r0MYX73se+dQWPHczqllADSwiYNSSTD7GzLPvpm+7CzEkcPOmVXPp+5bw4fefSKwsyoPP9fC1W14kFI5Spvu4YwylayqDozkcx+P3X15GZ3N84qiWApo6RXs/dFIUuXDP8xjc/AjbnrmKoS0/xnWfRtVjGFod+L047mYCEYVIQ4LmpgqCAYPn1mzlM1+4mbM+8kNWrd3KWUuauOr86eweHMBVdcbhEEe5OhHEclWuvHkNyWy+FLAm6C3htPGF79nyJNtX/hLb2UZF23QS0+oJV05HD7wLTanBVzaSyg7xzAsBGmqbCIU0Bgf2sGp9D488t5E7//AkdSGTB+79JvM7m7jqV3/jztUe02qjOLZdVL+Aqihs2DXCt/5lFhefNqPU5R/e8Xzt03o9tX5Av2Jn7OQuBlb+im2rbkUtq6Jy5kzKaiJE65ow4u/EMOeiYOIrPgqvzxkrX9jIV79xK7lUlr//5Woy2VHO+4+/YJR3EjU8XN+T7XRNYWDUpiKmcNtXTiAeCR7Kdl9Lh348BWCu42Jt2EluzVbsVE5+92ZcL4FWFIZeeID+Wz/O0Jrb0CtaCFVVYJgWoerjCVZ8GDNwDIpSBAxfkQ/k9X6WLehkxR++xrTOJq75+X1UV1Zz7qIE/bt78PW9C3Jcn4qYQffuLE+uHSgFMEmHBJrYeG7zAMkfryB/+7M4D6wn/7snsV7tQ/LE6yAnvk+lU6z47bfY+YcvgpfDjdbj5fsoq22hbsHXKKu/CE1vlKaB5NzXmAgHI9HOMExuufbfiER1hoaTnHncDAoDW/F8db/eugqGZvDg6v7DwWk/mjxoikJ+2wC57z+A2pdCqYig1ZSh5l3yNz5J+p4X8ArOAcCJf/ft6uE7nzyFoUdvpq19HqN2hnxhhKZ3XE7rKT8kGJ+zF6xDdEpEn4Bp8PlPLCcaCbFwdjtdtQGGhpJomj7RzvV8EtEA67tHGUmmD2mO19KkQCseSY/MtY+gCgHbEC/KGrHRsIleFoR7XiJ5zQNY2wcnNOP4pjTdYPExJzJvWiXD/S9DopWuC35J0+JPoSgBfLySPDg5h6oTCBiYgSDzZ9Qy2LMF3TSlwSvI8yFoCtnm8Epv7rDnYrKgFTeuYpw6W4Kn9CfxHRfPdSU4fsiAaRUoWwfJfuteMvevl/3GgauuruWDV1xLw9mfh9lnM/PCG0g0LZmQVUJ2lUoTYwHtLbUkR8Ux9PbjfKFFC57P9t2lcZo+iTZy4nzvMKGT2jHrIuRvfhJNyNOQCUEDPA/f9VBbq1EGkhR+8QR7NvYR+ehSjHhYbkaMkVh6EQkumngQR4rK42G8bBLXdVBVDd91xswPRQLZN5IpaeZJyzSvd5TkZ25Ba64g/JXl+CETfzBZHMTQUQwNXwe/KoYxqwoe30Ly6/eSe7FHLtgfOyL+EQZMUDwawfccbCsjQRsn4VoZukahUBpnTw403yd4TAvqvCaSn7oFP+cR/vcz5LF0d4zg6xpGJIQSNlBNVf7WZtWiZyyyV91H5q41UraoSnGsI03VleUEdY18JouyL2i+j7BEnBLFweRkmmioa8SvPAfzvIWkL7sN59XdRL74LtRECH9jP47qQ1kYNRaQXEhARW2JozfFcW56hvRVf6HQNzpm05Uuw95sxcKuVVyXMX21l4RS80obfdLHUzwl0Th66YmErjiL1NfvIXv/y4Q+cxpKezX2xj4020ULBiFm4IcNCAQkqNq8GtgwTOqrfyL5zCa5iSMJ3J5kllzBwffcsUe+z1x+6XNPShFMzCcEOhA+qwulNkryyrvwBy0iH38H/u+fwVnTjdlWjS7knaLiaxa+puNZGmpnAmNXjsL3HiJ59i7CFy/DCOpSzkx1vHg4mcErFIqy398LmTCTPCHXSnxehwQa49aU7xNa0IT60w+RuuJulJEMkctPIaOoFJ7dhN5eC5EAqhbC1iwUzUbJKSiNEYyYgvvAK6TWDxL67EmEWquKR2gKZV0yY+E6ijQx9iNhcjgumuaUNP5hh4akJT69htjPLsB6Ygu5H6wgdOFxsKQNe9MwTqaAWJoRCqIIGRdS8YWSKAuhz6zCHEiS//L/JX3vi9K0ncrjOjicwVc9aVSLJ+JPsLIvLA7pGZRChwXauDAXwJlNFcR/+gFyq3tJX7OCyAeXos2uQ9k8CPkCnueiRoKokQBe2EAJmqi6jt9ajl4TwbrhaZI/fgB7OF0ct4TNjAO/cfsQvojVKXtD54yZHBoa9YnQ0Qdtv0UKp7kuTtlPz6ewajvZax8jfOky6WppW5OQtSFno+oGmlAOER2/LAABDa82jDmzDp7eQfrffk/68Y1ys6VwXc6y2NzTRzgSRjFM8PeqSvGnbnjUVh5lThMb6t2xjeu/9yXS6eRejquKkvjRBVgrN5L9/fMELz8d1wB3II3nOHh46LouhbOr2CgiVma5KE4OvyKGcsoM1IY47j7zHA5t7Rnk1Y07iZfFCJphvH1As12f8rIALdWRowea2IjjODzxx5/x8l3X8NTt38fJF4+VMF7N2jJi37+A3J2r8J7fivGZk/BH05AryAiIa7vS3tMDAbyAjhLU8TQNtbkMsy6G8uA60jc8RuaJjTjJ3GEBt61niMFUBiMUQRFRDmF2KAqaBqmcTUtlmJqjfTw3rn6I9It3c/5ZJ6KuvYPs4Db5vTKuHFoSRL9zLulr/obmq2hnzMXZPAIpGy+dkyqfSAitMopeEUJtKsNQVNyntuC8vBPlyW3kr32c1FfvIvPI+kkf1/E2q1/pwSrYxBIxFEPFHbNkFVTyhQLTm4OlSqXJ95ZOey5J6uFrOGVWC11xj2Pf91liDV37eUYCuND8ZgLnH0v6Jw9hnjULP27iDWVQLR81XYBMXoLsikSu8FmDKtTE8BvLoSWBNi2Bbqnkf/AIe/77EZl8mQxwmVyevz+7gZCpES2rRPG1iZCTiKeJIRZ2VB4mVHtpkqAVF5x68tc0q6Mkggp+dRuREz6JouoHjYWFPrBQyhNvVS/68jk4oykhzGSo3C/YuHkb1QNN11ENFcVQUFVQNRVMBaUqjDm9Ae9P60j97qlJ2b/du0Z4acNOYuEQwUgcb4zLhL2WyTvEoybzW44SaOIJZXvXklt9B364jP7cHiKnfBZdP3jKX+xQDZqEjuug8I+tmDPqZBTEzVoorozOoDhCCXiowmNQVRSBva6gCBA1HV/z8A0Po70G+66XsdbtfINwevH33595lcGRLNU1VZjhGI5TmMhfpHMOMxpiVMdL05xMBrRxs2L48evI6i4juUGCs06nrG3p6/bxKbpbSiyAO5JGDwVRy2M4loXqqsiCC3xc8aOCp4p5NBTheik+qqrgqQqu4qAEFAzdJP/oJhlaOjhwxUKYP6x4HlPzCCfKUcyAjPMxdjRtx+XEWZWYunaQ/odGk+K0kY0P09/zGJYZwjKD1C39hOz4Zq6PO5JBiYVQRGQ3Yogkqzzp0kb3fWn4+qk85B38rOAKT1ryvqrI0gJFnFdfQYuasH2IQs563blWrd3G8y9vp6YyTqisHhxvLOwOlu0T0n2WdJQXH2qJLtsbgiZNCcem98kbcPQQ6fwQsTnnEanumtTEiqqgJ/MQ0PGDASnDZBBSAFdw8dJ5lCWtBM49Bn9WvVQoijfOp8XFCW4UHIauyzKDg8wiP39/33Nk0g6RWIxYeRWuaxf/24e0ZTO3tYKZzYnDAum19KactvuVvzDavwYtVoFR2UDzsn+d9ODakmnkX+nBs13MxS3Yu5P4VgHVFoBZhM9diC6y4as2y7CuPxY6R/ElgEKQ66qKl8qidiTQA8YBCkFw0o6+Qe5+aA3l8RDBeEIeTc/zJ/4/nXU5eX6VrCyairjA64Mmucxi9/rbCTW1oAd8GhddSDBcNTn2FjbbjDrU49pJffFOgsumE7n8NJzqCM6OEYzl8zC7GrCf3oy7rg9tywBqviDS4ahuscZDJHNIWwh/InDyzIlxX0u33beK7r5RwkGNRHWzlGGi6khW4Nke5WU+Zy2sGV/YkQNNTDjS+xS+N0ykoopwVTXV08+e9MAy9Of7xC87HS8YYORLf4ThLO7uNMrx7QSXtst2xulzIBHAs2x8rWiMioSIVCS7LQo7hgl8YhmBjpoDHpYQHwPDSW66eyVloQCRRIJQvBrPKUhwdV1lYI/FaXNraKgum7Lw00FBk3lOzyO162ES5VUYIYX49NMwI0W5M1kSTbWwSeI/z8M8tZ3CizsILWsnetHSoksGmHVxQhcchxc2cfpG0fKudPDtgRROxCT4teVEz11wAIeNa9E7//osG7qHiEV0ErVNqJqGZztSA1u2J6vR/nnZ1BZ6HhCEVChmjqzkZjSnH72yDkV3KW84fazFoWfA1YBGePlc1PcslMpBkMgXpB9eR/x9i9EbyolediaFLbtxdydRcy6Bmijm3AZUVX3dWUWm/Me3/J1ENEg4FKK8shHHLshjImThzhGLY2eVMWdMa445e4eK0QF08Mit71NIrcYUpQfBAGakCyN8eE9LGUvf5V7uxXlsM/r8JtR0Dmd1N9qGATJJi+D7hUJIEOisBfEztjV7Qx9EgxgN8YOOfcPtj7N1xx5aGmJEqxrRwzGy2Yw0VyyhbBybC09qQde0fQKRpdMBoAlzwHcz+NomzKoa6UwH40tl2eXhyASRhRd2V3hOI6m/baBw/aMYlx6Pvnwu1HRDtkDu9mfQ6ishGpAVkVqmgN09hPPwFoKfOxWzMbHf3OJBbN85yPX/8wRVlVEU1aCqqZ2CbUuXSfDTcNpiYUeEU+bVM/EUpogOkGkyB213Y4aDhEKNaKF56EbnYc0muSxrk7z9WfkwYle8E78rjl4XJXR8O355CD8aQBPhoXU78R56Beev68g/vAHWDktRoVXFDlwg8MNfr6B3OEVQ9ymrqsSIluO5xdi/wDebd/jQqdNkctj3S8zZvYYOqggcbw+61oWhNGAYs4uZpcPUPGrAwHt0I9l7VqPqKsGz5pO97yUprDWh0dKFossU01HLw2gVEZTqKF7Gwp9RjT6rdr/xBJCPPr+B3/5pJfVVZdL9qmqZiWtbkkt1VWEwmWdhZxlnLmyY6DWVtB9oxSishaKGUQPTcYOz0IzG0qYzNdSaOIV71uHmCgSWdKAXFArbh1DqEqgiLahpaNL3lCEJ1EwBWhMYFy+m0L9HgqGM18flC/yvn9yDp5lomktFXQPBSBWuXXTORZFy0ipw2fJOTN04IiUQ+4Em3RiRFzTaMfQOdK0eFKOEUoJiP72+EnXHKPnV29EiJlpjHPpTqJVBbBHZEPP6Kp7CWGYKVOEp3LYGbcRCxozGxrrulhWsfG4zDdVRudaq1pkymiGOoKmr7BjM8e4FNZw4t/ZN1nb4dMDx9BQDXzHw3CS4w8U4TgmQyYNRFpT1Hs7z3fJ7pa0Cv38UIiYic+vZNrYAzlVQbBtfFNQ8sR0lpGIsaByr1VVZs247377+ARqa6yjkMlQ3TicQLsctWFIujmYcYqbPZe/tLKbujlDdyD6gKdLn85xBKOyQpeg+ebnYw6bxKqEYqOGA5C7xCITcsvdkUH0VXTPQbA/d84v+onDq8wUcXYTKu4pBSaENRzN87pu3oRompupjBEJUTptJwcpLzhQmRf9Ijg+f3kZHY/kRrUyaQKQoKm0cZzsFZwe2sxMIlCxCRX+tPIoSNHBFnkDU8hsaXiqHrEQR8S3Xk0dTRDiEaaMVVHzHITCtamKc7/z8fp54uZu66hiFXI6ajhkogZC0xTRVY3BPga7WMJcun17iit+c9tppY/YZhe0o5FAQVY6HXLVwAMmKo4q49CsVy5Eukmaa6M5YbYimyKpKf8wXETE3u3sP2j8vwGwthqbvun8VP/ndI0xrrcGx8sQqKqis75TgCbtMXO3J23muPH++zA8c6fq3/RWBN4Rnb8fJ7sDN5vEpPTQstV5ALYZ7xoKPitCoejHt54oUm+3L8LfvgP3qAP4Z7UQvPlb2ff7lbXzmGzfTUFuOqQmAHOo7F8gYowhiappK9+4Ml57RztKZtUel/m0/0OxcisxgH+n+PnJ7RNSztPzgRDmAqeIZCopb5CdPABZQi7LI8fBtH8/2sV/pw1vSQtnlp6IrKj19I3z8y7/BUhXKYmFy6VFq22YQStTi5DMyKLlzOM/slij/ek7RAD8aF1bVfbeXTe8m3beb1K6duAVDRgpKXYTob+gGumGMlT35sqhfEZa6aCAMq7yNt7YfZWEjiS+chm7oFAoFPvX1m3i5Z5j6+mpy6TTReDn1rXOxcxnJYRnLlRmv/7hwNtHQ6yR5jgDtx2nu6DCpnTsZ7enDt8bDxSWUqo/9Vk1dFvgVbwkoeyMXwk/M+zjr+vCObyT2hTPRQgHZ6rKrbuWBlRuZ3lqHnbfk8W6atRgXVRbrCS+lfyTDF97TyjEdVW+4jqmm4urHzpGdzpHq3012eFiWvk8FSV9WHMeQh6J78m/xIGTMK+dgv9SLd+oMyj5/Jrqw24Arf3AnN9z5D9rb6mTVuEhS17XPJJQQAGYxdJWe3Wnetaiei8/sLOa/jhKXMaE9x6xQYdSajnBpfLRS7LPXkDjmgrkcU5X+p2852MMWAaEEPrKY2Du6ZJRV0HeuvZvv/eIh2tsbinfYM1mqqmupaZtNPpuRgPUO5WlrCPLND8+TXHs0AWOvyVFELRSpIerriKJOI29P3SzK2Ieiyoy6Lcz/cEDWc1Q07814f/9X93H1zx+kta1egljIFwgEDZrnLMUuOOg4DCUdAobK/77kGMpFau8oAyZI98fS9oKCkQqaowoBkb3NdY+lwtWS5VrxBp7McshjqTeXE//YCWghc6Ld1df9mW9fdz/NzfWYAQ3bsvFci9ZF78APxCCfIZX3Kdgu1122kNlN8aPOYeOkiwjCSCpHKBggHZrGl55ZxshQhqYGjauPT1FRHi9Jg8oAs+Ph9Y/it1bhhwMykkq4CFjBdrjiP+/g+lsfo6WtFsNQcRyXQj5Ny+z5RMqbsPMZsrbLSMriB5+Yx/FdtW8ZYIJ0Uej24qZeshmLd548n15tAQ+tX09sp8LHeoY4rvzgoeZDIREv0z92ArFjWmXiY/zaT9/AKJd/+xb+/PBaWqc3yMsZwv200ilqpnVQ3TKPQj6DVXDZPZzlqovmcNbipqOJz0FJNQ0NUzdZsXK9vM3xifNmEgxmsNU8f3v8JdmnlHJOmViJBYmc2oVWFpyIi61au4X3fvJ67nn8Vdo76otSAAUrm6SstobGWUsp5LOS63oGc1xxXhfnn9y238Wxt4pUIc+mNSS4//E1bNi2iw+cvZQFcxrJp/PcdM8/2D04WvrSBGeNXyH0fH5z52Oce8n1bOgZoKOluvgWF3FUMxkisSitC0+Qd5tE1eX2XRk+fXYrl55z5B3xyZLU8zUVcfYkfW656wkMQ+f/XPF+yoMGGzcN8Y3r7pVDlcJt41exRTLkki/9mk996zaUSJCGunIccf1RpAxzOelsty86CUMxyWdybOvP8pF3tvCFfyq+a+Wt5rBxkqAJ9b64s54bf/0Qm3cMctKxs/nZNy4kaqr84lcPcs1v/yqbT+au+r607z2om/74FMsv+TF3rniR1rY6olETx/VkZVDBsqQz3n7sqeiBKKPJDN2DGS47t42vfWie7P92AYx93agF85vpHc5yyVd+SyqT41/efwJ/+MWnWTinkS9f/ks+esUN9A+NvkGN2F7aF6xHn32Fd3/yZ3z6G7cylHFpmVYjj6onnHdFJZ/LYmgw87jTCZZVyGvUIph45YVdfO59sybKst5OpPi+L18xce8jq7nwKzdhWx4nLWrlxv/6KM31lVh2get+t4Lb735aVkif/+7juOC8ZTTXVO6fi2RvSaKQRc+v6+a6Wx/m7hUvyhB6fU0ZmqrsreYRCd1MRr6cZMaxp2DEKtiyY0AmWq6+eA7nLG2W7Y4UYCW/NkdRlK7+oSSnXHQNozmHbDZPdSLA1y97DxeecxxBs+g4vPBKL93du5g3t4XWhpoDRusdGGHlqo3ccd/zPPD0JjzHo6Y6jmlq2M5YRkl8qgqFdJpgOCABU4LlUgm11Yb47kcXcMxYMfGR5LApAU0EBL/43Tu5/vbHmdHRQP/wKKnRLMcvaufsd8xm2fwWlsxrJRwKSjs/axXIZS1e3bKLl7YMsnpdN0+s2sDm7iF5U6SiLEIsEpBct3fvxVqKXGaURHUFHYtOJm0F2Lqjj+VLarnqwwuoioeOynGckhc0iUGeXbuNMz/yA+KJciJhA1tY4ekcyVSOspBBTXUZwaAhj5l4j09BvBBuMEk6a8kYWFk0JI+b0MACnPGjWFykiuvYFPIpGtu6qJyxmJ7+JCEsPn1uJx85sx21hKT0odKUgSboU9/8HTfesZKOzkbJJUVroXgbN2fZOHaRc8R3QusKEEVW2/fHU2YHecGbqlHI5/DdAp3zFuMn2unrG+LkeZVc8YEZdDQUyzqPpsCfsleBiYGGRtKc/qH/ons4S319Fba9927kOIDjtPcFJK+7MnxRWZ1PEg7HqOtazLCfoEq3+Pz7Ozn3+JaJu1VHm6b0/WlisBfWd7P8kh+RLfg0NFdJ88DzJh+ULJbRIysSbTtLpK6VQM1sTEPnrAVx/v3cTmrKo7LtW2VOTDFo8pPnXtzKx7/8S17aOkpjcyWxaBDPcWU96xstRAQcRV1FJpPD0UIkps2hobmdE2dE+OCJ1czvKGrdt9r2OiJv6hOD9vQP890f3cvtDz5P3vaIJqJEIwGpCFRlvL6iWMIuGDGfz5NK5yhgMK1jBouPXcAZx9RxYmcZM5uK0ZK3i6F6hF5vWLTYBa1eu50b73iMx1/YImPz2ZyD4+9VCKKVMOWqa6uYPaeL05fN54xjp9FRG0Lci50Y8W1k2ZcK2hu8fXR/XzOXz7Nh22427Rikt39YVmKLXEIiGqGtpYLOjkbqx2TVvvR2c4OYAtC2Ay1TOcnbEaTXUgmgOcIKfRoYEgi+Ucti8E/+deACxsux5HE+0m93eYsJrP8Hc9tx/4MuBPsAAAAASUVORK5CYII="/>
					</svg>



                 </div>
                <!--<div class="typography">
                    <p> Welcome To</p>
                    <h1>Glaucoma </h1>
                    <h4>Patient Support</h4>
                    <h2>Program </h2>
                    <div class="typography_logo">
                        <span class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="267.579" height="64.027" viewBox="0 0 267.579 64.027">
                                <g id="Group_2060" data-name="Group 2060" transform="translate(0 -7)">
                                    <g id="Group_2016" data-name="Group 2016" transform="translate(0 7)">
                                    <path id="Path_33" data-name="Path 33" d="M5.352,0A5.352,5.352,0,1,1,0,5.352,5.352,5.352,0,0,1,5.352,0Z" transform="translate(73.141 39.88)" fill="#2fa6a5"/>
                                    <path id="Path_27" data-name="Path 27" d="M434.344,312.33a16.787,16.787,0,1,1-13.44-16.47,6.474,6.474,0,1,0,4.872,1.819A16.8,16.8,0,0,1,434.344,312.33Z" transform="translate(-375.172 -278.535)"/>
                                    <path id="Path_34" data-name="Path 34" d="M5.352,0A5.352,5.352,0,1,1,0,5.352,5.352,5.352,0,0,1,5.352,0Z" transform="translate(0 13.527)" fill="#cc58c3"/>
                                    <path id="Path_28" data-name="Path 28" d="M341.681,177.338a47.436,47.436,0,0,0-5.887-.615c-.488-.026-.977-.027-1.463-.045-.487,0-.971-.01-1.457.006-.97.013-1.933.065-2.891.144a37.659,37.659,0,0,0-5.625.9c-.456.109-.905.248-1.357.363-.446.14-.893.272-1.333.422l-.653.237c-.217.081-.437.153-.648.248l-.639.267-.32.13-.313.146-.623.292c-.1.05-.209.095-.31.148l-.3.163c-.4.219-.812.419-1.194.667s-.786.458-1.156.724-.756.5-1.111.783-.73.535-1.065.839l-1.1.937-.316.267c-.072.064-.141.118-.217.189l-.224.2-.223.218c-.15.144-.3.3-.446.455l-.433.482a21.641,21.641,0,0,0-1.586,2.109l-.359.561c-.122.187-.227.385-.343.575-.233.38-.434.782-.643,1.179a27.969,27.969,0,0,0-1.984,5.046c-.071.216-.124.438-.182.66l-.171.666c-.126.44-.2.894-.3,1.344l-.143.676c-.043.226-.075.456-.111.684l-.11.684c-.036.23-.078.456-.1.688l-.159,1.387c-.032.23-.043.464-.058.7l-.048.7c-.089.93-.074,1.873-.111,2.812-.013.235,0,.472,0,.708l.013.708.01.709,0,.356.021.355.08,1.422c.015.237.023.475.041.712l.07.712.133,1.427.033.357.047.355.094.713.182,1.429c.026.239.071.476.111.714l.12.713q.229,1.413.5,2.82a52.516,52.516,0,0,1-5.163-5.253c-.2-.9-.343-1.818-.494-2.732l-.124-.759c-.037-.255-.063-.511-.094-.767l-.178-1.535-.043-.384-.027-.386-.053-.774-.05-.775c-.014-.258-.04-.516-.039-.776l-.023-1.559c-.017-.519.007-1.042.026-1.564l.029-.782c.011-.262.018-.522.046-.784l.139-1.567c.018-.263.063-.522.1-.784l.113-.782c.038-.26.072-.522.116-.782l.151-.78c.107-.519.2-1.04.338-1.554l.2-.773c.066-.257.125-.516.211-.769.162-.51.31-1.021.485-1.527s.363-1.008.562-1.507c.216-.493.417-.99.647-1.478l.356-.726.18-.363c.06-.121.129-.238.193-.356a25.243,25.243,0,0,1,3.8-5.32l.588-.6c.206-.193.405-.389.621-.577l.325-.282.341-.278c.11-.091.24-.185.36-.277l.262-.2,1.218-.915c.424-.332.883-.613,1.33-.911s.917-.56,1.382-.829.943-.507,1.423-.742.974-.442,1.463-.653l.368-.155c.122-.052.25-.093.374-.14l.75-.272.375-.135.38-.116.76-.228c.252-.079.51-.137.766-.2l.769-.186c.513-.114,1.03-.208,1.545-.308.259-.04.518-.078.777-.114s.516-.08.776-.1a33.609,33.609,0,0,1,6.192-.112,36.014,36.014,0,0,1,6.021.98c.981.243,1.948.532,2.9.858A24.436,24.436,0,0,1,341.681,177.338Z" transform="translate(-287.815 -174.266)" fill="#cc58c3"/>
                                    <path id="Path_29" data-name="Path 29" d="M300.438,351.43c-.077.6-.156,1.2-.248,1.808-.121.6-.234,1.2-.363,1.8-.153.591-.311,1.183-.479,1.774-.2.58-.387,1.165-.591,1.747-.231.568-.455,1.143-.7,1.711-.27.554-.533,1.113-.81,1.666-.3.539-.6,1.084-.909,1.62-.331.524-.661,1.049-1.01,1.563-.368.5-.727,1.008-1.105,1.5-.4.478-.784.966-1.194,1.436-.21.229-.421.459-.635.686s-.421.462-.648.677l-.678.646-.34.323c-.112.109-.225.216-.346.315a31.264,31.264,0,0,1-13.218,6.98l-.925.208-.462.1-.231.052-.234.038-.935.152-.469.075c-.156.024-.312.052-.47.064-.629.061-1.258.129-1.889.175l-1.894.063-1.9-.057c-.629-.047-1.257-.112-1.885-.17l-.236-.023-.233-.037-.467-.074-.934-.149c-.155-.026-.312-.048-.464-.078l-.456-.092c-.3-.06-.606-.124-.908-.2q-1.807-.42-3.566-.987c-1.166-.386-2.318-.81-3.445-1.283s-2.231-.984-3.314-1.533-2.139-1.139-3.178-1.751c-.516-.311-1.032-.62-1.542-.948l-.1-.063-.285-.181-.221-.139-.211-.146c-.271-.188-.522-.372-.77-.563-.987-.759-1.911-1.552-2.806-2.376l-.363-.34a47.568,47.568,0,0,1-8.572-10.923,44.9,44.9,0,0,1-3.007-6.459,38.54,38.54,0,0,1-1.86-6.8,63.025,63.025,0,0,0,3.209,6.156,56.769,56.769,0,0,0,3.85,5.645q1.722,2.2,3.642,4.222a52.536,52.536,0,0,0,5.432,4.974c.133.107.266.211.4.315.873.68,1.77,1.33,2.675,1.915.225.144.452.289.666.419l.153.09.153.083.367.2c.484.27.981.527,1.476.782a58.162,58.162,0,0,0,6.075,2.687c1.032.378,2.07.732,3.117,1.04s2.1.573,3.161.793l.4.083.4.075.4.077c.133.025.263.038.395.058l.788.113.394.058.2.029.2.017c.528.043,1.055.1,1.583.135l1.585.043a28.583,28.583,0,0,0,12.277-2.8,29.9,29.9,0,0,0,5.432-3.362c.109-.077.211-.162.314-.25l.308-.259.617-.516c.4-.354.782-.735,1.183-1.095.384-.375.75-.77,1.128-1.154.361-.4.707-.815,1.064-1.22a29.557,29.557,0,0,0,5.887-11.618,30.954,30.954,0,0,0,.923-6.664l.02-.427c0-.141,0-.283,0-.426l-.009-.856c0-.574-.024-1.147-.066-1.721a25.62,25.62,0,0,0-.455-3.441,14.811,14.811,0,0,1,.7,1.617c.207.552.386,1.114.553,1.682s.306,1.144.421,1.725q.092.436.179.874c.027.146.061.293.086.44l.06.442A28.2,28.2,0,0,1,300.438,351.43Z" transform="translate(-227.451 -314.583)" fill="#2fa6a5"/>
                                    </g>
                                    <path id="Path_2481" data-name="Path 2481" d="M-75.39-21.66a7.834,7.834,0,0,1,3.255.585A11.163,11.163,0,0,1-69.87-19.71q.6.45.915.1a3.163,3.163,0,0,0,.435-1.635h.69q-.06,1.11-.09,2.7t-.03,4.2h-.69q-.21-1.29-.39-2.055a6.635,6.635,0,0,0-.435-1.305,5.807,5.807,0,0,0-.675-1.08,5.335,5.335,0,0,0-2.34-1.785,8.036,8.036,0,0,0-2.97-.555,4.935,4.935,0,0,0-2.655.735,6.376,6.376,0,0,0-2.04,2.115,11.244,11.244,0,0,0-1.32,3.33,18.926,18.926,0,0,0-.465,4.38,17.551,17.551,0,0,0,.51,4.455A10.266,10.266,0,0,0-80-2.85a6.276,6.276,0,0,0,2.16,2A5.546,5.546,0,0,0-75.15-.18a8.169,8.169,0,0,0,2.82-.54,4.726,4.726,0,0,0,2.31-1.74,5.182,5.182,0,0,0,.915-1.89A23.422,23.422,0,0,0-68.64-7.2h.69q0,2.73.03,4.4T-67.83,0h-.69a3.352,3.352,0,0,0-.4-1.62q-.285-.33-.945.09A15.7,15.7,0,0,1-72.225-.165,7.538,7.538,0,0,1-75.42.42,9.5,9.5,0,0,1-80.415-.87a8.627,8.627,0,0,1-3.33-3.72,13.239,13.239,0,0,1-1.185-5.85,13.257,13.257,0,0,1,1.23-5.88,9.639,9.639,0,0,1,3.375-3.93A8.779,8.779,0,0,1-75.39-21.66ZM-61.29.21a3.659,3.659,0,0,1-2.835-.975,3.713,3.713,0,0,1-.885-2.6A3.393,3.393,0,0,1-64.4-5.445,4.812,4.812,0,0,1-62.82-6.8,13.684,13.684,0,0,1-60.8-7.7q1.065-.375,2.025-.735A6.588,6.588,0,0,0-57.2-9.24a1.361,1.361,0,0,0,.615-1.14v-1.86a3.955,3.955,0,0,0-.375-1.92,1.871,1.871,0,0,0-1-.885,4.171,4.171,0,0,0-1.41-.225,6.745,6.745,0,0,0-1.6.21,2.389,2.389,0,0,0-1.365.87,1.785,1.785,0,0,1,.99.6,1.8,1.8,0,0,1,.42,1.23,1.519,1.519,0,0,1-.48,1.185,1.761,1.761,0,0,1-1.23.435,1.472,1.472,0,0,1-1.275-.555,2.061,2.061,0,0,1-.4-1.245,1.936,1.936,0,0,1,.39-1.26,4.835,4.835,0,0,1,.99-.9,6.607,6.607,0,0,1,1.785-.825,8.224,8.224,0,0,1,2.475-.345,6.791,6.791,0,0,1,2.1.285,3.758,3.758,0,0,1,1.44.825,3.374,3.374,0,0,1,1.02,1.755,11.092,11.092,0,0,1,.24,2.475v8.34a2.1,2.1,0,0,0,.21,1.065.775.775,0,0,0,.72.345,1.456,1.456,0,0,0,.735-.18,3.571,3.571,0,0,0,.615-.45l.33.51a9.911,9.911,0,0,1-1.2.8A3.168,3.168,0,0,1-54,.21,2.27,2.27,0,0,1-56.07-.6a3.417,3.417,0,0,1-.51-1.86A5.067,5.067,0,0,1-58.755-.33,6.176,6.176,0,0,1-61.29.21Zm1.32-1.2a3.39,3.39,0,0,0,1.77-.525A5.066,5.066,0,0,0-56.58-3.27V-9.15A3.02,3.02,0,0,1-57.975-8.1q-.945.39-1.95.855a5.3,5.3,0,0,0-1.725,1.26,3.259,3.259,0,0,0-.72,2.265,2.758,2.758,0,0,0,.69,2.055A2.349,2.349,0,0,0-59.97-.99Zm18.63-14.88a2.968,2.968,0,0,1,1.425.33,2.58,2.58,0,0,1,.96.855A2.11,2.11,0,0,1-38.61-13.5a2.027,2.027,0,0,1-.465,1.32,1.524,1.524,0,0,1-1.245.57,1.67,1.67,0,0,1-1.11-.405,1.427,1.427,0,0,1-.48-1.155,1.54,1.54,0,0,1,.315-.975,2.668,2.668,0,0,1,.705-.645.718.718,0,0,0-.63-.3,2.973,2.973,0,0,0-2.19.945,6.734,6.734,0,0,0-1.455,2.265,6.624,6.624,0,0,0-.525,2.43v6.36a2.076,2.076,0,0,0,.855,1.965A4.642,4.642,0,0,0-42.51-.63V0q-.69-.03-1.95-.075T-47.13-.12q-1.02,0-2.025.045T-50.67,0V-.63a2.7,2.7,0,0,0,1.755-.45,2.2,2.2,0,0,0,.525-1.71v-9.39a3.35,3.35,0,0,0-.48-2,2.123,2.123,0,0,0-1.8-.645v-.63q.96.09,1.86.09a12.716,12.716,0,0,0,1.665-.1,8.452,8.452,0,0,0,1.455-.315v3.75a8.167,8.167,0,0,1,.945-1.755A5.823,5.823,0,0,1-43.26-15.27,3.308,3.308,0,0,1-41.34-15.87Zm10.41,0a5.341,5.341,0,0,1,4.125,1.605Q-25.32-12.66-25.32-9.27H-35.91l-.03-.57h7.74a9.851,9.851,0,0,0-.24-2.715,4.7,4.7,0,0,0-.945-1.995,2.1,2.1,0,0,0-1.665-.75A3.061,3.061,0,0,0-33.5-13.92,8.165,8.165,0,0,0-34.77-9.54l.09.12q-.06.45-.09.99t-.03,1.08a8.522,8.522,0,0,0,.66,3.51,5.383,5.383,0,0,0,1.725,2.235A3.733,3.733,0,0,0-30.21-.84a4.882,4.882,0,0,0,2.49-.66A5.026,5.026,0,0,0-25.8-3.84l.6.24a6.177,6.177,0,0,1-1.08,1.92A5.78,5.78,0,0,1-28.17-.165,5.7,5.7,0,0,1-30.81.42a6.721,6.721,0,0,1-3.7-.99,6.423,6.423,0,0,1-2.37-2.745A9.382,9.382,0,0,1-37.71-7.35a11.21,11.21,0,0,1,.84-4.545,6.711,6.711,0,0,1,2.37-2.94A6.218,6.218,0,0,1-30.93-15.87ZM3.06-21.24v.6a3.723,3.723,0,0,0-1.545.3,1.377,1.377,0,0,0-.705.87,6.263,6.263,0,0,0-.18,1.71V.15H-.06l-12.6-19.47V-3.48a5.83,5.83,0,0,0,.195,1.695,1.353,1.353,0,0,0,.78.87A5.3,5.3,0,0,0-9.93-.6V0q-.54-.06-1.41-.075T-12.99-.09q-.75,0-1.515.015T-15.78,0V-.6a4.239,4.239,0,0,0,1.545-.315,1.328,1.328,0,0,0,.7-.87,6.291,6.291,0,0,0,.18-1.695V-18.06a5.438,5.438,0,0,0-.18-1.635,1.123,1.123,0,0,0-.7-.72,5.11,5.11,0,0,0-1.545-.225v-.6q.51.03,1.275.06t1.515.03q.66,0,1.26-.03t1.08-.06L-.06-4.92V-17.76a5.8,5.8,0,0,0-.195-1.71,1.4,1.4,0,0,0-.78-.87,4.666,4.666,0,0,0-1.755-.3v-.6q.54.03,1.425.06t1.635.03q.78,0,1.545-.03T3.06-21.24Zm15.63-.33a5.861,5.861,0,0,1,2.28.345,8.15,8.15,0,0,1,1.38.765q.36.21.585.315a1.026,1.026,0,0,0,.435.1q.48,0,.66-1.32h.69q-.03.51-.075,1.215t-.06,1.86q-.015,1.155-.015,3.045h-.69A8.668,8.668,0,0,0,23.25-18a5.317,5.317,0,0,0-1.575-2.16A4.175,4.175,0,0,0,18.96-21a3.709,3.709,0,0,0-2.625.96A3.276,3.276,0,0,0,15.3-17.52a3.654,3.654,0,0,0,.69,2.265,6.938,6.938,0,0,0,1.845,1.65q1.155.735,2.505,1.575a30.938,30.938,0,0,1,2.775,1.905,7.972,7.972,0,0,1,1.92,2.085,5.147,5.147,0,0,1,.7,2.76,5.47,5.47,0,0,1-.87,3.18A5.355,5.355,0,0,1,22.56-.21a7.849,7.849,0,0,1-3.18.63A7.161,7.161,0,0,1,16.86.06a10.829,10.829,0,0,1-1.59-.75,2.169,2.169,0,0,0-1.02-.42q-.48,0-.66,1.32H12.9q.06-.63.075-1.485T13-3.51q.015-1.38.015-3.48h.69a12.449,12.449,0,0,0,.675,3.36,5.562,5.562,0,0,0,1.71,2.505A4.707,4.707,0,0,0,19.17-.18a4.272,4.272,0,0,0,2.685-.945A3.619,3.619,0,0,0,23.07-4.17,3.961,3.961,0,0,0,21.78-7.2a20.45,20.45,0,0,0-3.39-2.46q-1.41-.87-2.64-1.77a7.451,7.451,0,0,1-1.965-2.085,5.331,5.331,0,0,1-.735-2.865,5.045,5.045,0,0,1,.8-2.925,4.771,4.771,0,0,1,2.085-1.71A6.909,6.909,0,0,1,18.69-21.57Zm13.8-1.92v11.22a5.168,5.168,0,0,1,2.175-2.82,5.909,5.909,0,0,1,2.925-.78,5.251,5.251,0,0,1,1.9.3,3.461,3.461,0,0,1,1.275.84,3.407,3.407,0,0,1,.81,1.5,9.8,9.8,0,0,1,.24,2.43v8.01a2.2,2.2,0,0,0,.525,1.71A2.7,2.7,0,0,0,44.1-.63V0q-.51-.03-1.56-.075T40.5-.12q-.99,0-1.935.045T37.14,0V-.63a2.119,2.119,0,0,0,1.53-.45,2.436,2.436,0,0,0,.45-1.71v-8.67a9.071,9.071,0,0,0-.15-1.71,2.221,2.221,0,0,0-.675-1.26,2.315,2.315,0,0,0-1.605-.48A3.669,3.669,0,0,0,33.675-13.4,6.168,6.168,0,0,0,32.49-9.48v6.69a2.436,2.436,0,0,0,.45,1.71,2.119,2.119,0,0,0,1.53.45V0q-.48-.03-1.425-.075T31.11-.12q-.99,0-2.04.045T27.51,0V-.63a2.7,2.7,0,0,0,1.755-.45,2.2,2.2,0,0,0,.525-1.71v-17.1a3.35,3.35,0,0,0-.48-2,2.123,2.123,0,0,0-1.8-.645v-.63q.96.09,1.86.09a12.715,12.715,0,0,0,1.665-.1A8.452,8.452,0,0,0,32.49-23.49ZM49.14.21a3.659,3.659,0,0,1-2.835-.975,3.713,3.713,0,0,1-.885-2.6,3.393,3.393,0,0,1,.615-2.085A4.812,4.812,0,0,1,47.61-6.8a13.684,13.684,0,0,1,2.025-.9Q50.7-8.07,51.66-8.43a6.588,6.588,0,0,0,1.575-.81,1.361,1.361,0,0,0,.615-1.14v-1.86a3.955,3.955,0,0,0-.375-1.92,1.871,1.871,0,0,0-1-.885,4.171,4.171,0,0,0-1.41-.225,6.745,6.745,0,0,0-1.6.21,2.389,2.389,0,0,0-1.365.87,1.785,1.785,0,0,1,.99.6,1.8,1.8,0,0,1,.42,1.23,1.519,1.519,0,0,1-.48,1.185,1.761,1.761,0,0,1-1.23.435,1.472,1.472,0,0,1-1.275-.555,2.061,2.061,0,0,1-.4-1.245,1.936,1.936,0,0,1,.39-1.26,4.835,4.835,0,0,1,.99-.9,6.607,6.607,0,0,1,1.785-.825,8.224,8.224,0,0,1,2.475-.345,6.791,6.791,0,0,1,2.1.285,3.758,3.758,0,0,1,1.44.825,3.374,3.374,0,0,1,1.02,1.755,11.092,11.092,0,0,1,.24,2.475v8.34a2.1,2.1,0,0,0,.21,1.065.775.775,0,0,0,.72.345,1.456,1.456,0,0,0,.735-.18,3.571,3.571,0,0,0,.615-.45l.33.51a9.911,9.911,0,0,1-1.2.8A3.168,3.168,0,0,1,56.43.21,2.27,2.27,0,0,1,54.36-.6a3.417,3.417,0,0,1-.51-1.86A5.067,5.067,0,0,1,51.675-.33,6.176,6.176,0,0,1,49.14.21Zm1.32-1.2a3.39,3.39,0,0,0,1.77-.525A5.066,5.066,0,0,0,53.85-3.27V-9.15A3.02,3.02,0,0,1,52.455-8.1q-.945.39-1.95.855a5.3,5.3,0,0,0-1.725,1.26,3.259,3.259,0,0,0-.72,2.265,2.758,2.758,0,0,0,.69,2.055A2.349,2.349,0,0,0,50.46-.99ZM69.09-15.87a2.968,2.968,0,0,1,1.425.33,2.58,2.58,0,0,1,.96.855A2.11,2.11,0,0,1,71.82-13.5a2.027,2.027,0,0,1-.465,1.32,1.524,1.524,0,0,1-1.245.57A1.67,1.67,0,0,1,69-12.015a1.427,1.427,0,0,1-.48-1.155,1.54,1.54,0,0,1,.315-.975,2.668,2.668,0,0,1,.705-.645.718.718,0,0,0-.63-.3,2.973,2.973,0,0,0-2.19.945,6.734,6.734,0,0,0-1.455,2.265,6.624,6.624,0,0,0-.525,2.43v6.36A2.076,2.076,0,0,0,65.6-1.125,4.642,4.642,0,0,0,67.92-.63V0q-.69-.03-1.95-.075T63.3-.12q-1.02,0-2.025.045T59.76,0V-.63a2.7,2.7,0,0,0,1.755-.45,2.2,2.2,0,0,0,.525-1.71v-9.39a3.35,3.35,0,0,0-.48-2,2.123,2.123,0,0,0-1.8-.645v-.63q.96.09,1.86.09a12.716,12.716,0,0,0,1.665-.1,8.452,8.452,0,0,0,1.455-.315v3.75a8.167,8.167,0,0,1,.945-1.755A5.823,5.823,0,0,1,67.17-15.27,3.308,3.308,0,0,1,69.09-15.87Zm10.41,0a5.341,5.341,0,0,1,4.125,1.605Q85.11-12.66,85.11-9.27H74.52l-.03-.57h7.74a9.851,9.851,0,0,0-.24-2.715,4.7,4.7,0,0,0-.945-1.995,2.1,2.1,0,0,0-1.665-.75,3.061,3.061,0,0,0-2.445,1.38A8.165,8.165,0,0,0,75.66-9.54l.09.12q-.06.45-.09.99t-.03,1.08a8.522,8.522,0,0,0,.66,3.51,5.383,5.383,0,0,0,1.725,2.235,3.733,3.733,0,0,0,2.2.765,4.882,4.882,0,0,0,2.49-.66,5.026,5.026,0,0,0,1.92-2.34l.6.24a6.177,6.177,0,0,1-1.08,1.92A5.78,5.78,0,0,1,82.26-.165,5.7,5.7,0,0,1,79.62.42a6.721,6.721,0,0,1-3.705-.99,6.423,6.423,0,0,1-2.37-2.745A9.382,9.382,0,0,1,72.72-7.35a11.21,11.21,0,0,1,.84-4.545,6.711,6.711,0,0,1,2.37-2.94A6.218,6.218,0,0,1,79.5-15.87Z" transform="translate(182.349 51)"/>
                                </g>
                            </svg>
                         </span>
                    </div>
                </div>-->
            </div>
        </section>
         <div class="footer">
            <div class="inner" >
                <p class="join_now m-0"><b><span href="#" onClick="window.location.href='#';" data-toggle="modal" data-target="#JoinNow">Join Now</span></b> for Free Patient Support & to Know More About Osteoarthritis</p>
            </div>
            <div class="row footer_managed_by m-0">
                <div class="col-lg">
                    <p class="managed_by m-0">Managed by <a style="color: #fff;" href="https://www.techizerindia.com/">Techizer Tech Solutions Private Limited </a>as the Service Provider</p>
                </div>
                <div class="col-lg">
                    <p class="managed_by m-0">Designed & Developed by <a style="color: #fff;" href="https://www.webapprise.com/"> Webapprise System </a> </p>
                </div>
            </div>
        </div>			
    </main>




 
    <script src="../frontend/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="../frontend/js/popper.min.js" type="text/javascript"></script>
    <script src="../frontend/js/bootstrap.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
$('input[type="checkbox"]').click(function(){
	if($(this).prop("checked") == true){
		$("#checkboxid").val("checked.");
		$("#check_error_msg").text("");
	}
	else if($(this).prop("checked") == false){
		$("#checkboxid").val("unchecked.");
		$("#check_error_msg").text("Terms & Conditions are required");
	}
});
$(document).on("click", "#subBtn", function(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	var name = $("#name").val();
	var age = $("#age").val();
	var gender = $("#gender").val();
	var address = $("#address").val();
	var city = $("#city").val();
	var state = $("#state").val();
	var mobile = $("#mobile").val();
	var pincode = $("#pincode").val();
	var doctorname = $("#doctorname").val();
	var doctorcity = $("#doctorcity").val();
	var language = $("#langugae").val();
	var checkboxid = $("#checkboxid").val();
	var file1 = document.getElementById('inputGroupFile01').files.length;
	var file2 = document.getElementById('inputGroupFile02').files.length;
	// var file3 = document.getElementById('inputGroupFile03').files.length;
	
	if(name == ""){
	  $("#name_error_msg").text("Name is required");
	}
	if(age == ""){
	  $("#age_error_msg").text("Age is required");
	}
	if(gender == ""){
	  $("#gen_error_msg").text("Gender is required");
	}
	if(address == ""){
	  $("#address_error_msg").text("Address is required");
	}if(state == ""){
	  $("#state_error_msg").text("State is required");
	}if(city == ""){
	  $("#city_error_msg").text("City is required");
	}if(mobile == ""){
	  $("#mobile_error_msg").text("Mobile is required");
	}if(pincode == ""){
	  $("#pin_error_msg").text("Pincode is required");
	}if(doctorname == ""){
	  $("#dname_error_msg").text("Doctor name is required");
	}if(language == ""){
	  $("#language_error_msg").text("Language is required");
	}
	/*if(file1 == "0"){
	  $("#file1_error_msg").text("Prescription is required");
	}
	if(file2 == "0" && file3 == "0"){
	   $("#file2_error_msg").text("Invoice/Bill or Image of bottle purchased is required");
	   //$("#file3_error_msg").text("Image of bottle purchased is required");
	}*/
	if((checkboxid == "") || (checkboxid == "unchecked")){
	   $("#check_error_msg").text("Please indicate that you have read and agree to the Terms and Conditions");
	   //$("#file3_error_msg").text("Image of bottle purchased is required");
	}
	var len = mobile.length;
	var res = mobile.charAt(0);
	if(len != 10){
			$("#mobile_error_msg").text("Mobile no length should be 10 digits only.");	
			return false;
	}
	if(res == 0){
			$("#mobile_error_msg").text("Mobile is not start of 0 digit.");	
			return false;
	}
	if(name != "" && age != "" && gender != "" && address != "" && city != "" && state != "" && mobile != "" && pincode != "" && doctorname != "" && language != "" && checkboxid == "checked."){
		$("#quickform").submit();  
	}
	
	
});	
$(document).on("change", "#name", function(){
	var name = $(this).val();
	if(name != ""){
		$("#name_error_msg").text("");
	}else{
		$("#name_error_msg").text("Name is required");
	}
});
$(document).on("change", "#age", function(){
	var age = $(this).val();
	if(age != ""){
		if(age > 120){
			$("#age_error_msg").text("Please enter value below 120");
			$("#age").val("");
			return false;
		}else{
			$("#age_error_msg").text("");
		}
	}else{
		$("#age_error_msg").text("Age is required");
	}
});	
$(document).on("change", "#gender", function(){
	var gender = $(this).val();
	if(gender != ""){
		$("#gen_error_msg").text("");
	}else{
		$("#gen_error_msg").text("Gender is required");
	}
});
$(document).on("change", "#address", function(){
	var address = $(this).val();
	if(address != ""){
		$("#address_error_msg").text("");
	}else{
		$("#address_error_msg").text("Address is required");
	}
});
$(document).on("change", "#city", function(){
	var city = $(this).val();
	if(city != ""){
		$("#city_error_msg").text("");
	}else{
		$("#city_error_msg").text("City is required");
	}
});
$(document).on("change", "#state", function(){
	var state = $(this).val();
	if(state != ""){
		$("#state_error_msg").text("");
	}else{
		$("#state_error_msg").text("State is required");
	}
});
$(document).on("change", "#mobile", function(){
	var mobile = $(this).val();
	
	var res = mobile.charAt(0);
	if(mobile != ""){
		var len = mobile.length;
		if(len != 10){
			$("#mobile_error_msg").text("Mobile no length should be 10 digits only.");	
			return false;
			if(res == 0){
				$("#mobile_error_msg").text("Mobile is not start of 0 digit.");	
				return false;
			}else{
				$("#mobile_error_msg").text("");
			}
		}else{
			if(res == 0){
				$("#mobile_error_msg").text("Mobile is not start of 0 digit.");	
				return false;
			}else{
				$("#mobile_error_msg").text("");
			}
		}
	}else{
		$("#mobile_error_msg").text("Mobile is required");
	}
});
$(document).on("change", "#pincode", function(){
	var pincode = $(this).val();
	if(pincode != ""){
		var plen = pincode.length;
		if(plen != 6){
		$("#pin_error_msg").text("Pincode length should be 6 digits only.");	
		return false;
		}else{
			$("#pin_error_msg").text("");
		}
	}else{
		$("#pin_error_msg").text("Pincode is required");
	}
});
$(document).on("change", "#doctorname", function(){
	var doctorname = $(this).val();
	if(doctorname != ""){
		$("#dname_error_msg").text("");
	}else{
		$("#dname_error_msg").text("Doctor name is required");
	}
});
$(document).on("change", "#langugae", function(){
	var language = $(this).val();
	if(language != ""){
		$("#language_error_msg").text("");
	}else{
		$("#language_error_msg").text("Language is required");
	}
});

/*$(document).on("change", "#inputGroupFile01", function(){
	var file1 = document.getElementById('inputGroupFile01').files.length;
	if(file1 != "0"){
		$("#file1_error_msg").text("");
	}else{
		$("#file1_error_msg").text("Prescription is required");
	}
});
$(document).on("change", "#inputGroupFile02", function(){
	var file2 = document.getElementById('inputGroupFile02').files.length;
	if(file2 != "0"){
		$("#file2_error_msg").text("");
	}else{
		$("#file2_error_msg").text("Invoice/Bill is required");
	}
});
$(document).on("change", "#inputGroupFile03", function(){
	var file3 = document.getElementById('inputGroupFile03').files.length;
	if(file3 != "0"){
		$("#file3_error_msg").text("");
	}else{
		$("#file3_error_msg").text("Image of bottle purchased is required");
	}
});*/
$('#age').bind("cut copy paste",function(e) {
          e.preventDefault();
      });
$("#mobile").keydown(function (e) {
	
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
		         return;
        }
        if ((e.shiftKey || (e.keyCode == 32 ) || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			
            e.preventDefault();
        }
    });
$("#pincode").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode == 32 ) || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });	
$("#age").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
$("#inputGroupFile01").on("change", function() {
		var a = (this.files[0].size);
		if(a > 5000000) {
            $("#file1_error_msg").text("You can upload maximum 5 MB size file");
			return false;
        }else{
			groupprescriptionURL(this);
		}
});	
$("#inputGroupFile02").on("change", function() {
		var a = (this.files[0].size);
		if(a > 5000000) {
            $("#file2_error_msg").text("You can upload maximum 5 MB size file");
			return false;
        }else{
			groupinvoiceURL(this);
		}
	    
});
// $("#inputGroupFile03").on("change", function() {
// 		var a = (this.files[0].size);
// 		if(a > 5000000) {
//             $("#file3_error_msg").text("You can upload maximum 5 MB size file");
// 			return false;
//         }else{
// 			groupbottleURL(this);
// 		}
	    
// });
$(".nickname").keypress(function(e){

        if ((e.which > 96 && e.which < 123 ) || (e.which > 64 && e.which < 91 ) || (e.which == 8) || (e.which == 82) || (e.which == 32) || (e.which == 0) || (e.which == 46) ) {            
        
          }
        else{

               return false;
         }
    });	
});
function groupprescriptionURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
               var files = $("#inputGroupFile01").get(0).files;
			   var ext = files[0]['name'].split('.').pop().toLowerCase().toUpperCase();
               var validExtension = ['gif','png','jpg','jpeg','GIF','PNG','JPG','JPEG','pdf','PDF'];
              if($.inArray(ext, validExtension) == -1) {
            
				}else{
					if(ext == "pdf" || ext == "PDF"){
						$("#prescriptionpreview").css("display", "block");
						$('#prescriptionpreview').attr('src', '../images/pdf-icon.png').height(40);
						$('#prescriptionname').text(files[0]['name']);
						$("#firstdiv").css("display", "block");
						
					}else{
						$("#prescriptionpreview").css("display", "block");
						$('#prescriptionpreview').attr('src', e.target.result).height(40);
					    $('#prescriptionname').text(files[0]['name']);
					    $("#firstdiv").css("display", "block");
					   
					}	
               }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
function groupinvoiceURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
               var files = $("#inputGroupFile02").get(0).files;
		       var ext = files[0]['name'].split('.').pop().toLowerCase().toUpperCase();
			   var validExtension = ['gif','png','jpg','jpeg','GIF','PNG','JPG','JPEG','pdf','PDF'];
			   
              if($.inArray(ext, validExtension) == -1) {
					
				}else{
					if(ext == "pdf" || ext == "PDF"){
						$("#invoicepreview").css("display", "block");
						$('#invoicepreview').attr('src', '../images/pdf-icon.png').height(40);
						$('#invoicename').text(files[0]['name']);
						$("#firstdiv").css("display", "block");
						
					}else{
						$("#invoicepreview").css("display", "block");
						$('#invoicepreview').attr('src', e.target.result).height(40);
						$('#invoicename').text(files[0]['name']);
						$("#firstdiv").css("display", "block");
						
					}
               }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
// function groupbottleURL(input) {
//         if (input.files && input.files[0]) {
//             var reader = new FileReader();
//             reader.onload = function (e) {
//                var files = $("#inputGroupFile03").get(0).files;
// 		       var ext = files[0]['name'].split('.').pop().toLowerCase().toUpperCase();
// 			   var validExtension = ['gif','png','jpg','jpeg','GIF','PNG','JPG','JPEG','pdf','PDF'];
			   
//               if($.inArray(ext, validExtension) == -1) {
					
// 				}else{
// 					if(ext == "pdf" || ext == "PDF"){
// 						$("#bottlepreview").css("display", "block");
// 						$('#bottlepreview').attr('src', '../images/pdf-icon.png').height(40);
// 						$('#bottlename').text(files[0]['name']);
// 						$("#seconddiv").css("display", "block");
// 					}else{
// 						$("#bottlepreview").css("display", "block");
// 						$('#bottlepreview').attr('src', e.target.result).height(40);
// 						$('#bottlename').text(files[0]['name']);
// 						$("#seconddiv").css("display", "block");
// 					}
//                }
//             }
//             reader.readAsDataURL(input.files[0]);
//         }
//     }
</script>
	<script type="text/javascript"> // RELOADS WEBPAGE WHEN MOBILE ORIENTATION CHANGES
		$(window).bind('orientationchange', function (event) {
			location.reload(true);
		});
		</script>
</body>

</html>
