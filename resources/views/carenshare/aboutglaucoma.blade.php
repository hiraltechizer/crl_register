<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=G-YT7TRLL0Q6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-YT7TRLL0Q6');
</script>
    <meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../frontend/images/ft-logo.png" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../frontend/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../frontend/css/style.css" media="all" type="text/css" />
    <title>Bone & Joint</title>
</head>
@php
$lan = isset($_SESSION['lan'])?$_SESSION['lan']:"";
@endphp
<body class="body_index body_activities about_glaucoma">
    <!----------------header start------------------->

    <header class="header_activities ">
        <div class="container">
            <nav class="navbar navbar-expand-lg  navbar-dark p-0 d-flex justify-content-between">
                <a class="" href="{{ url('carenshare\activites') }}"><span class="back_arrow">Back</span></a>
                <a class="navbar-brand m-0" href="/"><img src="../frontend/images/logo_white.svg" alt="logo" class="logo img-fluid"></a>
                <div>
                    <div class="navbar-collapse collapse show" id="navbarsExample03">
                        <div class="navbar-nav ml-auto"></div>
                    </div>
                </div>
            </nav>
        </div>
    </header>
		<div class="modal fade p-0" id="AboutGlaucomaVideo" tabindex="-1" role="dialog" aria-labelledby="AboutGlaucomaVideo" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="SignupModalCenterTitle">Watch Video To Know About Glaucoma</h5>
                    <div class="language-picker js-language-picker">
                        <form action="" class="language-picker__form">
                            <label for="language-picker-select"><svg id="language-24px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path id="Path_2472" data-name="Path 2472" d="M0,0H24V24H0Z" fill="none"/> <path id="Path_2473" data-name="Path 2473" d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z" fill="#fff"/> </svg></label>
                            <select name="language-picker-select" class="getvideos" id="language-picker-select">
                            @foreach($video as $row)
								@if(isset($_SESSION['video']))
										<option  value="{{$row->language}}" @if($row->language == $_SESSION['video'][0]->language) selected @endif >{{$row->language}}</option>
								@else
										@if(isset($lan))
											<option  value="{{$row->language}}" @if($row->language == $lan) selected @endif>{{$row->language}}</option>
										@else
											<option  value="{{$row->language}}">{{$row->language}}</option>
										@endif
								@endif
							@endforeach
                          </select>
                        </form>
                    </div>
                </div>
                <div class="modal-body" id="bindvideo">
				@if(isset($_SESSION['video']))
                    <iframe width="670" height="390" src="{{$_SESSION['video'][0]->url}}" data-toggle="modal" data-target="#JoinNow"></iframe>
					@elseif($lan == "English")
						<iframe width="670" height="390" src="https://www.youtube.com/embed/L1c9zdzfx1A" data-toggle="modal" data-target="#JoinNow"></iframe>
					@elseif($lan == "Hindi")  
						<iframe width="670" height="390" src="https://www.youtube.com/embed/tn68HvqT9FM" data-toggle="modal" data-target="#JoinNow"></iframe>
					@elseif($lan == "Bengali")  
						<iframe width="670" height="390" src="https://www.youtube.com/embed/z64fTPChYn8" data-toggle="modal" data-target="#JoinNow"></iframe>
					@elseif($lan == "Gujarati")  
						<iframe width="670" height="390" src="https://www.youtube.com/embed/yjBAJxhDKPw" data-toggle="modal" data-target="#JoinNow"></iframe>
					@elseif($lan == "Kannada")  
						<iframe width="670" height="390" src="https://www.youtube.com/embed/jxt4y-rTwZg" data-toggle="modal" data-target="#JoinNow"></iframe>
					@elseif($lan == "Malayalam")  
						<iframe width="670" height="390" src="https://www.youtube.com/embed/lh1EYR4lGlk" data-toggle="modal" data-target="#JoinNow"></iframe>
					@elseif($lan == "Marathi")  
						<iframe width="670" height="390" src="https://www.youtube.com/embed/ZnwC7bV422c" data-toggle="modal" data-target="#JoinNow"></iframe>
					@elseif($lan == "Tamil")  
						<iframe width="670" height="390" src="https://www.youtube.com/embed/AcZD50OL3eg" data-toggle="modal" data-target="#JoinNow"></iframe>
					@elseif($lan == "Telugu")  
						<iframe width="670" height="390" src="https://www.youtube.com/embed/0-bxUHuLf-M" data-toggle="modal" data-target="#JoinNow"></iframe>
					@else
						<iframe width="670" height="390" src="https://www.youtube.com/embed/L1c9zdzfx1A" data-toggle="modal" data-target="#JoinNow"></iframe>
					@endif
				</div>
            </div>
        </div>
        <button type="button" class="btn modal_btn_close" id="videoclose" data-dismiss="modal">
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" width="16.99" height="16.953" viewBox="0 0 16.99 16.953"> <path id="Path_2-8" data-name="Path 2-8" d="M5622.466-2293.481a1.543,1.543,0,0,0,1.077.447,1.533,1.533,0,0,0,1.078-.447l5.895-5.9,5.894,5.893a1.546,1.546,0,0,0,1.077.447,1.525,1.525,0,0,0,1.076-.447,1.5,1.5,0,0,0,.014-2.123l-.014-.013-5.893-5.893,5.893-5.893a1.512,1.512,0,0,0,0-2.138,1.511,1.511,0,0,0-2.136,0l-5.893,5.892-5.892-5.893a1.51,1.51,0,0,0-2.137,0,1.511,1.511,0,0,0,0,2.136l5.89,5.889-5.893,5.894a1.469,1.469,0,0,0-.1,2.077C5622.422-2293.525,5622.444-2293.5,5622.466-2293.481Z" transform="translate(-5622.019 2309.987)" fill="#056371" style="isolation: isolate"/> </svg>
        </span>
    </button>
    </div>
	<div class="modal fade p-0" id="AboutGlaucomaVideo1" tabindex="-1" role="dialog" aria-labelledby="AboutGlaucomaVideo" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="SignupModalCenterTitle">What is Glaucoma</h5>
                    <div class="language-picker js-language-picker">
                        <form action="" class="language-picker__form">
                            <label for="language-picker-select"><svg id="language-24px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path id="Path_2472" data-name="Path 2472" d="M0,0H24V24H0Z" fill="none"/> <path id="Path_2473" data-name="Path 2473" d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z" fill="#fff"/> </svg></label>
                            <select name="language-picker-select" class="videoss" id="language-picker-select">
                            @foreach($video1 as $vrow)
								@if(isset($_SESSION['video1']))
										<option  value="{{$vrow->language}}" @if($vrow->language == $_SESSION['video1'][0]->language) selected @endif >{{$vrow->language}}</option>
								@else
											@if(isset($lan))
												<option  value="{{$vrow->language}}" @if($vrow->language == $lan) selected @endif>{{$vrow->language}}</option>
											@else
												<option  value="{{$vrow->language}}">{{$vrow->language}}</option>
											@endif
								@endif
							@endforeach
                          </select>
                        </form>
                    </div>
                </div>
                <div class="modal-body" id="videosBind">
					@if(isset($_SESSION['video1']))
						<iframe width="670" height="390" src="{{$_SESSION['video1'][0]->url}}" data-toggle="modal" data-target="#JoinNow"></iframe>
						@elseif($lan == "English")
							<iframe width="670" height="390" src="https://www.youtube.com/embed/4xWpiVulN-Q" data-toggle="modal" data-target="#JoinNow"></iframe>
						@elseif($lan == "Hindi")  
							<iframe width="670" height="390" src="https://www.youtube.com/embed/ORbDUYgBAzk" data-toggle="modal" data-target="#JoinNow"></iframe>
						@elseif($lan == "Bengali")  
							<iframe width="670" height="390" src="https://www.youtube.com/embed/swrHLe3Z-6o" data-toggle="modal" data-target="#JoinNow"></iframe>
						@elseif($lan == "Gujarati")  
							<iframe width="670" height="390" src="https://www.youtube.com/embed/jvIflRXQ4NQ" data-toggle="modal" data-target="#JoinNow"></iframe>
						@elseif($lan == "Kannada")  
							<iframe width="670" height="390" src="https://www.youtube.com/embed/adScCEkYWmU" data-toggle="modal" data-target="#JoinNow"></iframe>
						@elseif($lan == "Malayalam")  
							<iframe width="670" height="390" src="https://www.youtube.com/embed/_RB5sN4Z7hk" data-toggle="modal" data-target="#JoinNow"></iframe>
						@elseif($lan == "Marathi")  
							<iframe width="670" height="390" src="https://www.youtube.com/embed/jG3sKwLgFiA" data-toggle="modal" data-target="#JoinNow"></iframe>
						@elseif($lan == "Tamil")  
							<iframe width="670" height="390" src="https://www.youtube.com/embed/OiRnQRhUFpg" data-toggle="modal" data-target="#JoinNow"></iframe>
						@elseif($lan == "Telugu")  
							<iframe width="670" height="390" src="https://www.youtube.com/embed/2nzSzMR216Q" data-toggle="modal" data-target="#JoinNow"></iframe>
						@else
							<iframe width="670" height="390" src="https://www.youtube.com/embed/4xWpiVulN-Q" data-toggle="modal" data-target="#JoinNow"></iframe>
						@endif
				</div>
            </div>
        </div>
        <button type="button" class="btn modal_btn_close" id="videocloses" data-dismiss="modal">
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" width="16.99" height="16.953" viewBox="0 0 16.99 16.953"> <path id="Path_2-8" data-name="Path 2-8" d="M5622.466-2293.481a1.543,1.543,0,0,0,1.077.447,1.533,1.533,0,0,0,1.078-.447l5.895-5.9,5.894,5.893a1.546,1.546,0,0,0,1.077.447,1.525,1.525,0,0,0,1.076-.447,1.5,1.5,0,0,0,.014-2.123l-.014-.013-5.893-5.893,5.893-5.893a1.512,1.512,0,0,0,0-2.138,1.511,1.511,0,0,0-2.136,0l-5.893,5.892-5.892-5.893a1.51,1.51,0,0,0-2.137,0,1.511,1.511,0,0,0,0,2.136l5.89,5.889-5.893,5.894a1.469,1.469,0,0,0-.1,2.077C5622.422-2293.525,5622.444-2293.5,5622.466-2293.481Z" transform="translate(-5622.019 2309.987)" fill="#056371" style="isolation: isolate"/> </svg>
        </span>
    </button>
    </div>
    <!-- Modal -->
    

    <main role="main" class="main_activities">
        <section class="">
            <div class="container">
                <div class="about_glaucoma_title">
                    <h2>Know More About Glaucoma</h2>
                     <p style="opacity: 0;">To Know More About Glaucoma</p>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12 text-center">
                        <div class="about_glaucoma_box_1">
                            <div class="language-picker js-language-picker float-right mr-3">
                                <form action="" class="language-picker__form">
                                    <label for="language-picker-select"><svg id="language-24px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path id="Path_2472" data-name="Path 2472" d="M0,0H24V24H0Z" fill="none"/> <path id="Path_2473" data-name="Path 2473" d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z" fill="#656565"/> </svg></label>
                                    <select name="language-picker-select" class="getvideo" id="language-picker-select">
                                    	@foreach($video as $row)
										@if(isset($_SESSION['video']))
												<option  value="{{$row->language}}" @if($row->language == $_SESSION['video'][0]->language) selected @endif >{{$row->language}}</option>
										@else
											@if(isset($lan))
												<option  value="{{$row->language}}" @if($row->language == $lan) selected @endif>{{$row->language}}</option>
											@else
												<option  value="{{$row->language}}">{{$row->language}}</option>
											@endif
										@endif
										@endforeach
                                  </select>
                                </form>
                            </div>
                            <div class="icon_bg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="100" height="86" viewBox="0 0 100 86"> <g id="Group_2101" data-name="Group 2101" transform="translate(-11020 -20874)"> <rect id="Rectangle_335" data-name="Rectangle 335" width="100" height="86" transform="translate(11020 20874)" fill="rgba(255,255,255,0)"/> <g id="multimedia" transform="translate(11034 20854)"> <g id="Group_2097" data-name="Group 2097" transform="translate(2.138 33.138)"> <path id="Path_2486" data-name="Path 2486" d="M15,406v8.551H83.695V406H15Z" transform="translate(-15 -354.693)" fill="#fff"/> <path id="Path_2487" data-name="Path 2487" d="M49.347,46H15v8.551H83.695V46Z" transform="translate(-15 -46)" fill="#fff"/> </g> <path id="Path_2488" data-name="Path 2488" d="M256,46h34.347v8.551H256Z" transform="translate(-219.515 -12.862)" fill="#fff"/> <path id="Path_2489" data-name="Path 2489" d="M256,406h34.347v8.551H256Z" transform="translate(-219.515 -321.555)" fill="#fff"/> <path id="Path_2490" data-name="Path 2490" d="M62.21,106H30v42.756H94.419V106Z" transform="translate(-25.724 -64.311)" fill="#29a9ac"/> <path id="Path_2491" data-name="Path 2491" d="M256,106h32.21v42.756H256Z" transform="translate(-219.515 -64.311)" fill="#31bfa7"/> <g id="Group_2100" data-name="Group 2100" transform="translate(0 31)"> <g id="Group_2098" data-name="Group 2098"> <path id="Path_2492" data-name="Path 2492" d="M0,391v12.827H72.97V391H0Zm11.544,8.551H4.276v-4.276h7.269Zm11.4,0H15.82v-4.276h7.126Zm11.4,0H27.221v-4.276h7.126Zm27.079-4.276h7.269v4.276H61.426Zm-11.4,0h7.126v4.276H50.025Zm-11.4,0h7.126v4.276H38.623Z" transform="translate(0 -339.693)" fill="#056371"/> <path id="Path_2493" data-name="Path 2493" d="M36.485,31H0V43.827H72.97V31ZM11.544,39.551H4.276V35.276h7.269Zm11.4,0H15.82V35.276h7.126Zm11.4,0H27.221V35.276h7.126Zm11.4,0H38.623V35.276h7.126Zm11.4,0H50.025V35.276h7.126Zm11.544,0H61.412V35.276h7.283Z" transform="translate(0 -31)" fill="#056371"/> </g> <g id="Group_2099" data-name="Group 2099" transform="translate(36.485)"> <path id="Path_2494" data-name="Path 2494" d="M265.264,39.551h-7.126V35.276h7.126Zm11.4,0h-7.126V35.276h7.126ZM256,31V43.827h36.485V31Zm32.21,8.551h-7.283V35.276h7.283Z" transform="translate(-256 -31)" fill="#056371"/> <path id="Path_2495" data-name="Path 2495" d="M292.485,391H256v12.827h36.485Zm-27.221,8.551h-7.126v-4.276h7.126Zm11.4,0h-7.126v-4.276h7.126Zm11.544,0h-7.269v-4.276h7.269Z" transform="translate(-256 -339.693)" fill="#056371"/> </g> </g> <path id="Union_9" data-name="Union 9" d="M-10891-20160.932l6.415,4.275,12.4,8.268-12.4,8.273-6.415,4.275Z" transform="translate(10921.072 20211.455)" fill="#fff"/> </g> </g> </svg>
                            </div>
                            <div class="content">
                                <h6> How to manage Glaucoma - Prerna's inspiring story </h6>
                                <button type="button" class="box_custom_btn btn" style="background: #2FA6A5;" href="javascript:void(0);"  data-toggle="modal" data-target="#AboutGlaucomaVideo">Click Here</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 text-center">
                        <div class="about_glaucoma_box_1">
                            <div class="language-picker js-language-picker float-right mr-3">
                                <form action="" class="language-picker__form">
                                    <label for="language-picker-select"><svg id="language-24px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path id="Path_2472" data-name="Path 2472" d="M0,0H24V24H0Z" fill="none"/> <path id="Path_2473" data-name="Path 2473" d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z" fill="#656565"/> </svg></label>
                                    <select name="language-picker-select" class="getvideo1" id="language-picker-select">
										@foreach($video1 as $vrow)
										@if(isset($_SESSION['video1']))
												<option  value="{{$vrow->language}}" @if($vrow->language == $_SESSION['video1'][0]->language) selected @endif >{{$vrow->language}}</option>
										@else
											@if(isset($lan))
												<option  value="{{$vrow->language}}" @if($vrow->language == $lan) selected @endif>{{$vrow->language}}</option>
											@else
												<option  value="{{$vrow->language}}">{{$vrow->language}}</option>
											@endif
										@endif
										@endforeach
                                  </select>
                                </form>
                            </div>
                            <div class="icon_bg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="100" height="86" viewBox="0 0 100 86"> <g id="Group_2101" data-name="Group 2101" transform="translate(-11020 -20874)"> <rect id="Rectangle_335" data-name="Rectangle 335" width="100" height="86" transform="translate(11020 20874)" fill="rgba(255,255,255,0)"/> <g id="multimedia" transform="translate(11034 20854)"> <g id="Group_2097" data-name="Group 2097" transform="translate(2.138 33.138)"> <path id="Path_2486" data-name="Path 2486" d="M15,406v8.551H83.695V406H15Z" transform="translate(-15 -354.693)" fill="#fff"/> <path id="Path_2487" data-name="Path 2487" d="M49.347,46H15v8.551H83.695V46Z" transform="translate(-15 -46)" fill="#fff"/> </g> <path id="Path_2488" data-name="Path 2488" d="M256,46h34.347v8.551H256Z" transform="translate(-219.515 -12.862)" fill="#fff"/> <path id="Path_2489" data-name="Path 2489" d="M256,406h34.347v8.551H256Z" transform="translate(-219.515 -321.555)" fill="#fff"/> <path id="Path_2490" data-name="Path 2490" d="M62.21,106H30v42.756H94.419V106Z" transform="translate(-25.724 -64.311)" fill="#29a9ac"/> <path id="Path_2491" data-name="Path 2491" d="M256,106h32.21v42.756H256Z" transform="translate(-219.515 -64.311)" fill="#31bfa7"/> <g id="Group_2100" data-name="Group 2100" transform="translate(0 31)"> <g id="Group_2098" data-name="Group 2098"> <path id="Path_2492" data-name="Path 2492" d="M0,391v12.827H72.97V391H0Zm11.544,8.551H4.276v-4.276h7.269Zm11.4,0H15.82v-4.276h7.126Zm11.4,0H27.221v-4.276h7.126Zm27.079-4.276h7.269v4.276H61.426Zm-11.4,0h7.126v4.276H50.025Zm-11.4,0h7.126v4.276H38.623Z" transform="translate(0 -339.693)" fill="#056371"/> <path id="Path_2493" data-name="Path 2493" d="M36.485,31H0V43.827H72.97V31ZM11.544,39.551H4.276V35.276h7.269Zm11.4,0H15.82V35.276h7.126Zm11.4,0H27.221V35.276h7.126Zm11.4,0H38.623V35.276h7.126Zm11.4,0H50.025V35.276h7.126Zm11.544,0H61.412V35.276h7.283Z" transform="translate(0 -31)" fill="#056371"/> </g> <g id="Group_2099" data-name="Group 2099" transform="translate(36.485)"> <path id="Path_2494" data-name="Path 2494" d="M265.264,39.551h-7.126V35.276h7.126Zm11.4,0h-7.126V35.276h7.126ZM256,31V43.827h36.485V31Zm32.21,8.551h-7.283V35.276h7.283Z" transform="translate(-256 -31)" fill="#056371"/> <path id="Path_2495" data-name="Path 2495" d="M292.485,391H256v12.827h36.485Zm-27.221,8.551h-7.126v-4.276h7.126Zm11.4,0h-7.126v-4.276h7.126Zm11.544,0h-7.269v-4.276h7.269Z" transform="translate(-256 -339.693)" fill="#056371"/> </g> </g> <path id="Union_9" data-name="Union 9" d="M-10891-20160.932l6.415,4.275,12.4,8.268-12.4,8.273-6.415,4.275Z" transform="translate(10921.072 20211.455)" fill="#fff"/> </g> </g> </svg>
                            </div>
                            <div class="content">
                                <h6>What is Glaucoma? </h6>
                                <button type="button" class="box_custom_btn btn" style="background: #2FA6A5;" href="javascript:void(0);"  data-toggle="modal" data-target="#AboutGlaucomaVideo1">Click Here</button>
                            </div>
                        </div>
                    </div>
					<div class="col-lg-6 col-md-12 text-center mt-lg-5 mb-lg-5 mt-md-0 mb-sm-5">
                        <div class=" about_glaucoma_box_2">
                            <div class="language-picker js-language-picker float-right mr-3">
                                <form action="" class="language-picker__form">
                                    <label for="language-picker-select"><svg id="language-24px" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path id="Path_2472" data-name="Path 2472" d="M0,0H24V24H0Z" fill="none"/> <path id="Path_2473" data-name="Path 2473" d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z" fill="#656565"/> </svg></label>
                                    <select name="language-picker-select" class="getfile" id="language-picker-select">
                                    @foreach($file as $rows)
										@if(isset($_SESSION['file']))
											<option  value="{{$rows->language}}" @if($rows->language == $_SESSION['file'][0]->language) selected @endif>{{$rows->language}}</option>
										@else
											@if(isset($lan))
												<option  value="{{$rows->language}}" @if($rows->language == $lan) selected @endif>{{$rows->language}}</option>
											@else
												<option  value="{{$rows->language}}">{{$rows->language}}</option>
											@endif
										@endif
								   @endforeach
                                  </select>
                                </form>
                            </div>
                            <div class="icon_bg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="100" height="86" viewBox="0 0 100 86"> <rect id="Rectangle_335" data-name="Rectangle 335" width="100" height="86" fill="rgba(255,255,255,0)"/> <g id="document_1_" data-name="document (1)" transform="translate(14.988 7.988)"> <path id="Path_2498" data-name="Path 2498" d="M146.52,0,120,66.571h35.866a15.379,15.379,0,0,0,9.056,2.706,8.142,8.142,0,0,0,8.118-8.118V0Z" transform="translate(-103.763)" fill="#b459ac"/> <path id="Path_2499" data-name="Path 2499" d="M120,0h26.52V66.571H120Z" transform="translate(-103.763)" fill="#dc79d4"/> <path id="Path_2500" data-name="Path 2500" d="M300.343,452H290.059L286,456.059l4.059,4.059h18.4A8.142,8.142,0,0,1,300.343,452Z" transform="translate(-247.302 -390.841)" fill="#6b2365"/> <path id="Path_2501" data-name="Path 2501" d="M0,452a8.142,8.142,0,0,0,8.118,8.118H42.757V452Z" transform="translate(0 -390.841)" fill="#b459ac"/> <path id="Path_2502" data-name="Path 2502" d="M321.7,271H307.353L306,273.03l1.353,2.03H321.7Z" transform="translate(-264.596 -234.332)" fill="#fff"/> <path id="Path_2503" data-name="Path 2503" d="M210,271h14.343v4.059H210Z" transform="translate(-181.585 -234.332)" fill="#fff"/> <path id="Path_2504" data-name="Path 2504" d="M321.7,211H307.353L306,213.03l1.353,2.03H321.7Z" transform="translate(-264.596 -182.45)" fill="#fff"/> <path id="Path_2505" data-name="Path 2505" d="M210,211h14.343v4.059H210Z" transform="translate(-181.585 -182.45)" fill="#fff"/> <path id="Path_2506" data-name="Path 2506" d="M321.7,151H307.353L306,153.03l1.353,2.03H321.7Z" transform="translate(-264.596 -130.569)" fill="#fff"/> <path id="Path_2507" data-name="Path 2507" d="M210,151h14.343v4.059H210Z" transform="translate(-181.585 -130.569)" fill="#fff"/> </g> </svg>
                            </div>
                           <div class="content">
                                <h6>Know more about Glucoma</h6>
								@if(isset($_SESSION['file']))
									<a  class="box_custom_btn btn" href="{{$_SESSION['file'][0]->url}}"  target="_blank"> Click Here</a>
									@elseif($lan == "English")
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/English_bz.pdf"  target="_blank"> Click Here</a>
									@elseif($lan == "Hindi")  
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/hindi_cc.pdf"  target="_blank"> Click Here</a>
									@elseif($lan == "Bengali")  
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/Bengali_bz.pdf"  target="_blank"> Click Here</a>
									@elseif($lan == "Gujarati")  
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/Gujarati_bz.pdf"  target="_blank"> Click Here</a>
									@elseif($lan == "Kannada")  
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/kannada_cc.pdf"  target="_blank"> Click Here</a>
									@elseif($lan == "Malayalam")  
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/malayalam_cc.pdf"  target="_blank"> Click Here</a>
									@elseif($lan == "Marathi")  
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/Marathi_bz.pdf"  target="_blank"> Click Here</a>
									@elseif($lan == "Tamil")  
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/Tamil_bz.pdf"  target="_blank"> Click Here</a>
									@elseif($lan == "Telugu")  
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/Telugu_bz.pdf"  target="_blank"> Click Here</a>
									@else
										<a  class="box_custom_btn btn" href="../frontend/careshare/glaucoma/English_bz.pdf"  target="_blank"> Click Here</a>
									@endif
						    </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>


    <script src="../frontend/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="../frontend/js/popper.min.js" type="text/javascript"></script>
    <script src="../frontend/js/bootstrap.min.js" type="text/javascript"></script>
 <script>
        $('#AboutGlaucomaVideo').on('hidden.bs.modal', function(e) {
            $('iframe').attr('src', $('iframe').attr('src'));
        })
    </script>
<script>
$(document).ready(function(){
$(document).on("change", ".getvideo", function(){
 $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
  var lan = $(this).val();
	$.ajax({
		url:'{{ route("carenshare.video") }}',
		type:'post',
		data:{"lan":lan},
		success:function(data)
		{
		  if(data == "update"){
			window.location.reload(true);
			}
		}
	  });


});
$(document).on("change", ".getvideo1", function(){
 $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
  var lan = $(this).val();
	$.ajax({
		url:'{{ route("carenshare.video1") }}',
		type:'post',
		data:{"lan":lan},
		success:function(data)
		{
		  if(data == "update"){
			window.location.reload(true);
			}
		}
	  });


});
$(document).on("change", ".getvideos", function(){
 $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
  var lan = $(this).val();
	$.ajax({
		url:'{{ route("carenshare.modalvideo") }}',
		type:'post',
		data:{"lan":lan},
		success:function(data)
		{
			$("#bindvideo").html(data);
		}
	  });


});
$(document).on("change", ".videoss", function(){
 $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
  var lan = $(this).val();
	$.ajax({
		url:'{{ route("carenshare.modalvideos") }}',
		type:'post',
		data:{"lan":lan},
		success:function(data)
		{
			$("#videosBind").html(data);
		}
	  });


});
$(document).on("change", ".getfile", function(){
 $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
  var lan = $(this).val();
	$.ajax({
		url:'{{ route("carenshare.file") }}',
		type:'post',
		data:{"lan":lan},
		success:function(data)
		{
		  if(data == "update"){
			window.location.reload(true);
			}
		}
	  });


});
$(document).on("click", "#videoclose", function(){
	window.location.reload(true);
});
$(document).on("click", "#videocloses", function(){
	window.location.reload(true);
});
});

</script>	
</body>

</html>