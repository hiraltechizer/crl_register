<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=G-YT7TRLL0Q6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-YT7TRLL0Q6');
</script>
    <meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../frontend/images/ft-logo.png" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../frontend/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../frontend/css/style.css" media="all" type="text/css" />
    <title>Bone & Joint</title>
</head>
<body>
    <!----------------header start------------------->

    <header class="header_activities ">
        <div class="container">
            <nav class="navbar navbar-expand-lg  navbar-dark p-0 d-flex justify-content-between">
                <a class="" href="{{ url('/') }}"><span class=""></span></a>
                <a class="navbar-brand m-0" href="/"><img src="../frontend/images/logo_white.jpg" alt="logo" class="logo img-fluid"></a>
                <div>
                    <div class="navbar-collapse collapse show" id="navbarsExample03">
                        <div class="navbar-nav ml-auto"></div>
                    </div>
                </div>
            </nav>
        </div>
    </header>
	
	
                
    <!-- Modal -->
    
	</br></br>
    <main role="main" class="main_activities">
        <section class="">
            <div class="container">
                <div class="about_glaucoma_title">
                    <h5>Terms & Conditions</h5>
                </div></br>
				<div>
                    <p>By sharing the details, I hereby authorize the service provider & its employees to collect and process my personal information for the purpose of enabling me to participate in Osteoarthritis – Bone & Joint Care Patient Support Program. This program is of free service to patients in India who have been prescribed OstoShine, to help manage the disease.</p>
					<p>I hereby authorize the service provider to:</p>
					<ul>
					  <li>Collect personal information from me through WhatsApp number, Link , &/or telephonically, etc.</li>
					  <li>Provide education on the disease condition, health tips, exercise tips through Web videos, patient education leaflets, SMS, etc.</li>
					  <li>Follow-up with me telephonically or via WhatsApp for assistance in Osteoarthritis - Bone & Joint Care Patient Support Program</li>
					</ul>
				</div>
              
            </div>
        </section>
    </main>


    <script src="../frontend/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="../frontend/js/popper.min.js" type="text/javascript"></script>
    <script src="../frontend/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>