<?php

return [
    'common_error_msg'=>'Something went wrong!',

    'error_account_exists'=>'Account does not exists.',

    'error_account_disable'=>'Your account has been disable.',

    'error_account_delete'=>'Your account has been suspended by administrator.',

    'error_auth_fail'=>'Authentication failed.',

    'succ_logout'=>'You have been successfully logged out!',

    'error_user_id'=>'The user id field required.',

    'no_tour_plan_found' => 'No tour plan found.',

    'no_red_found' => 'No record found.',

    'no_doctors_registered' => 'No doctors registered by user.',

    'scc_doctor_detail_added' => 'Doctor information has been updated.',

    'scc_potential_updated' => 'Potential has been saved successfully',

    'scc_tour_plan_updated' => 'Tour plan has been updated successfully',

    'scc_tour_plan_added' => 'Tour plan has been added successfully',

    'err_tour_plan_exist' => 'Tour plan already exist with selected date.',

    'scc_user_status_updated' => 'User status has been updated successfully',
	
    'scc_dr_support' => 'Support has been saved successfully',

    'not_allowed_view_data' => 'You are not allowed to view data of this month.',

    'not_allowed_add_data' => 'You can edit or add, only the current month of data.',

    'error_password_incorrect' => 'Invalid password.',

    'warning_rem_month_support' => "You must fill out the remaining month's support.",

    'warning_ren_month_potential' => 'You will need to fill the older month of support. before fill support, you must be filled potential on that month.',

    'warning_did_not_change_support' => "You can't not change support value.",

    'succ_reminder_sent' => "Reminder has been sent successfully.",
];
